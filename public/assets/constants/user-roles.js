app.constant('USER_ROLES', {
    all: 'all',
    basic: 'basic',
    employee: 'employee',
    hod: 'hod',
    admin: 'admin',
    accounts: 'accounts',
    superadmin: 'superadmin',
    vendor: 'vendor'
});

app.constant('UI_EVENTS', {
    showBackButton: 'show-back-button',
    isLoginPage: 'is-login-page',

    // Create job section
    createJob: {
        formError: 'form-error'
    },

    // Success bar
    messageActive: 'message-active',
    showSuccessBar: 'show-success-bar',

    // Multi select
    resetMultiSelect: 'reset-multi-select',
    updateMultiSelect: 'update-multi-select'

});

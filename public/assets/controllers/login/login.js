app.controller('LoginCtrl', function ($scope, $localStorage, $rootScope, AUTH_EVENTS, Auth, UI_EVENTS, Notification) {
    $scope.credentials = {
        contactNum: '',
        password: ''
    };
    $scope.processing = false;
    $scope.login = function (credentials) {
        $scope.processing = true;
        Auth.login(credentials).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
            },
            function (error) {
                if (error == null) {
                    Notification({
                        message: "Error logging in!"
                    }, 'error');
                } else {
                    Notification({
                        message: error.msg
                    }, 'error');
                }
            }).finally(function () {
            $scope.processing = false;
        });

    };

    $scope.fb_login = function (credentials) {
        $scope.processing = true;

        //        var fb_login_data = new FormData();
        //        fb_login_data.append('contactNum', credentials.contactNum);
        //        fb_login_data.append('password', credentials.password);

        Auth.fb_login(credentials).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
            },
            function (error) {
                if (error == null) {
                    Notification({
                        message: "Error logging in!"
                    }, 'error');
                } else {
                    Notification({
                        message: error.msg
                    }, 'error');
                }
            }).finally(function () {
            $scope.processing = false;
        });

    };

});

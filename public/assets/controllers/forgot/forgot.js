app.controller('ForgotCtrl', function ($scope, $localStorage, $rootScope, AUTH_EVENTS, AuthForgot, UI_EVENTS, Notification) {
    $scope.credentials = {
        contactNum: ''
    };
    $scope.processing = false;

    $scope.submit = function (credentials) {

        $scope.processing = true;
        AuthForgot.send(credentials).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }
        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            $scope.processing = false;
        });
    };

});

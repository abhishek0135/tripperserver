app.controller('OrderCtrl', function ($scope, Session, $state, Users, Notification, AuthSignup,Operators, Vendor, Plan,
    GetUserDashboard , Stores,Banks,fileService,Policy,Trend,Subscription,Offer,Electricity,Broadband,LifeInsurance,LandlinePostpaid,
    Educationfees,HousingSociety,HealthInsurance,Gas,Fastag,LoanRepayment,MunicipalTaxes,Water,Operators) {

    $scope.isSuperAdmin=false;
    $scope.pwdeditmode=false;
    $scope.role = Session.user.userRole;
    if($scope.role == 'superadmin')
        {
           
            $scope.isSuperAdmin=true;
        }
    $scope.genpolicymode=true;
    $scope.pdpolicymode=false;
    $scope.editusermode=false;
    $scope.typeofaccounts = ["Savings", "Current", "Other"];
    $scope.typeofoperators = ["Flight", "Train", "Bus","Taxi","Others"];
     $scope.travpolicymode=false;
    $scope.show_warning = false;
    
    
    var refresh_all = function () {
        $scope.selectedProductId = "";
        $scope.processing = true;
        $scope.new_user_processing = false;
        $scope.new_department_processing = false;
        $scope.new_policy_processing = false;
        $scope.new_trend_processing = false;
        
        $scope.new_vendor_processing = false;
        $scope.new_expensehead_processing = false;
        
        $scope.new_bank_processing = false;
        $scope.new_operator_processing = false;
        $scope.new_employeegrade_processing = false;

        $scope.edit_user_processing = false;
        $scope.edit_department_processing = false;
        $scope.edit_policy_processing = false;
        // $scope.edit_hotel_processing = false;
        // $scope.edit_vendor_processing = false;
        $scope.edit_expensehead_processing = false;
          $scope.edit_client_processing = false;
        $scope.edit_city_processing = false;
        $scope.edit_bank_processing = false;
        $scope.edit_operator_processing = false;
         $scope.edit_employeegrade_processing = false;

        $scope.employee_tabs = false;
        $scope.department_tabs = false;
        $scope.policy_tabs = false;
        // $scope.hotel_tabs = false;
        $scope.vendor_tabs = false;
        $scope.plan_tabs = false;
        $scope.subscription_tabs = false;
        $scope.client_tabs = false;
        $scope.expensehead_tabs = false;
         $scope.city_tabs = false;
         $scope.operator_tabs = false;
         $scope.prime_tabs = false;
        $scope.bank_tabs = false;
        $scope.operator_tabs = false;
         $scope.employeegrade_tabs = false;
         $scope.policy_tabs = false;
        $scope.employeeGrades = [];
        $scope.banks = [];
         
        $scope.employeeGrade = [];
        $scope.hods = [];
        $scope.policy_employee_grades_signals = [];
        $scope.policy_employee_grades = [];
        $scope.edit_policy_employee_grades = [];
        // $scope.hotel_cities = Session.citiesArray;
        $scope.policy_cities = Session.citiesArray;
        $scope.client_cities = Session.citiesArray;
        $scope.expenseHeads = Session.expenseHeadsArray;
     //   $scope.employeeGrades = Session.employeegrades;
        

        $scope.users = [];
        
        
        // $scope.hotels = [];
        $scope.gasvendors = [];
        $scope.educationvendors = [];
        $scope.electricityvendors = [];
        $scope.municipalvendors = [];
        $scope.fastagvendors = [];
        $scope.broadbandvendors = [];
        $scope.landlinevendors = [];
        $scope.watervendors = [];
        $scope.housingvendors = [];
        $scope.loanvendors = [];
        $scope.healthvendors = [];
        $scope.lifevendors = [];
        $scope.trends = [];
        $scope.feed = [];
        $scope.bbpsTxn = [];
        $scope.dmtTxn = [];
        $scope.transactions = [];
        $scope.subscriptions = [];
        
        
       
        $scope.confirm_email = '';
        $scope.userinfo = {
            fullName: '',
            contactNum: '',
            emailId: '',
            employeeAddress: '',
            clientId: Session.user.clientId,
            accessLevelName: ''
        };
        
       

       


        
       
        $scope.subscriptioninfo = {
            "title": '',
            "description": '',
            "validity": ''   ,
            "freeTransaction": ''   ,
            "validity": '' ,
            'amount'  :''
        };
        $scope.offerinfo = {
            "title": '',
            "description": '',
            "validity": ''   ,
            "couponCode": ''   ,
            "validity": '' ,
            'amount'  :''
        };
        

        GetUserDashboard.get().then(function (response) {
            if (response.success) {
              
              
              
                $scope.orders =response.orders;
               
                $scope.dmtTxn =response.dmtTxn;
                $scope.subscriptions =response.subscriptions;
                
             
                $scope.trends = response.trends;
                $scope.bbpsTxn = response.bbpsTxn;
                $scope.operators = response.operators;
                $scope.vouchers = response.voucherTxn;
                $scope.trends = response.topBanner;
                $scope.feed = response.offers;
                $scope.transactions = response.transactions;
                $scope.jriRes =response.jriRes;
                $scope.bbpsBalance =response.bbpsBalance;
                //changes
                
                
                //changesend
                $scope.useremp=response.employeeGrade;
                $scope.employeeGrades = response.employeeGrade;
                angular.forEach(response.employeeGrade, function (value, key) {
                    $scope.policy_employee_grades_signals.push({
                        value: false,
                        name: value.employeeGrade
                    });
                });
                $scope.policies = response.policiesarr;
                $scope.banks = response.banks;
              
                $scope.processing = false;
                $scope.subcategories = response.subcategories;
                
                $scope.expenseHeads = response.expenseHeads;
                $scope.plans =response.plans;
                $scope.cities =response.cities;
                $scope.product_file=undefined;
               // Session.citiesArray=response.cities;
                $scope.policy_cities =response.cities;
                $scope.clients =response.clients;
                if($scope.isSuperAdmin)
                    {
                        $scope.users=response.users;
                    }
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }
        }, function (error) {
            Notification({
                message: "Error fetching data. Please check your internet connection."
            }, 'error');
        });
        
        // if($scope.editusermode)
        //     {
        //         Users.getAllHods($scope.edit_employee_data).then(function (response) {
        //     if (response.success) {
        //            if(!$scope.isSuperAdmin)
        //                {
        //                    $scope.users = response.users;
        //                }
                
        //         angular.forEach(response.users, function (value, key) {
        //             if (angular.equals(value.accessLevelName, "hod")) {
        //                 $scope.hods.push(value);
        //             }
        //         });
        //         $scope.processing = false;
        //     } else {
        //         Notification({
        //             message: response.msg
        //         }, 'error');

        //     }
        // }, function (error) {
        //     Notification({
        //         message: "Error fetching data. Please check your internet connection."
        //     }, 'error');
        // });
        //     }
        // else
        //     {
        //         Users.getAll().then(function (response) {
        //     if (response.success) {
        //            if(!$scope.isSuperAdmin)
        //                {
        //                    $scope.users = response.users;
        //                }
                
        //         angular.forEach(response.users, function (value, key) {
        //             if (angular.equals(value.accessLevelName, "hod")) {
        //                 $scope.hods.push(value);
        //             }
        //         });
        //         $scope.processing = false;
        //     } else {
        //         Notification({
        //             message: response.msg
        //         }, 'error');

        //     }
        // }, function (error) {
        //     Notification({
        //         message: "Error fetching data. Please check your internet connection."
        //     }, 'error');
        // });
        //     }
        
    };
       $scope.editpwd = function ()
      {
          $scope.pwdeditmode= true;
          
      };
        $scope.updateuser = function (response) {
       
          if($scope.pwdeditmode==true)
              {
                  var pwd=response.pwd;
                   var updated_data = {
                                    userId: response._id,
                                    clientId: response.clientId,
                                    fullName: response.fullName,
                                    contactNum: response.contactNum,
                                    homeNum: response.homeNum,
                                    officeNum:response.officeNum,
                                    employeeAddress: response.employeeAddress,
                                    emailId: response.emailId,
                                    pwd:pwd,
                                    pwdmode:true
                                        };
              }
          else
              {
                    var updated_data = {
            userId: response._id,
            clientId: response.clientId,
            fullName: response.fullName,
            contactNum: response.contactNum,
            homeNum: response.homeNum,
            officeNum:response.officeNum,
            employeeAddress: response.employeeAddress,
            emailId: response.emailId,
                        pwdmode:false
        };
              }
      

        Users.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {

           
            $scope.editmode = false;
            $scope.pwdeditmode =false;
        });

    };

    //------------------------------------------------------------------- Add Functions
    $scope.add_user = function (userinfo, confirm_email) {
        
//        if($scope.userform.$invalid)
//            {
//                Notification({
//                        message: "Please check all form fields"
//                    }, 'failure');
//                return;
//            }
        $scope.new_user_processing = true;
        
        if(userinfo.accessLevelName=="hod")
            {
                userinfo.isHod=true;
            }
         else if(userinfo.accessLevelName=="admin")
            {
                userinfo.isAdmin=true;
            }
        else if(userinfo.accessLevelName=="accounts")
            {
                userinfo.isAccounts=true;
            }
        if (userinfo.emailId != confirm_email) {
            Notification({
                message: 'Email mismatch!'
            }, 'error');
            $scope.new_user_processing = false;
        } else {
            if(!(userinfo.departmentId==null || userinfo.departmentId==""))
                {
                     userinfo.departmentId = userinfo.departmentId._id;
                }
           
            userinfo.employeeGrade = userinfo.employeeGradeId.employeeGrade;
            userinfo.employeeGradeId = userinfo.employeeGradeId._id;
            userinfo.currentWorkingCityId = userinfo.currentWorkingCityId._id;
            userinfo.bankName=userinfo.bankName.bankName;
            if(!(userinfo.clientId==null ||userinfo.clientId==""))
                {
                    userinfo.clientName=userinfo.clientId.clientName;
                }

            AuthSignup.signup(userinfo).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                    $scope.userinfo = {
                        fullName: '',
                        contactNum: '',
                        emailId: '',
                        employeeAddress: '',
                        clientId: Session.user.clientId,
                        accessLevelName: '',
                        departmentId: '',
                        employeeGradeId: ''
                    };
                    $scope.confirm_email = '';
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }

            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                $scope.new_user_processing = false;
            });

        }
    };
    
    // $scope.updateData= function(vendorinfo)
    // {
    //     $scope.vendorinfo.vendorCategoryId=vendorinfo.vendorCategoryId;
        
    //  $scope.vendorinfo.vendorCategoryName=vendorinfo.vendorCategoryName;
        
        
    // };

    
    
    //add category
      $scope.add_category = function (categoryinfo) {
        $scope.new_bank_processing = true;
        categoryinfo.userId=Session.user.user;

        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }

        var category = new FormData();
            
        category.append('userId',Session.user.user);
        category.append('attachment',file);
        category.append('categoryName',categoryinfo.categoryName);
        category.append('categoryVendorId',categoryinfo.categoryVendorId._id);
        category.append('categoryVendorName',categoryinfo.categoryVendorId.vendorName);
        

        Banks.createn(category).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.categoryinfo = {
                   categoryName: '',
                   categoryVendorId:'',
                    categoryVendorName:'',
                    
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            $scope.new_bank_processing= false;
            fileService.length=0;
        });

    };

    //add category
    $scope.add_store = function (storeinfo) {
        $scope.new_department_processing = true;
        storeinfo.userId=Session.user.user;

        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }

        var store = new FormData();
            
        store.append('userId',Session.user.user);
        store.append('attachment',file);
        store.append('storeName',storeinfo.storeName);
        store.append('storeContactNumber',storeinfo.storeContactNumber);
        store.append('storeAddress',storeinfo.storeAddress);
        store.append('storeVendorId',storeinfo.storeVendorId._id);
        store.append('storeVendorName',storeinfo.storeVendorId.vendorName);
        

        Stores.createn(store).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.storeinfo = {
                   storeName: '',
                   storeContactNumber:'',
                   storeAddress: '',
                   storeVendorId:'',
                    storeVendorName:'',
                    
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            $scope.new_department_processing= false;
            fileService.length=0;
        });

    };

   
    
    //add operator
    $scope.add_vendorCategory = function (vendorcategoryinfo) {
        $scope.new_operator_processing = true;
        vendorcategoryinfo.userId=Session.user.user;
        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }


        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('vendorCategoryName',vendorcategoryinfo.vendorCategoryName);
        

        Operators.createn(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    };


    //vendor Active inactive

    $scope.mySelectHandler = function (vendor,selected) {
      
        $scope.new_operator_processing = true;
       console.log(vendor);
    
      var s= selected;
       console.log(s);
       if(s=="true"){
           s=true;
       }else{
           s=false;
       }
      
        // var updated_data = new FormData();
        var updated_data = {'userId':Session.user.user,'billerId':vendor.billerId,'isActive':s};
        var updated_data = JSON.stringify(updated_data);
    // updated_data.append('userId',Session.user.user);
  
    // updated_data.append('billerId',vendor.billerId);
    // updated_data.append('isActive',s);
   
   if(vendor.billerCategory=="Electricity"){
    var file = fileService[0];
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editelectricity').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
        Electricity.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }
    
        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editelectricity').modal('hide');
            $scope.edit_city_processing = false;
            fileService.length=0;
        });
    }
   
   }else if(vendor.billerCategory=="Fastag"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editvendor').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    Fastag.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editvendor').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Broadband Postpaid"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editvendor').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    Broadband.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editgas').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Gas"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editgas').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    Gas.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editgas').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Municipal Taxes"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editmunicipal').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    MunicipalTaxes.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editmunicipal').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Education Fees"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editeducation').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    Educationfees.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editeducation').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Landline Postpaid"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editlandline').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    LandlinePostpaid.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editlandline').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Loan Repayment"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editloan').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    LoanRepayment.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editloan').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Life Insurance"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editlife').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    LifeInsurance.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editlife').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Health Insurance"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#edithealth').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
        
    HealthInsurance.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#edithealth').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Housing Society"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#edithousing').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
       
    HousingSociety.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#edithousing').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }else if(vendor.billerCategory=="Water"){
    if (file != undefined) {
        var vendorCategory = new FormData();
            
        vendorCategory.append('userId',Session.user.user);
        vendorCategory.append('attachment',file);
        vendorCategory.append('billerCategory',vendor.billerCategory);
        vendorCategory.append('billerId',vendor.billerId);

        Electricity.updateI(vendorCategory).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorcategoryinfo = {
                   vendorCategoryName: '',
                   "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editwater').modal('hide');
            $scope.new_operator_processing= false;
            fileService.length=0;
        });
    }else{
    Water.update(updated_data).then(function (response) {
        if (response.success) {
            Notification({
                message: response.msg
            }, 'success');
            refresh_all();
        } else {
            Notification({
                message: response.msg
            }, 'error');
        }

    }, function (error) {
        Notification({
            message: error.msg
        }, 'error');
    }).finally(function () {
        angular.element('#editwater').modal('hide');
        $scope.edit_city_processing = false;
        fileService.length=0;
    });
}
   }

    
        
    };
     
    
      //add plan
    
    $scope.add_product = function (productinfo) {
        $scope.new_plan_processing = true;
        productinfo.userId=Session.user.user;

        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }
        var product = new FormData();
            
        product.append('userId',Session.user.user);
        product.append('attachment',file);
        product.append('productName',productinfo.productName);
        product.append('productCost',productinfo.productCost);
        product.append('productSell',productinfo.productSell);
        product.append('productQuantity',productinfo.productQuantity);
        product.append('productCategory',productinfo.productcategoryId.vendorCategoryName);
        product.append('productCategoryId',productinfo.productcategoryId._id);
        product.append('productSubCategory',productinfo.productSubCategoryId.subCategoryName);
        product.append('productSubCategoryId',productinfo.productSubCategoryId._id);
        product.append('productVendorId',productinfo.productVendorId._id);
        product.append('productVendorName',productinfo.productVendorId.vendorName);


       

        Plan.createn(product).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.productinfo = {
                    "productName": '',
                    "productCost": '',
                    "productSell": '',
                    "productQuantity":'',
                    "productCategory":'',
                    "productcategoryId":'',
                    "productSubCategoryName":'',
                    "productSubCategoryId":'',
                    "productVendorId":'',
                    "productVendorName":'',
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            fileService.length=0;
            $scope.new_plan_processing = false;
        });

    };

    //Add News Feed

     //add plan
    
     $scope.add_policy = function (newsinfo) {
        $scope.new_policy_processing = true;
        newsinfo.userId=Session.user.user;

        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }
        var news = new FormData();
            
        news.append('userId',Session.user.user);
        news.append('attachment',file);
        news.append('newsTitle',newsinfo.newsTitle);
        news.append('newsType',newsinfo.newsType);
        news.append('newsDescription',newsinfo.newsDescription);
        news.append('newsVendorId',newsinfo.newsVendorId._id);
        news.append('newsVendorName',newsinfo.newsVendorId.vendorName);
        


       

        Policy.createn(news).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.newsinfo = {
                    "newsDescription": '',
                    "newsTitle": '',
                    "newsVendorId":'',
                    "newsVendorName":'',
                    "newsType":'',
                    
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            fileService.length=0;
            $scope.new_policy_processing = false;
        });

    };


    //add Operator

    

    $scope.add_operator = function (operatorinfo) {
        $scope.new_policy_processing = true;
        operatorinfo.userId=Session.user.user;

        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }
        var operator = new FormData();
            
        operator.append('userId',Session.user.user);
        operator.append('attachment',file);
        operator.append('operatorName',operatorinfo.operatorName);
        operator.append('benefits',operatorinfo.benefits);
        operator.append('validity',operatorinfo.validity);
        operator.append('amount',operatorinfo.amount);
        
        


       

        Operators.createn(operator).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.operatorinfo = {
                
                    "operatorName": '',
                    "benefits":'',
                    "validity":'',
                    "amount":'',
                    
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            fileService.length=0;
            $scope.new_policy_processing = false;
        });

    };

      //Delete Operator

      $scope.deleteOperator = function (operatorinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
   
       
             operatorId: operatorinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Operators.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Operator deleted successfully");
         });
 
     };

    //add trends

    $scope.add_trends = function (trendinfo) {
        console.log(trendinfo);
        $scope.new_trend_processing = true;
        trendinfo.userId=Session.user.user;

        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }
        var trends = new FormData();
            
        trends.append('userId',Session.user.user);
        trends.append('attachment',file);
        trends.append('trendTitle',trendinfo.trendTitle);
        trends.append('trendType',trendinfo.trendType);
        trends.append('trendDescription',trendinfo.trendDescription);
        
        


       

        Trend.createn(trends).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.trendinfo = {
                    "newsDescription": '',
                    "trendTitle": '',
                    "trendType": '',
                    

                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            fileService.length=0;
            $scope.new_trend_processing = false;
        });

    };

     // Edit Trend

     $scope.edit_trend = function (trendinfo) {
        $scope.edit_city_processing = true;

        var file = fileService[0];
        if (file != undefined) {
            var updated_data = new FormData();
            var subCatImg=true;
        updated_data.append('userId',Session.user.user);
        updated_data.append('attachment',file);
        updated_data.append('trendTitle',trendinfo.trendTitle);
        updated_data.append('trendDescription',trendinfo.trendDescription);
        updated_data.append('trendId',$scope.selectedTrendId);
        updated_data.append('subCatImg',subCatImg);

        Trend.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editTrend').modal('hide');
            $scope.edit_city_processing = false;
            fileService.length=0;
        });
        }
        else{
            var updated_data = new FormData();
            var subCatImg=false;
            updated_data.append('userId',Session.user.user);
            updated_data.append('trendTitle',trendinfo.trendTitle);
            updated_data.append('trendDescription',trendinfo.trendDescription);
            updated_data.append('trendId',$scope.selectedTrendId);
            updated_data.append('subCatImg',subCatImg);

            Trend.update(updated_data).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                angular.element('#editTrend').modal('hide');
                $scope.edit_city_processing = false;
               
            });

        }
        

      

    };

    //Delete Trend

    $scope.deleteTrend = function (trendinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
            trendType: trendinfo.trendType,
            trendDescription: trendinfo.trendDescription,
             trendId: trendinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Trend.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Trend deleted successfully");
         });
 
     };

// Add Offer
     $scope.add_offer = function (offerinfo) {
        $scope.new_plan_processing = true;
       

        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }
        var offers = new FormData();
            
        offers.append('userId',Session.user.user);
        offers.append('attachment',file);
       
        offers.append('title',offerinfo.title);
        offers.append('description',offerinfo.description);
        offers.append('validity',offerinfo.validity);
        offers.append('couponCode',offerinfo.couponCode);
        offers.append('amount',offerinfo.amount);
        


       

        Offer.createn(offers).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.offerinfo = {
                    "title": '',
                    "description": '',
                    "couponCode": '',
                    "validity": '',
                    "amount":'',
                  
                    
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            fileService.length=0;
            $scope.new_plan_processing = false;
        });

    };

     // Edit Subscription

     $scope.edit_subscription = function (edit_subscription_data) {
        $scope.edit_subscription_processing = true;

        var file = fileService[0];
        if (file != undefined) {
            var updated_data = new FormData();
            var subCatImg=true;
            updated_data.append('userId',Session.user.user);
            updated_data.append('attachment',file);
            updated_data.append('subscriptionName',edit_subscription_data.subscriptionName);
            updated_data.append('subscriptionDescription',edit_subscription_data.subscriptionDescription);
            updated_data.append('subscriptionId', $scope.selectedSubscriptionId);
            updated_data.append('subscriptionAmount', edit_subscription_data.subscriptionAmount);
            updated_data.append('subscriptionOfferPrice', edit_subscription_data.subscriptionOfferPrice);
       
            updated_data.append('subCatImg',subCatImg);
        Subscription.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editSubscription').modal('hide');
            $scope.edit_subscription_processing = false;
            fileService.length=0;
        });
        }
        else{
            var updated_data = new FormData();
            var subCatImg=false;
            updated_data.append('userId',Session.user.user);
            updated_data.append('subscriptionDescription',edit_subscription_data.subscriptionDescription);
            updated_data.append('subscriptionName',edit_subscription_data.subscriptionName);
            updated_data.append('subscriptionId', $scope.selectedSubscriptionId);
            updated_data.append('subscriptionAmount', edit_subscription_data.subscriptionAmount);
            updated_data.append('subscriptionOfferPrice', edit_subscription_data.subscriptionOfferPrice);
       
            updated_data.append('subCatImg',subCatImg);
        Subscription.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editSubscription').modal('hide');
            $scope.edit_subscription_processing = false;
        });

        }
       

    };


    // Add Subscription
    $scope.add_subscription = function (subscriptioninfo) {
        $scope.new_plan_processing = true;
       

        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
            var subscription = new FormData();
            
            subscription.append('userId',Session.user.user);
            subscription.append('attachment',file);
           
            subscription.append('title',subscriptioninfo.title);
            subscription.append('description',subscriptioninfo.description);
            subscription.append('validity',subscriptioninfo.validity);
            subscription.append('freeTransaction',subscriptioninfo.freeTransaction);
            subscription.append('amount',subscriptioninfo.amount);
            
    
    
           
    
            Subscription.createn(subscription).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                    $scope.subscriptioninfo = {
                        "title": '',
                        "description": '',
                        "freeTransaction": '',
                        "validity": '',
                        "amount":'',
                      
                        
                        "userId": Session.user.user
                      
                    };
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                fileService.length=0;
                $scope.new_plan_processing = false;
            });
        }else{
            alert("Please Upload an Image");
        }
       

    };

    //Delete Trend

    $scope.deleteSubscription = function (subscriptioninfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
            subscriptionType: subscriptioninfo.subscriptionName,
            subscriptionDescription: subscriptioninfo.subscriptionDescription,
            subscriptionAmount: subscriptioninfo.subscriptionAmount,
            subscriptionOfferPrice: subscriptioninfo.subscriptionOfferPrice,
             subscriptionId: subscriptioninfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Subscription.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Subscription deleted successfully");
         });
 
     };
    
     //Delete Product

    $scope.deleteProduct = function (productinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
            
             productId: productinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Plan.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Trend deleted successfully");
         });
 
     };

     //Delete Category

    $scope.deleteCategory = function (categoryinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
            
             categoryId: categoryinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Banks.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Trend deleted successfully");
         });
 
     };

     //edit SubCategory

     $scope.editSubCategory = function (subCategoryinfo) {
        // $scope.edit_hotel_processing = true;
        
        var file = fileService[0];
        if (file != undefined) {
            var editSubCategory = new FormData();
            var subCatImg=true;
        editSubCategory.append('userId',Session.user.user);
        editSubCategory.append('attachment',file);
        editSubCategory.append('subCategoryName',subCategoryinfo.subCategoryName);
        editSubCategory.append('subCategoryId',$scope.selectedsubCategoryId);
        editSubCategory.append('subCatImg',subCatImg);
      
       
       
        // Hotel.update(editSubCategory).then(function (response) {
        //     if (response.success) {
        //         Notification({
        //             message: response.msg
        //         }, 'success');
        //         refresh_all();
        //         $scope.subCategoryinfo = {
        //             "subCategoryName": '',
                   
                   
                  
        //             "userId": Session.user.user
                  
        //         };
        //     } else {
        //         Notification({
        //             message: response.msg
        //         }, 'error');
        //     }

        // }, function (error) {
        //     Notification({
        //         message: error.msg
        //     }, 'error');
        // }).finally(function () {
        //     angular.element('#edithotel').modal('hide');
        //     $scope.edit_hotel_processing = false;
        //     fileService.length=0;
        // });
        }else{
           
            var editSubCategory = new FormData();
            var subCatImg=false;
            editSubCategory.append('userId',Session.user.user);
           
            editSubCategory.append('subCategoryName',subCategoryinfo.subCategoryName);
            editSubCategory.append('subCategoryId',$scope.selectedsubCategoryId);
            editSubCategory.append('subCatImg',subCatImg);
          
            // Hotel.update(editSubCategory).then(function (response) {
            //     if (response.success) {
            //         Notification({
            //             message: response.msg
            //         }, 'success');
            //         refresh_all();
            //         $scope.productinfo = {
            //             "subCategoryName": '',
                        
                      
            //             "userId": Session.user.user
                      
            //         };
            //     } else {
            //         Notification({
            //             message: response.msg
            //         }, 'error');
            //     }
    
            // }, function (error) {
            //     Notification({
            //         message: error.msg
            //     }, 'error');
            // }).finally(function () {
            //     angular.element('#edithotel').modal('hide');
            //     $scope.edit_hotel_processing = false;
                
            // });
        }

    };


     //Delete SubCategory

    $scope.deleteSubCategory = function (subCategoryinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
         
             subCategoryId: subCategoryinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
        //  Hotel.delete(updated_data).then(function (response) {
        //      if (response.success) {
        //          Notification({
        //              message: response.msg
        //          }, 'success');
        //          refresh_all();
        //      } else {
        //          Notification({
        //              message: response.msg
        //          }, 'error');
        //      }
 
        //  }, function (error) {
        //      Notification({
        //          message: error.msg
        //      }, 'error');
        //  }).finally(function () {
        //     // angular.element('#editexpensehead').modal('hide');
        //      //$scope.edit_expensehead_processing = false;
        //      alert("Trend deleted successfully");
        //  });
 
     };

     //Delete VendorCategory

    $scope.deleteVendorCategory = function (vendorcategoryinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
            
            vendorCategoryId: vendorcategoryinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Operators.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Trend deleted successfully");
         });
 
     };

     //Delete Store

    $scope.deleteStore = function (storeinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
           
             storeId: storeinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Stores.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Trend deleted successfully");
         });
 
     };

     $scope.deleteVendor = function (vendorinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
           
             vendorId: vendorinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Vendor.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Vendor deleted successfully");
         });
 
     };




    //add vendor
    $scope.add_vendor = function (vendorinfo) {
        $scope.new_plan_processing = true;
        vendorinfo.userId=Session.user.user;
var BillerCategory = vendorinfo.billerCategory;
        // alert("fileService"+fileService);
        var file = fileService[0];
        if (file != undefined) {
        }else{
            alert("Please Upload an Image");
        }
        var vendor = new FormData();
            
        vendor.append('userId',Session.user.user);
        vendor.append('attachment',file);
        vendor.append('billerId',vendorinfo.billerId);
        vendor.append('billerName',vendorinfo.billerName);
        vendor.append('billerCategory',vendorinfo.billerCategory);
        vendor.append('billerAdhoc',vendorinfo.billerAdhoc);
        vendor.append('billerCoverage',vendorinfo.billerCoverage);
        vendor.append('billerFetchRequiremet',vendorinfo.billerFetchRequiremet);
        vendor.append('inputParam',vendorinfo.inputParam);
        vendor.append('billerPaymentExactness',vendorinfo.billerPaymentExactness);
        vendor.append('billerSupportBillValidation',vendorinfo.billerSupportBillValidation);
        
        
       


       

        BillerCategory.createn(vendor).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.vendorinfo = {
                    "billerId": '',
                    "billerName": '',
                    "billerCategory": '',
                    "billerAdhoc":'',
                    "billerCoverage":'',
                    "billerFetchRequiremet":'',
                    "inputParam":'',
                    "billerPaymentExactness":'',
                    "billerSupportBillValidation":'',
                    "userId": Session.user.user
                    
                    
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            fileService.length=0;
            $scope.new_plan_processing = false;
        });

    };
    //end 
    //changes start
    //edit vendor
    $scope.edit_vendor = function (vendorinfo) {
        $scope.edit_vendor_processing = true;
//test
        var file = fileService[0];
        if (file != undefined) {
            var updated_data = new FormData();
            var subCatImg=true;
            updated_data.append('userId',Session.user.user);
            updated_data.append('attachment',file);
            updated_data.append('vendorCategoryId',vendorinfo.vendorCategoryId._id);
            updated_data.append('vendorCategoryName',vendorinfo.vendorCategoryId.vendorCategoryName);
            updated_data.append('vendorId',$scope.selectedVendorId);
            updated_data.append('subCatImg',subCatImg);
            
      
       
       
            Vendor.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.subCategoryinfo = {
                    "productName": '',
                   
                   
                  
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editvendor').modal('hide');
            $scope.edit_vendor_processing = false;
            fileService.length=0;
        });
        }
        else
            {
           
           
            var updated_data = new FormData();
            var subCatImg=false;
            updated_data.append('userId',Session.user.user);
       
            updated_data.append('vendorCategoryId',vendorinfo.vendorCategoryId._id);
            updated_data.append('vendorCategoryName',vendorinfo.vendorCategoryId.vendorCategoryName);
            updated_data.append('vendorId',$scope.selectedVendorId);
            updated_data.append('subCatImg',subCatImg);

            Vendor.update(updated_data).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                    $scope.productinfo = {
                        "productName": '',
                        
                      
                        "userId": Session.user.user
                      
                    };
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                angular.element('#editvendor').modal('hide');
                $scope.edit_vendor_processing = false;
                
            });
        }
       

    };

    $scope.edit_vendorcategory = function (vendorcategoryinfo) {
        $scope.edit_vendor_processing = true;
//test
console.log(vendorcategoryinfo);
        var file = fileService[0];
        if (file != undefined) {
            var updated_data = new FormData();
            var subCatImg=true;
            updated_data.append('userId',Session.user.user);
            updated_data.append('attachment',file);
            updated_data.append('vendorCategoryId',$scope.selectedVendorCategoryId);
            updated_data.append('vendorCategoryName',vendorcategoryinfo.vendorCategoryName);
          
            updated_data.append('subCatImg',subCatImg);
            
      
       
       
            Operators.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.subCategoryinfo = {
                    "vendorCategoryName": '',
                   
                   
                  
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editvendorcategory').modal('hide');
            $scope.edit_vendor_processing = false;
            fileService.length=0;
        });
        }
        else
            {
           
           
            var updated_data = new FormData();
            var subCatImg=false;
            updated_data.append('userId',Session.user.user);
       
            
            updated_data.append('vendorCategoryName',vendorcategoryinfo.vendorCategoryName);
            updated_data.append('vendorCategoryId',$scope.selectedVendorCategoryId);
            updated_data.append('subCatImg',subCatImg);

            Operators.update(updated_data).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                    $scope.productinfo = {
                        "vendorCategoryName": '',
                        
                      
                        "userId": Session.user.user
                      
                    };
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                angular.element('#editvendorcategory').modal('hide');
                $scope.edit_vendor_processing = false;
                
            });
        }
        

    };
    
    //Delete functions
    
    
    //------------------------------------------------------------------- Edit Functions
    $scope.edit_user = function (userinfo) {
        $scope.edit_user_processing = true;
        var updated_data = {
            userId: userinfo._id,
            clientId: userinfo.clientId,
            fullName: userinfo.fullName,
            contactNum: userinfo.contactNum,
            homeNum: userinfo.homeNum,
            departmentId: userinfo.departmentId._id,
            departmentName: userinfo.departmentId.departmentName,
            employeeGradeId: userinfo.employeeGradeId._id,
            employeeGrade: userinfo.employeeGradeId.employeeGrade,
            employeeAddress: userinfo.employeeAddress,
            emailId: userinfo.emailId,
            hodId:userinfo.hodId,
            currentWorkingCityId: userinfo.currentWorkingCityId._id,
            bankName:userinfo.bankNameId._id,
            bankIfscCode:userinfo.bankIfscCode,
            bankCityName:userinfo.bankCityName,
            bankAccountNumber:userinfo.bankAccountNumber,
            typeOfAccount:userinfo.typeOfAccount,
            otherBankDetails:userinfo.otherBankDetails
        };

        Users.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {

            angular.element('#edituser').modal('hide');
            $scope.edit_user_processing = false;
        });

    };

   
    
     $scope.edit_product = function (productinfo) {
        $scope.edit_product_processing = true;
        
       
      
        var file = fileService[0];
        if (file != undefined) {
            var updated_data = new FormData();
            var subCatImg=true;
            updated_data.append('userId',Session.user.user);
            updated_data.append('attachment',file);
            updated_data.append('productName',productinfo.productName);
            updated_data.append('productCost',productinfo.productCost);
            updated_data.append('productsell',productinfo.productsell);
            updated_data.append('productQuantity',productinfo.productQuantity);
            updated_data.append('productCategoryId',productinfo.productCategoryId);
            updated_data.append('productCategoryName',productinfo.productCategoryId.productCategoryName);
            updated_data.append('productId',$scope.selectedProductId)
      
       
       
        Plan.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.subCategoryinfo = {
                    "productName": '',
                   
                   
                  
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            angular.element('#editplan').modal('hide');
            // $scope.edit_hotel_processing = false;
            fileService.length=0;
        });
        }
        else
            {
           
           
            var updated_data = new FormData();
            var subCatImg=false;
            updated_data.append('userId',Session.user.user);
           
            updated_data.append('productName',productinfo.productName);
            updated_data.append('productCost',productinfo.productCost);
            updated_data.append('productQuantity',productinfo.productQuantity);
            updated_data.append('productCategoryId',productinfo.productCategoryId);
            updated_data.append('productCategoryName',productinfo.productCategoryId.productCategoryName);
            updated_data.append('productId',$scope.selectedProductId)
          
            Plan.update(updated_data).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                    $scope.productinfo = {
                        "productName": '',
                        
                      
                        "userId": Session.user.user
                      
                    };
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                angular.element('#editplan').modal('hide');
                // $scope.edit_hotel_processing = false;
                
            });
        }
        //new data
        

    };

    // Edit Feed

    $scope.edit_feed = function (newsinfo) {
        $scope.edit_policy_processing = true;

        var file = fileService[0];
        if (file != undefined) {
            var updated_data = new FormData();
            var subCatImg=true;
            updated_data.append('userId',Session.user.user);
            updated_data.append('attachment',file);
            updated_data.append('newsDescription',newsinfo.newsDescription);
            updated_data.append('newsTitle',newsinfo.newsTitle);
            updated_data.append('feedId',$scope.selectedFeedId);
            updated_data.append('subCatImg',subCatImg);

            Policy.update(updated_data).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                angular.element('#editpolicy').modal('hide');
                $scope.edit_policy_processing = false;
            });

        }else{

            var updated_data = new FormData();
            var subCatImg=false;
            updated_data.append('userId',Session.user.user);
            updated_data.append('attachment',file);
            updated_data.append('newsDescription',newsinfo.newsDescription);
            updated_data.append('newsTitle',newsinfo.newsTitle);
            updated_data.append('feedId',$scope.selectedFeedId);
            updated_data.append('subCatImg',subCatImg);

            Policy.update(updated_data).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                angular.element('#editpolicy').modal('hide');
                $scope.edit_policy_processing = false;
            });

        }
      

        

    };

    //Edit Store
    
    $scope.edit_store = function (storeinfo) {
        $scope.edit_policy_processing = true;
        var file = fileService[0];
        if (file != undefined) {
            var updated_data = new FormData();
            var subCatImg=true;
            updated_data.append('storeId',$scope.selectedStoreId);
            updated_data.append('userId',Session.user.user);
            updated_data.append('attachment',file);
            updated_data.append('storeName',storeinfo.storeName);
            updated_data.append('storeAddress',storeinfo.storeAddress);
            updated_data.append('storeContactNumber',storeinfo.storeContactNumber);
            updated_data.append('subCatImg',subCatImg);
            
            Stores.update(updated_data).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                angular.element('#editstore').modal('hide');
                $scope.edit_policy_processing = false;
                fileService.length=0;
            });
        }
        else{

            var updated_data = new FormData();
            var subCatImg=false;
            updated_data.append('userId',Session.user.user);
            updated_data.append('storeId',$scope.selectedStoreId);
            updated_data.append('storeName',storeinfo.storeName);
            updated_data.append('storeAddress',storeinfo.storeAddress);
            updated_data.append('storeContactNumber',storeinfo.storeContactNumber);
            updated_data.append('subCatImg',subCatImg);

            Stores.update(updated_data).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');
                    refresh_all();
                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }
    
            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                angular.element('#editstore').modal('hide');
                $scope.edit_policy_processing = false;
            });
        }
        
    };

    //

    


    //Delete Feed

    $scope.deleteOffer = function (feedinfo) {
        // $scope.edit_expensehead_processing = true;
         var updated_data = {
         
             offerId: feedinfo._id,
             userId: Session.user.user,
             clientId: Session.user.clientId
         };
 
         Offer.delete(updated_data).then(function (response) {
             if (response.success) {
                 Notification({
                     message: response.msg
                 }, 'success');
                 refresh_all();
             } else {
                 Notification({
                     message: response.msg
                 }, 'error');
             }
 
         }, function (error) {
             Notification({
                 message: error.msg
             }, 'error');
         }).finally(function () {
            // angular.element('#editexpensehead').modal('hide');
             //$scope.edit_expensehead_processing = false;
             alert("Feed deleted successfully");
         });
 
     };

   
    
    //changes start
    $scope.add_subCategory = function (subCategoryinfo) {
      
        // $scope.new_hotel_processing = true;
        
        console.log(subCategoryinfo);
        var file = fileService[0];
        if (file != undefined) {
        }else{ 
            alert("Please Upload an Image");
        }
        var subcat = new FormData();
            
        subcat.append('userId',Session.user.user);
        subcat.append('attachment',file);
        subcat.append('subCategoryName',$scope.subCategoryinfo.subCategoryName);
       
        subcat.append('subCategoryVendorId',$scope.subCategoryinfo.subCategoryVendorId._id);
        subcat.append('subCategoryVendorName',$scope.subCategoryinfo.subCategoryVendorId.vendorName);
        subcat.append('vendorCategoryId',subCategoryinfo.categoryVendorId._id);
        subcat.append('vendorCategoryName',subCategoryinfo.categoryVendorId.vendorCategoryName);

    


        // Hotel.createn(subcat).then(function (response) {
        //     if (response.success) {
        //         Notification({
        //             message: response.msg
        //         }, 'success');
        //         refresh_all();
        //         $scope.subCategoryinfo = {
        //             "subCategoryName": '',
        //             "subCategoryVendorId":'',
        //             "subCategoryVendorName":'',
        //             "vendorCategoryId":'',
        //             "vendorCategoryName":'',
                    
        //             "userId": Session.user.user,
        //             "isActive": true,
        //             "clientId": Session.user.clientId
        //         };
        //     } else {
        //         Notification({
        //             message: response.msg
        //         }, 'error');
        //     }
        // }, function (error) {
        //     Notification({
        //         message: error.msg
        //     }, 'error');
        // }).finally(function () {
        //     fileService.length=0;
        //     $scope.new_hotel_processing = false;
        // });

    };
    //changes end

    //------------------------------------------------------------------- Switches
    $scope.employee_tabs_switch = function () {
        $scope.employee_tabs = !$scope.employee_tabs;
    };

    $scope.operator_tabs_switch = function () {
        $scope.operator_tabs = !$scope.operator_tabs;
    };

    $scope.vendor_tabs_switch = function () {
        $scope.vendor_tabs = !$scope.vendor_tabs;
    };

    $scope.plan_tabs_switch = function () {
        $scope.plan_tabs = !$scope.plan_tabs;
    };

    $scope.city_tabs_switch = function () {
        $scope.city_tabs = !$scope.city_tabs;
    };
    $scope.prime_tabs_switch = function () {
        $scope.prime_tabs = !$scope.prime_tabs;
    };

    $scope.activeSub_tabs_switch = function () {
        $scope.activeSub_tabs = !$scope.activeSub_tabs;
    };

    $scope.subscription_tabs_switch = function () {
        $scope.subscription_tabs = !$scope.subscription_tabs;
    }; 

    $scope.bank_tabs_switch = function () {
        $scope.bank_tabs = !$scope.bank_tabs;
    };
    $scope.department_tabs_switch = function () {
        $scope.department_tabs = !$scope.department_tabs;
    };

    $scope.policy_tabs_switch = function () {
        $scope.edit_policy_employee_grades.length = 0;
        $scope.policy_employee_grades.length = 0;
        $scope.policy_tabs = !$scope.policy_tabs;
    };

    // $scope.hotel_tabs_switch = function () {
    //     $scope.hotel_tabs = !$scope.hotel_tabs;
    // };
    
    
    

    //------------------------------------------------------------------- Modal Populator
    $scope.employee_modal_populator = function (data) {
        $scope.edit_employee_data = data;
        $scope.edit_employee_data.departmentId = {
            _id: data.departmentId,
            departmentName: data.departmentName
        }
        $scope.edit_employee_data.employeeGradeId = {
            _id: data.employeeGradeId,
            departmentName: data.employeeGrade
        }
        $scope.edit_employee_data.currentWorkingCityId = {
            _id: data.currentWorkingCityId
        }
         $scope.edit_employee_data.bankNameId = {
            _id: data.bankName
        }
         $scope.edit_employee_data.hodId ={
             _id:data.hodId
         }
         $scope.editusermode=true;
         refresh_all();
    };

    $scope.department_modal_populator = function (data) {
        $scope.edit_department_data = data;
        $scope.edit_department_data.hodId = {
            _id: data.hodId,
            fullName: data.hodName
        }
    };
    $scope.expensehead_modal_populator = function (data) {
        $scope.edit_expensehead_data = data;
    };
    $scope.employeegrade_modal_populator = function (data) {
       
        $scope.edit_employeegrade_data = data;
    };
    
      $scope.city_modal_populator = function (data) {
        $scope.edit_city_data = data;
    };

    $scope.vendor_modal_populator = function (data) {
        $scope.edit_vendor_data = data;
    };

    $scope.subscription_modal_populator = function (data) {
        $scope.edit_subscription_data = data;
        $scope.selectedSubscriptionId = data._id;
    };

      $scope.bank_modal_populator = function (data) {
        $scope.edit_bank_data = data;
    };
    $scope.operator_modal_populator = function (data) {
        $scope.edit_operator_data = data;
    };
      $scope.client_modal_populator = function (data) {
        $scope.edit_client_data = data;
            $scope.edit_client_data.cityId = {
            _id: data.cityId,
            cityName: data.cityName
        }
    };
      $scope.product_modal_populator = function (data) 
      {
          $scope.selectedProductId = data._id;
        $scope.edit_product_data = data;
    };
    $scope.vendor_modal_populator = function (data) 
      {
          $scope.selectedVendorId = data._id;
          $scope.selectedVendorName = data.vendorName;
        $scope.edit_vendor_data = data;
    };
    $scope.vendorcategory_modal_populator = function (data) 
      {
          $scope.selectedVendorCategoryId = data._id;
          $scope.selectedVendorCategoryName = data.vendorCategoryName;
        $scope.edit_vendorcategory_data = data;
    };

    $scope.feed_modal_populator = function (data) 
    {
        $scope.selectedFeedId = data._id;
        
      $scope.edit_feed_data = data;
  };

  $scope.subCategoryData_modal_populator = function (data) 
      {
          $scope.selectedsubCategoryId = data._id;
        $scope.edit_subCategory_data = data;
    };

  $scope.Store_modal_populator = function (data) 
  {
      $scope.selectedStoreId = data._id;
      
    $scope.edit_store_data = data;
};

  $scope.trend_modal_populator = function (data) 
  {
      $scope.selectedTrendId = data._id;
      
    $scope.edit_trend_data = data;
};

  $scope.delete_feed = function (data) 
  {
      $scope.selectedFeedId = data._id;
      
    $scope.delete_feed_data = data;
};

// $scope.updateData = function(vendor) { 
//     console.log(vendor._id);
//     console.log(vendor.vendorName);

// }
    
    

    // $scope.hotel_modal_populator = function (data) {
    //     $scope.edit_hotel_data = data;
    //    // $scope.edit_hotel_data.hotelcityId = data.hotelCityId;
    // };

    

    //------------------------------------------------------------------- Extra
    $scope.policy_emp_checker = function (gkey, gvalue) {
        if (gvalue) {
            $scope.policy_employee_grades.push($scope.employeeGrades[gkey]._id);
        } else {
            $scope.policy_employee_grades.splice(gkey, 1);
        }
    };

    $scope.edit_policy_emp_checker = function (gkey, gvalue) {
        if (gvalue) {
            $scope.edit_policy_employee_grades.push($scope.employeeGrades[gkey]._id);
        } else {
            $scope.edit_policy_employee_grades.splice(gkey, 1);
        }
    };

    refresh_all();
});











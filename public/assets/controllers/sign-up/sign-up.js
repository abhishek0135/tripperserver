app.controller('SignupCtrl', function ($scope, $localStorage, $rootScope, AUTH_EVENTS, AuthSignup, Auth, UI_EVENTS, Notification) {
    $scope.userinfo = {
        fullName: '',
        password: '',
        contactNum: '',
        emailId: '',
        employeeAddress: '',
        clientId: 'superadmin', 
         accessLevelName: 'superadmin',
        isSuperAdmin: true,
    };

    $scope.userinfo_comp = {
        fullName: '',
        password: '',
        contactNum: '',
        emailId: '',
        employeeAddress: '',
        clientId: 'superadmin',
        clientName: '',
        accessLevelId: '',
        accessLevelName: 'superadmin',
        isSuperAdmin: true,
        departmentId: '',
        departmentName: '',
        employeeId: '',
        employeeGrade: '',
        employeeGradeId: ''
    };

    $scope.processing = false;

    $scope.signup = function (userinfo, c_password) {
        $scope.processing = true;

       // if (!userinfo.password.match(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9\.\-\_]+){6,15}$/g)) {
            // Notification({
            //     message: 'Password must contain at least one digit and be between 6 and 15 characters long.'
            // }, 'warning');
            // $scope.processing = false;
       // } 
        if (userinfo.password != c_password) {
            Notification({
                message: 'Password mismatch!'
            }, 'error');
            console.log("Password mismatch!");
            $scope.processing = false;
        } else {

            AuthSignup.signup(userinfo).then(function (response) {
                if (response.success) {
                    Notification({
                        message: response.msg
                    }, 'success');

                    var credentials = {
                        contactNum: userinfo.contactNum,
                        password: userinfo.password
                    };

                    Auth.login(credentials).then(function (response) {
                        if (response.success) {
                            Notification({
                                message: response.msg
                            }, 'success');
                        } else {
                            Notification({
                                message: response.msg
                            }, 'error');
                        }
                    }, function (error) {
                        Notification({
                            message: error.msg
                        }, 'error');
                    });

                } else {
                    Notification({
                        message: response.msg
                    }, 'error');
                }

            }, function (error) {
                Notification({
                    message: error.msg
                }, 'error');
            }).finally(function () {
                $scope.processing = false;
            });
        }
    }

});

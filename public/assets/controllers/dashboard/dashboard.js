app.controller('DashboardCtrl', function ($scope,$state, $document, FlickityService, Session, DTOptionsBuilder, DTColumnDefBuilder, Notification, GetUserDashboard,GetVendorDashboard, Users) {
    $scope.trips_to_copy =[];
    if (angular.equals(Session.user.userRole, 'basic') || angular.equals(Session.user.userRole, 'employee')) {
        $scope.template = {
            name: 'normal-dashboard',
            url: 'templates/dashboard/dashboards/normal-dashboard.html'
        }
    } else if (angular.equals(Session.user.userRole, 'hod')) {
        $scope.template = {
            name: 'hod-dashboard',
            url: 'templates/dashboard/dashboards/hod-dashboard.html'
        }
        $scope.dtOptionsTravelRequests = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(5);
        $scope.dtColumnDefsTravelRequests = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5),
            DTColumnDefBuilder.newColumnDef(6),
            DTColumnDefBuilder.newColumnDef(7),
            DTColumnDefBuilder.newColumnDef(8),
            DTColumnDefBuilder.newColumnDef(9)
        ];
        $scope.dtOptionsMyRequests = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(5);
        $scope.dtColumnDefsMyRequests = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5)
        ];
    } else if (angular.equals(Session.user.userRole, 'admin')) {
        $scope.template = {
            name: 'admin-dashboard',
            url: 'templates/dashboard/dashboards/admin-dashboard.html'
        }
        $scope.dtOptionsTravelRequests = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(5);
        $scope.dtColumnDefsTravelRequests = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5),
            DTColumnDefBuilder.newColumnDef(6),
            DTColumnDefBuilder.newColumnDef(7),
            DTColumnDefBuilder.newColumnDef(8),
            DTColumnDefBuilder.newColumnDef(9),
            DTColumnDefBuilder.newColumnDef(10)
        ];
        $scope.dtOptionsMyRequests = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(5);
        $scope.dtColumnDefsMyRequests = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5)
        ];
    } else if (angular.equals(Session.user.userRole, 'accounts')) {
        $scope.template = {
            name: 'accounts-dashboard',
            url: 'templates/dashboard/dashboards/accounts-dashboard.html'
        }
        $scope.dtOptionsTravelRequests = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(5);
        $scope.dtColumnDefsTravelRequests = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5),
            DTColumnDefBuilder.newColumnDef(6),
            DTColumnDefBuilder.newColumnDef(7),
            DTColumnDefBuilder.newColumnDef(8),
            DTColumnDefBuilder.newColumnDef(9)
        ];
        $scope.dtOptionsMyRequests = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(5);
        $scope.dtColumnDefsMyRequests = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5)
        ];
    } else if (angular.equals(Session.user.userRole, 'vendor')) {
        $scope.template = {
            name: 'vendor-dashboard',
            url: 'templates/dashboard/dashboards/vendor-dashboard.html'
        }
        $scope.dtOptionsTravelRequests = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(5);
        $scope.dtColumnDefsTravelRequests = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5),
            DTColumnDefBuilder.newColumnDef(6),
            DTColumnDefBuilder.newColumnDef(7),
            DTColumnDefBuilder.newColumnDef(8)
        ];
        $scope.dtOptionsMyRequests = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(5);
        $scope.dtColumnDefsMyRequests = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5)
        ];
    }
    else if (angular.equals(Session.user.userRole, 'superadmin')) {
        $state.go("app.settings");
//        $scope.template = {
//            name: 'settings',
//            url: 'templates/settings/settings.html'
//        }
    }

    $scope.processing = true;
    $scope.response = {};
    $scope.expenses_array = [];
    $scope.trips_array = [];
    $scope.recent_expenses = [];
    $scope.trips = [];
    $scope.business = [];
    $scope.personal = [];
    $scope.users = [];
    $scope.re_selected = 0;
    $scope.lt_selected = 0;
    Users.getAll().then(function (users) {
        if (users.success) {
            $scope.users = angular.copy(users.users);
        } else {
            Notification({
                message: users.msg
            }, 'error');
        }
    });
    // -----------------------------------   Charts Options

    $scope.options_d_chart_total = {
        unit: "%",
        readOnly: true,
        size: 70,
        textColor: '#E7505A',
        trackWidth: 6,
        barWidth: 5,
        trackColor: '#EFD2D3',
        barColor: '#E7505A'
    };

    $scope.options_d_chart_pending = {
        unit: "%",
        readOnly: true,
        size: 70,
        textColor: '#33CC7E',
        trackWidth: 6,
        barWidth: 5,
        trackColor: '#CFF2E1',
        barColor: '#33CC7E'
    };

    $scope.pie_chart_top = {
        total_expenses: 0,
        pending_expenses: 0,
        total_trips: 0,
        pending_trips: 0
    };

    $scope.flickityOptions = {
        cellSelector: '.trips',
        initialIndex: 1,
        prevNextButtons: true,
        pageDots: false
    };
    $scope.currentWorkingCityId = Session.user.currentWorkingCity;
    $scope.currentWorkingCity = '';

    // -----------------------------------   Get Dashboard Data Function
    GetUserDashboard.get().then(function (response) {
            if (response.hasOwnProperty("expenseHeads")) {
                Session.saveExpenseHeads(response.expenseHeads);
            } else if (response.hasOwnProperty("expenseheads")) {
                Session.saveExpenseHeads(response.expenseheads);
            }
         if (response.hasOwnProperty("banks")) {
                Session.saveBanks(response.banks);
            } 
        
            $scope.processing = false;
            $scope.response = response;
            // -----------------------------------   Total and Pending trips and percentage
            var count = 0;
            if (response.hasOwnProperty("trip")) {
                if (response.trip.length != 0) {
                    $scope.trips_array = response.trip;
                    $scope.trips_to_copy = angular.copy($scope.trips_array);
                     Session.saveTrips(response.trip);
                } else {
                    $scope.trips_array = [];
                }
            } else if (response.hasOwnProperty("trips")) {
                if (response.trips.length != 0) {
                    $scope.trips_array = response.trips;
                    $scope.trips_to_copy = angular.copy($scope.trips_array);
                    Session.saveTrips(response.trips);
                } else {
                    $scope.trips_array = [];
                }
            }

            angular.forEach($scope.trips_array, function (value, key) {
                angular.forEach($scope.users, function (value1, key1) {
                    if (angular.equals(value1._id, value.userId)) {
                        $scope.trips_array[key].requestedBy = value1.fullName;
                    }
                });
            });

            angular.forEach($scope.trips_array, function (value, key) {
               if (angular.equals(Session.user.userRole, 'admin')) 
                {
                    if (angular.equals(value.tripStatus, "approved-hod") || angular.equals(value.tripStatus, "pending-admin")) {
                    count += 1;
                }
                }
                if  (angular.equals(Session.user.userRole, 'hod')) 
                    {
                        if (angular.equals(value.tripStatus, "pending-hod") || angular.equals(value.tripStatus, "pending")) 
                        {
                        count += 1;
                        }
                        
                    }
                 if  (angular.equals(Session.user.userRole, 'accounts')) 
                    {
                        if (angular.equals(value.tripStatus, "pending-accounts") || angular.equals(value.tripStatus, "approved-admin")) 
                        {
                        count += 1;
                        }
                        
                    }
                
            });

            var pending_trips_percentage = (count / $scope.trips_array.length) * 100;
            $scope.pending_trips = count;
            // -----------------------------------   Trips Days Left Extraction
            var oneDay = 24 * 60 * 60 * 1000;
            var current_date = new Date();
            current_date.setUTCHours(0, 0, 0, 0);

            angular.forEach($scope.trips_array, function (value, key) {
                var start_date = new Date(value.tripStartDate);
                start_date.setUTCHours(0, 0, 0, 0);
                 var diffDays = Math.round((start_date.getTime() - current_date.getTime()) / (oneDay));
                if (diffDays > 0) {
                    
                    var diffDays = Math.round(Math.abs((start_date.getTime() - current_date.getTime()) / (oneDay)));
                    $scope.trips_array[key].daysLeft = diffDays;
                } else {
                    $scope.trips_array[key].daysLeft = 0;
                }
            });

            $scope.trips = $scope.trips_array;

            // -----------------------------------   Total and Pending transactions and percentage
            count = 0;
            $scope.business.length = 0;
            $scope.personal.length = 0;

            if (response.hasOwnProperty("transactionDetails")) {
                $scope.expenses_array = response.transactionDetails;
            } else if (response.hasOwnProperty("transactions")) {
                $scope.expenses_array = response.transactions;
            } else if (response.hasOwnProperty("transaction")) {
                $scope.expenses_array = response.transaction;
            }

            angular.forEach($scope.expenses_array, function (value, key) {
                angular.forEach($scope.users, function (value1, key1) {
                    if (angular.equals(value1._id, value.userId)) {
                        $scope.expenses_array[key].requestedBy = value1.fullName;
                    }
                });
            });

            angular.forEach(Session.citiesArray, function (value, key) {
                if (angular.equals(value._id, $scope.currentWorkingCityId)) {
                    $scope.currentWorkingCity = value.cityName;
                }
            })

            angular.forEach($scope.expenses_array, function (value, key) {
                if (!value.isAdminApproved) {
                    count += value.transactionAmount;
                }
                if (angular.equals(value.tripId, 'general')) {
                    $scope.expenses_array[key].toCity = $scope.currentWorkingCity;
                }
                if (angular.equals(value.travelPurpose, "Business")) {
                    $scope.business.push(value);
                } else if (angular.equals(value.travelPurpose, "Personal")) {
                    $scope.personal.push(value);
                }
            });

            var pending_expenses_percentage = (count / response.transactionAmount) * 100;
            $scope.pending_expenses = count;

            pending_expenses_percentage = Number((pending_expenses_percentage).toFixed(0));
            pending_trips_percentage = Number((pending_trips_percentage).toFixed(0));

            $scope.pie_chart_top = {
                total_expenses: 100,
                pending_expenses: pending_expenses_percentage,
                total_trips: 100,
                pending_trips: pending_trips_percentage
            };

            $scope.re_select_range(0);

            // -----------------------------------   Business Expenses
            var b_conveyance = 0;
            var b_food = 0;
            var b_travel = 0;

            angular.forEach($scope.business, function (value, key) {
                if (angular.equals(value.expenseHeadName, "Conveyance Expenses")) {
                    b_conveyance += value.transactionAmount;
                } else if (angular.equals(value.expenseHeadName, "Food")) {
                    b_food += value.transactionAmount;
                } else if (angular.equals(value.expenseHeadName, "Travelling Inland Fare") || angular.equals(value.expenseHeadName, "Travelling Inland Lodging")) {
                    b_travel += value.transactionAmount;
                }
            });

            $scope.business_expenses = [{
                label: "Conveyance",
                value: b_conveyance
            }, {
                label: "Food",
                value: b_food
            }, {
                label: "Travel",
                value: b_travel
            }];

            // -----------------------------------   Personal Expenses
            var p_rent = 0;
            var p_food = 0;
            var p_travel = 0;

            angular.forEach($scope.personal, function (value, key) {
                if (angular.equals(value.expenseHeadName, "Gift Expenses")) {
                    p_rent += value.transactionAmount;
                } else if (angular.equals(value.expenseHeadName, "Food")) {
                    p_food += value.transactionAmount;
                } else if (angular.equals(value.expenseHeadName, "travel") || angular.equals(value.expenseHeadName, "Travel")) {
                    p_travel += value.transactionAmount;
                }
            });

            $scope.personal_expenses = [{
                label: "Gift",
                value: p_rent
            }, {
                label: "Food",
                value: p_food
            }, {
                label: "Travel",
                value: p_travel
            }];

            // -----------------------------------   Admin, HOD  Dashboard
            if (angular.equals(Session.user.userRole, 'hod') || angular.equals(Session.user.userRole, 'admin') ||  angular.equals(Session.user.userRole, 'accounts')) {

                $scope.percentages = response.percentages;
              //  alert(JSON.stringify($scope.percentages));
                if($scope.percentages==null||$scope.percentages==undefined)
                    {
                            $scope.ispercentdata=false;
                    }
                else if($scope.percentages[1].travel==null || $scope.percentages[1].travel==undefined || $scope.percentages[0].food==null||$scope.percentages[0].food==undefined )
                    {
                            var b = 0;
                            var f = 0;
                            var c = 0;
                            $scope.ispercentdata=false;
                           // alert("here");
                    }
                else
                    {
                            $scope.ispercentdata=true;
                            var b = response.percentages[1].travel;
                            var f = response.percentages[0].food;
                            var c = response.percentages[2].other;
                          //  alert("there");
                    }
               
              //  alert(b);
                b = Number((b).toFixed(0));
                f = Number((f).toFixed(0));
                c = Number((c).toFixed(0));
                $scope.percentages_options_expenses = [{
                    label: "Travel",
                    value: b
                }, {
                    label: "Food",
                    value: f
                }, {
                    label: "Other",
                    value: c
                }];

                var pending_trips_percentage = ($scope.pending_trips / $scope.trips_array.length) * 100;
                $scope.pending_trips_percentage = Number((pending_trips_percentage).toFixed(0));
                $scope.approved_trips = $scope.trips_array.length - $scope.pending_trips;
                var approved_trips_percentage = ($scope.approved_trips / $scope.trips_array.length) * 100;
                $scope.approved_trips_percentage = Number((approved_trips_percentage).toFixed(0));
//                if ($scope.trips_array.length==0||$scope.trips_array.length=null||$scope.trips_array.length==undefined)
//                    {
//                        $scope.displaytrip=false;
//                    }
//                else
//                    {
//                        $scope.displaytrip=true;
//                    }

                $scope.percentages_options_trips = [
                    {
                        label: "Pending",
                        value: $scope.pending_trips_percentage
                    }, {
                        label: "Approved",
                        value: $scope.approved_trips_percentage
                    }
                ];


                $scope.trips_to_copy.push({
                    total: response.nooftrips
                });

                $scope.trips_to_copy = JSON.stringify($scope.trips_to_copy);
                $scope.my_requests_to_copy = angular.copy(response.transactions);
                $scope.my_requests_to_copy.push({
                    total: response.nooftransactions
                });
                $scope.my_requests_to_copy = JSON.stringify($scope.my_requests_to_copy);


                var count = 0;
                var t_count = 0;
                angular.forEach(response.transactions, function (value, key) {
                    if (angular.equals(value.expenseHeadName, "Travel") || angular.equals(value.expenseHeadName, "travel")) {
                        count += value.transactionAmount;
                    } else {
                        t_count += value.transactionAmount;
                    }
                });


                $scope.hod_travel_total = count;
                $scope.hod_expenses_total = t_count;

                var hod_expenses_travel_total = $scope.hod_travel_total + $scope.hod_expenses_total;

                var travel_percentage = 0;
                travel_percentage = ($scope.hod_travel_total / hod_expenses_travel_total) * 100;
                travel_percentage = Number((travel_percentage).toFixed(0));

                var expenses_percentage = 0;
                expenses_percentage = ($scope.hod_expenses_total / hod_expenses_travel_total) * 100;
                expenses_percentage = Number((expenses_percentage).toFixed(0));
                if(travel_percentage==null||travel_percentage==NaN||travel_percentage==""||expenses_percentage==null||expenses_percentage==NaN||expenses_percentage=="")
                    {
                        $scope.showdonut=false;
                    }
                else{
                    $scope.showdonut=true;
                }

                $scope.percentages_options_total = [
                    {
                        label: "Travel",
                        value: travel_percentage
                    }, {
                        label: "Expense",
                        value: expenses_percentage
                    }
                ];

            }

        },
        function (error) {
            Notification({
                message: "Error fetching data. Please check your internet connection."
            }, 'error');
        });

    // -----------------------------------   Filters
    $scope.custom_format = function (input) {
        return '₹' + input;
    };

    $scope.percent_format = function (input) {
        return input + '%';
    };

    $scope.re_select_range = function (filter) {

        var current_date = new Date();
        current_date.setUTCHours(0, 0, 0, 0);
        $scope.recent_expenses.length = 0;
        $scope.re_selected = filter;
        if (filter == 0) {
            angular.forEach($scope.expenses_array, function (value, key) {
                var created_date = new Date(value.createdTime);
                created_date.setUTCHours(0, 0, 0, 0);
                if (current_date.getDate() == created_date.getDate()) {
                    $scope.recent_expenses.push(value);
                }
            });
        } else if (filter == 1) {
            angular.forEach($scope.expenses_array, function (value, key) {
                var created_date = new Date(value.createdTime);
                created_date.setUTCHours(0, 0, 0, 0);

                for (var i = 0; i < 7; i++) {
                    current_date.setDate(created_date.getDate() - i)
                    if (current_date.getDate() == created_date.getDate()) {
                        $scope.recent_expenses.push(value);
                        break;
                    }
                }
            });
        } else if (filter == 2) {
            angular.forEach($scope.expenses_array, function (value, key) {

                var created_date = new Date(value.createdTime);
                created_date.setUTCHours(0, 0, 0, 0);

                for (var j = 0; j < 32; j++) {
                    current_date.setDate(created_date.getDate() - j)
                    if (current_date.getDate() == created_date.getDate()) {
                        $scope.recent_expenses.push(value);
                        break;
                    }
                }
            });
        };
    };

    $scope.lt_select_range = function (filter) {
        var current_date = new Date();
        current_date.setUTCHours(0, 0, 0, 0);
        $scope.lt_selected = filter;
        $scope.trips.length = 0;
        if (filter == 0) {
            angular.forEach($scope.trips_array, function (value, key) {
                var created_date = new Date(value.createdTime);
                created_date.setUTCHours(0, 0, 0, 0);
                if (current_date.getDate() == created_date.getDate()) {
                    $scope.trips.push(value);
                }
            });
        } else if (filter == 1) {
            angular.forEach($scope.trips_array, function (value, key) {
                var created_date = new Date(value.createdTime);
                created_date.setUTCHours(0, 0, 0, 0);

                for (var i = 0; i < 7; i++) {
                    current_date.setDate(created_date.getDate() - i)
                    if (current_date.getDate() == created_date.getDate()) {
                        $scope.trips.push(value);
                        break;
                    }
                }
            });
        } else if (filter == 2) {
            angular.forEach($scope.trips_array, function (value, key) {
                var created_date = new Date(value.createdTime);
                created_date.setUTCHours(0, 0, 0, 0);
                for (var i = 0; i < 31; i++) {
                    current_date.setDate(created_date.getDate() - i)
                    if (current_date.getDate() == created_date.getDate()) {
                        $scope.trips.push(value);
                        break;
                    }
                }
            });
        };

    };

    $scope.copy_success = function () {
        Notification({
            message: "Table Copied!"
        }, 'success');
    };

    $scope.download_csv_trips = function (text) {
        alasql('SELECT * INTO CSV("Trips_Report.csv",{headers:true}) FROM ?', [text]);
    };

    $scope.download_xls_trips = function (text) {
        alasql('SELECT * INTO XLSX("Trips_Report.xlsx",{headers:true}) FROM ?', [text]);
    };

    $scope.download_pdf_trips = function (text) {
        var doc = new jsPDF();
        doc.setFontSize(4);
        text.forEach(function (value, i) {
            var date = new Date(value.createdTime);
            var parsedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear(); //prints expected format

            var date_trip = new Date(value.tripStartDate);
            var parsedDateEndDate = date_trip.getDate() + '/' + (date_trip.getMonth() + 1) + '/' + date_trip.getFullYear(); //prints expected format

            doc.text(10, 10 + (i * 10),
                "Request On: " + parsedDate +
                "  |  Purpose: " + value.travelPurpose +
                "  |  Requested By: " + value.fullName +
                "  |  End Date: " + parsedDateEndDate +
                "  |  From City: " + value.fromCity +
                "  |  To City: " + value.toCity +
                "  |  Travel Mode Type: " + value.travelClass + "-" + value.travelModeName +
                "  |  Ticket Fair(INR): " + value.budgetTo +
                "  |  Status: " + value.tripStatus);
        });
        doc.save('Trips_Report.pdf');
    };

    $scope.download_csv_my_requests = function (text) {
        alasql('SELECT * INTO CSV("My_Requests.csv",{headers:true}) FROM ?', [text]);
    };

    $scope.download_xls_my_requests = function (text) {
        alasql('SELECT * INTO XLSX("My_Requests.xlsx",{headers:true}) FROM ?', [text]);
    };

    $scope.download_pdf_my_requests = function (text) {
        var doc = new jsPDF();
        doc.setFontSize(4);
        text.forEach(function (value, i) {
            var date = new Date(value.createdTime);
            var parsedDateFrom = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();

            var date = new Date(value.createdTime);
            var parsedDateTo = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();

            doc.text(10, 10 + (i * 10),
                "Claim Type: " + value.expenseHeadName +
                "  |  From: " + parsedDateFrom +
                "  |  To: " + parsedDateTo +
                "  |  City: " + value.toCity +
                "  |  Amount(INR): " + value.transactionAmount);
        });
        doc.save('My_Requests.pdf');
    };

    $scope.download_pdf_trips_vendor = function (text) {
        var doc = new jsPDF();
        doc.setFontSize(4);
        text.forEach(function (value, i) {
            var date = new Date(value.tripStartDate);
            var parsedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear(); // for date

            var date_timefrom = new Date(value.travelTimefrom);
            var parsedTimeFrom = date_timefrom.getUTCHours() + ':' + date_timefrom.getUTCMinutes(); //for time

            var date_timeto = new Date(value.travelTimeto);
            var parsedTimeTo = date_timeto.getUTCHours() + ':' + date_timeto.getUTCMinutes(); //for time

            doc.text(10, 10 + (i * 10),
                "From Company: " + "" +
                "  |  Persons: " + 1 +
                "  |  Place: " + value.fromCity + "-" + value.toCity +
                "  |  On Date: " + parsedDate +
                "  |  Depart Between: " + parsedTimeFrom + "-" + parsedTimeTo +
                "  |  Travel Mode Type: " + value.travelClass + "-" + value.travelModeName +
                "  |  Budget: " + value.budgetTo +
                "  |  Status: " + value.tripStatus);
        });
        doc.save('Trips_Report.pdf');
    };

});





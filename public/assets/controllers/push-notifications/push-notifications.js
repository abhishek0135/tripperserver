app.controller('PushNotificationsCtrl', function ($scope, Session, $state, Users,Notifications,GetUserDashboard) {
    $scope.particularSelected=false;
    $scope.notify_tabs = false;


    $scope.sendNotification = function (notificationinfo) {
        console.log(notificationinfo);
        $scope.edit_notification_processing = true;
        notificationinfo.userId=Session.user.user;

        // alert("fileService"+fileService);
       if(notificationinfo.selectOption=="AU"){
        var notificationsdata = {"userId":Session.user.user,"title":notificationinfo.title,"description":notificationinfo.description,
        "selectOption":notificationinfo.selectOption};
       

        Notifications.createn(notificationsdata).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.notificationinfo = {
                    title: '',
                    description:'',
                    selectOption: '',
                    deviceKey:'',
                 
                    
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            $scope.edit_notification_processing= false;
           
        });
       
       }else{

        var notificationsdata = {"userId":Session.user.user,"title":notificationinfo.title,"description":notificationinfo.description,
        "selectOption":notificationinfo.selectOption,
        "deviceKey":notificationinfo.deviceKey};

       


        Notifications.createn(notificationsdata).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
                $scope.notificationinfo = {
                    title: '',
                    description:'',
                    selectOption: '',
                    deviceKey:'',
                 
                    
                    "userId": Session.user.user
                  
                };
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {
            $scope.edit_notification_processing= false;
           
        });
       }
       

      
      
        

  
    }

    $scope.setOption = function (x) {
        console.log(x);
        if(x=="PU"){
            $scope.particularSelected=true;
        }else{
            $scope.particularSelected=false;
        }
    }

    

    $scope.notify_tabs_switch = function () {
        if($scope.notify_tabs){
            $scope.notify_tabs = false;
        }else{
            $scope.notify_tabs = true;
        }
       
    };


    var refresh_all = function () {
        $scope.edit_notification_processing= false;
        $scope.notify_tabs = false;
        $scope.notifications = [];
        $scope.notificationinfo = {
            "clientId": Session.user.clientId,
         
            "title": '',
            "description": '',
            "selectOption": '',
            "deviceKey": '',
            "userId": Session.user.user,
            "isActive": true
        };
    
        GetUserDashboard.get().then(function (response) {
            if (response.success) {
                $scope.notifications = response.notifications;
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }
        }, function (error) {
            Notification({
                message: "Error fetching data. Please check your internet connection."
            }, 'error');
        });
    };
    
    refresh_all();
});
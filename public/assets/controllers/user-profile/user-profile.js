app.controller('UserProfileCtrl', function ($scope, UserProfile, GetUserDashboard,Users, Notification) {
    $scope.processing = true;
    $scope.d_processing = true;
    $scope.response = {};
    $scope.transactionDetails = [];
    $scope.approved_trips = [];
    $scope.editmode= false;
    $scope.pwdeditmode= false;
    UserProfile.get().then(function (response) {
        if (response.success) {
            $scope.response = response.user;
            $scope.processing = false;
        } else {
            Notification({
                message: response.msg
            }, 'error');

        }
    }, function (error) {
        Notification({
            message: "Error fetching data. Please check your internet connection."
        }, 'error');
    });

    GetUserDashboard.get().then(function (response) {
        $scope.d_processing = false;

        if (response.hasOwnProperty("transactionDetails")) {
            $scope.transactionDetails = response.transactionDetails;
        } else if (response.hasOwnProperty("transaction")) {
            $scope.transactionDetails = response.transaction;
        } else if (response.hasOwnProperty("transactions")) {
            $scope.transactionDetails = response.transactions;
        }

        var total = 0;
        var amount = 0;
        angular.forEach($scope.transactionDetails, function (value, key) {
            total += value.transactionAmount;
            if (angular.equals(value.travelPurpose, "Personal") || angular.equals(value.travelPurpose, "personal")) {
                amount += value.transactionAmount;
            }
        });

        var percentage = (amount / total) * 100;
        $scope.options_d_chart = {
            unit: "%",
            readOnly: true,
            size: 100,
            subText: {
                enabled: true,
                text: '₹' + amount,
                color: '#9D9D9D',
                font: '11'
            },
            textColor: '#669BDF',
            trackWidth: 6,
            barWidth: 5,
            trackColor: '#CDF3DE',
            barColor: '#669BDF'
        };
        $scope.d_chart = {
            personal_expenses: Math.round(percentage)
        };

        var trips_array = response.trips;
        angular.forEach(trips_array, function (value, key) {
            if (!angular.equals(value.tripStatus, "Pending")) {
                $scope.approved_trips.push(value);
            }
        });

    }, function (error) {
        Notification({
            message: "Error fetching data. Please check your internet connection."
        }, 'error');
    });
    
  
      $scope.editprofile = function ()
      {
          $scope.editmode= true;
          
      };
     $scope.editpwd = function ()
      {
          $scope.pwdeditmode= true;
          
      };
   
      $scope.updateuser = function (response) {
       console.log(response);
          if($scope.pwdeditmode==true)
              {
                  var pwd=response.pwd;
                   var updated_data = {
                                    userId: response._id,
                                    clientId: response.clientId,
                                    fullName: response.fullName,
                                    contactNum: response.contactNum,
                                    homeNum: response.homeNum,
                                    officeNum:response.officeNum,
                                    employeeAddress: response.employeeAddress,
                                    emailId: response.emailId,
                                    pwd:pwd,
                                    pwdmode:true
                                        };
              }
          else
              {
                    var updated_data = {
            userId: response._id,
            clientId: response.clientId,
            fullName: response.fullName,
            contactNum: response.contactNum,
            homeNum: response.homeNum,
            officeNum:response.officeNum,
            employeeAddress: response.employeeAddress,
            emailId: response.emailId,
                        pwdmode:false
        };
              }
      

        Users.update(updated_data).then(function (response) {
            if (response.success) {
                Notification({
                    message: response.msg
                }, 'success');
                refresh_all();
            } else {
                Notification({
                    message: response.msg
                }, 'error');
            }

        }, function (error) {
            Notification({
                message: error.msg
            }, 'error');
        }).finally(function () {

           
            $scope.editmode = false;
            $scope.pwdeditmode =false;
        });

    };


});

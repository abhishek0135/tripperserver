var app = angular.module('app', [
    'ui.router',
    'ui.bootstrap',
    'ngStorage',
    'ui-notification',
    'rcForm',
    'ui.knob',
    'bc.Flickity',
    'mwl.calendar',
    'angular.morris',
    'angularMoment',
    'datatables',
    'angular-clipboard',
    'angular-js-xlsx',
    'file-model',
    'angular.filter'
]);

app.directive("datepicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };
            var options = {
                dateFormat: "dd/mm/yy",
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            elem.datepicker(options);
        }
    }
});

app.directive('fileModel', ['$parse', 'fileService', function ($parse, fileService) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.bind('change', function(){
                scope.$apply(function(){
                    if (element[0].files != undefined) {
                        fileService.push(element[0].files[0]);
                        console.log('directive applying with file');
                    }
                });
            });
        }
    };
}])
 app.factory('fileService', function() {
    var files = [];
    return files;
})

app.filter("dateRangeFilter", function ($filter) {
    return function (items, from, to) {
        return $filter('filter')(items, "name", function (v) {
            var date = moment(v);
            return date >= moment(from) && date <= moment(to);
        });
    };
});

app.service('Water', function ($http, $q, API, Session) {
    return {
        
        'createn': function (request) {
            console.log("transactions"+request);

          var token = Session.user.token;

          var defer = $q.defer(),
              httpConfig = {},
              httpProperties = {
                  'method': 'POST',
                  'url': API.baseUrl + 'addWaterBiller/',
                  'data': request,
                  'headers': {
                      'Content-Type': undefined,
                      'Authorization': token
                  },
                  'transformRequest': angular.identity
              };
          $http(httpProperties).success(function (resp) {
              if (resp.success) {
                  defer.resolve(resp);
              } else {
                  defer.reject(resp);
              }

          }).error(function (err, status) {
              defer.reject(err);
          });
          return defer.promise;
      },
        'confirmbooking': function (request) {
        
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'method': 'POST',
                    'url': API.baseUrl + 'confirmbooking',
                    'data': request,
                    'headers': {
                        'Content-Type': undefined,
                        'Authorization': token
                    },
                    'transformRequest': angular.identity
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'update': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "updateWater/",
                    'headers': {
                        'Content-Type': "application/json",
                        'Authorization': token
                    },
                    'transformRequest': angular.identity,
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'sendtripinfo': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "sendtripinfo/",
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'bookvendor': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "bookvendor/",
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },'delete': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "deleteVendor/",
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        }
    };
});
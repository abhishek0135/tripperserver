app.service('Policy', function ($http, $q, API, Session) {
    return {
        'create': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "policy/",
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'createn': function (request) {
            console.log("transactions"+request);

          var token = Session.user.token;

          var defer = $q.defer(),
              httpConfig = {},
              httpProperties = {
                  'method': 'POST',
                  'url': API.baseUrl + 'addNews/',
                  'data': request,
                  'headers': {
                      'Content-Type': undefined,
                      'Authorization': token
                  },
                  'transformRequest': angular.identity
              };
          $http(httpProperties).success(function (resp) {
              if (resp.success) {
                  defer.resolve(resp);
              } else {
                  defer.reject(resp);
              }

          }).error(function (err, status) {
              defer.reject(err);
          });
          return defer.promise;
      },
      'update': function (request) {
        console.log("transactions"+request);

      var token = Session.user.token;

      var defer = $q.defer(),
          httpConfig = {},
          httpProperties = {
              'method': 'POST',
              'url': API.baseUrl + 'updateNews/',
              'data': request,
              'headers': {
                  'Content-Type': undefined,
                  'Authorization': token
              },
              'transformRequest': angular.identity
          };
      $http(httpProperties).success(function (resp) {
          if (resp.success) {
              defer.resolve(resp);
          } else {
              defer.reject(resp);
          }

      }).error(function (err, status) {
          defer.reject(err);
      });
      return defer.promise;
  }
        ,
        'delete': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "deleteFeed/",
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        }
        
    };
});

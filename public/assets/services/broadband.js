app.service('Broadband', function ($http, $q, API, Session) {
    return {
        
        
        'createn': function (request) {
            console.log("transactions"+request);

          var token = Session.user.token;

          var defer = $q.defer(),
              httpConfig = {},
              httpProperties = {
                  'method': 'POST',
                  'url': API.baseUrl + 'addbroadband/',
                  'data': request,
                  'headers': {
                      'Content-Type': undefined,
                      'Authorization': token
                  },
                  'transformRequest': angular.identity
              };
          $http(httpProperties).success(function (resp) {
              if (resp.success) {
                  defer.resolve(resp);
              } else {
                  defer.reject(resp);
              }

          }).error(function (err, status) {
              defer.reject(err);
          });
          return defer.promise;
      },
      'update': function (request) {
        console.log("transactions"+request);

      var token = Session.user.token;

      var defer = $q.defer(),
          httpConfig = {},
          httpProperties = {
              'method': 'POST',
              'url': API.baseUrl + 'updateBroadband/',
              'data': request,
              'headers': {
                  'Content-Type':  "application/json",
                  'Authorization': token
              },
              'transformRequest': angular.identity
          };
      $http(httpProperties).success(function (resp) {
          if (resp.success) {
              defer.resolve(resp);
          } else {
              defer.reject(resp);
          }

      }).error(function (err, status) {
          defer.reject(err);
      });
      return defer.promise;
  },
      'delete': function (request) { 
        var token = Session.user.token;
        var defer = $q.defer(),
            httpConfig = {},
            httpProperties = {
                'url': API.baseUrl + "deleteSubCategory/",
                'headers': {
                    'content-type': 'application/json',
                    'Authorization': token
                },
                'method': 'POST',
                'data': request
            };
        $http(httpProperties).success(function (resp) {
            if (resp.success) {
                defer.resolve(resp);
            } else {
                defer.reject(resp);
            }

        }).error(function (err, status) {
            defer.reject(err);
        });
        return defer.promise;
    }
    };
});

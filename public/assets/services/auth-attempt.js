app.service('AuthAttempted', function ($rootScope) {
    $rootScope.routeAttempted = 'app.dashboard';
    return {
        set: function (state) {
            $rootScope.routeAttempted = state;
        },
        setTimes: function (number) {
            $rootScope.routeAttemptedTimes = number;
        }
    };
});

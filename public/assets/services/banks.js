app.service('Banks', function ($http, $q, API, Session) {
    return {
        'get': function () {
            var token = Session.user.token;
            var userId = Session.user.user;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "banks",
                    'cache': false,
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'GET',
                    'params': {
                        'userId': userId
                    }

                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'create': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "category/",
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'update': function (request) {
            var token = Session.user.token;
            var defer = $q.defer(),
                httpConfig = {},
                httpProperties = {
                    'url': API.baseUrl + "updatecategory/",
                    'headers': {
                        'content-type': 'application/json',
                        'Authorization': token
                    },
                    'method': 'POST',
                    'data': request
                };
            $http(httpProperties).success(function (resp) {
                if (resp.success) {
                    defer.resolve(resp);
                } else {
                    defer.reject(resp);
                }

            }).error(function (err, status) {
                defer.reject(err);
            });
            return defer.promise;
        },
        'createn': function (request) {
            console.log("transactions"+request);

          var token = Session.user.token;

          var defer = $q.defer(),
              httpConfig = {},
              httpProperties = {
                  'method': 'POST',
                  'url': API.baseUrl + 'category/',
                  'data': request,
                  'headers': {
                      'Content-Type': undefined,
                      'Authorization': token
                  },
                  'transformRequest': angular.identity
              };
          $http(httpProperties).success(function (resp) {
              if (resp.success) {
                  defer.resolve(resp);
              } else {
                  defer.reject(resp);
              }

          }).error(function (err, status) {
              defer.reject(err);
          });
          return defer.promise;
      },
      'delete': function (request) {
        var token = Session.user.token;
        var defer = $q.defer(),
            httpConfig = {},
            httpProperties = {
                'url': API.baseUrl + "deleteCategory/",
                'headers': {
                    'content-type': 'application/json',
                    'Authorization': token
                },
                'method': 'POST',
                'data': request
            };
        $http(httpProperties).success(function (resp) {
            if (resp.success) {
                defer.resolve(resp);
            } else {
                defer.reject(resp);
            }

        }).error(function (err, status) {
            defer.reject(err);
        });
        return defer.promise;
    }
    };
});

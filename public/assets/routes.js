app.config(['$urlRouterProvider', '$stateProvider', 'USER_ROLES',
    function ($urlRouterProvider, $stateProvider, USER_ROLES) {

        // Any unmatched routes
        $urlRouterProvider.otherwise(function ($injector, $location) {
            var $state = $injector.get("$state");
            $state.go('app.dashboard');
            
           
        });

        // Set routes
        $stateProvider
            .state('app', {
                url: '',
                abstract: true,
                views: {
                    'sidebar@': {
                        templateUrl: '/templates/sidebar/sidebar.html',
                        controller: 'SidebarCtrl'
                    },
                    'header@': {
                        templateUrl: '/templates/header/header.html',
                        controller: 'HeaderCtrl'
                    }
                }
            })
            .state('app.locked', {
                url: '/locked',
                views: {
                    'login@': {
                        templateUrl: '/templates/locked/locked.html',
                        controller: 'LockedCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.basic, USER_ROLES.employee, USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                }
            })
            .state('app.dashboard', {
                url: '/',
                views: {
                    'container@': {
                        templateUrl: '/templates/dashboard/dashboard.html',
                        controller: 'DashboardCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.basic, USER_ROLES.employee, USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            }).state('app.voucher', {
                url: '/voucher',
                views: {
                    'container@': {
                        templateUrl: '/templates/printvoucher/printvoucher.html',
                        controller: 'PrintVoucherCtrl'
                    }
                },
            
                    params: {
                                obj: null
                            },
                
                data: {
                    authorizedRoles: [USER_ROLES.basic, USER_ROLES.employee, USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.user-profile', {
                url: '/user-profile',
                views: {
                    'container@': {
                        templateUrl: '/templates/user-profile/user-profile.html',
                        controller: 'UserProfileCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.basic, USER_ROLES.employee, USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.calendar', {
                url: '/calendar',
                views: {
                    'container@': {
                        templateUrl: '/templates/calendar/calendar.html',
                        controller: 'CalendarCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.basic, USER_ROLES.employee, USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.expenses', {
                url: '/expenses',
                views: {
                    'container@': {
                        templateUrl: '/templates/expenses/expenses.html',
                        controller: 'ExpensesCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.basic, USER_ROLES.employee, USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            }).state('app.payments', {
                url: '/payments',
                views: {
                    'container@': {
                        templateUrl: '/templates/payments/payments.html',
                        controller: 'PaymentsCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.accounts,USER_ROLES.superadmin,USER_ROLES.employee,USER_ROLES.basic]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            }).state('app.bookedtickets', {
                url: '/orders',
                views: {
                    'container@': {
                        templateUrl: '/templates/order/order.html',
                        controller: 'OrderCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.admin,  USER_ROLES.superadmin]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.travel-requests', {
                url: '/travel-requests',
                views: {
                    'container@': {
                        templateUrl: '/templates/travel-requests/travel-requests.html',
                        controller: 'TravelRequestsCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.basic, USER_ROLES.employee, USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.expenses-report', {
                url: '/push-notifications',
                views: {
                    'container@': {
                        templateUrl: '/templates/push-notifications/push-notifications.html',
                        controller: 'PushNotificationsCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.basic, USER_ROLES.employee, USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.settings', {
                url: '/settings',
                views: {
                    'container@': {
                        templateUrl: '/templates/settings/settings.html',
                        controller: 'SettingsCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.admin, USER_ROLES.superadmin]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.approval', {
                url: '/approval/:param',
                views: {
                    'container@': {
                        templateUrl: '/templates/approval/approval.html',
                        controller: 'ApprovalCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.invoice-management', {
                url: '/invoice-management/:param',
                views: {
                    'container@': {
                        templateUrl: '/templates/invoice-management/invoice-management.html',
                        controller: 'InvoiceManagementCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.hod, USER_ROLES.admin, USER_ROLES.accounts, USER_ROLES.superadmin, USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            })
            .state('app.fares', {
                url: '/fares',
                views: {
                    'container@': {
                        templateUrl: '/templates/fares/fares.html',
                        controller: 'FaresCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.vendor]
                },
                resolve: {
                    auth: ['AuthResolver', function (AuthResolver) {
                        AuthResolver.resolve();
                    }]
                }
            }).state('app.login', {
                url: '/login',
                views: {
                    'login@': {
                        templateUrl: '/templates/login/login.html',
                        controller: 'LoginCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.all]
                }
            })
            .state('app.sign-up', {
                url: '/sign-up',
                views: {
                    'login@': {
                        templateUrl: '/templates/sign-up/sign-up.html',
                        controller: 'SignupCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.all]
                }
            })
            .state('app.forgot', {
                url: '/forgot',
                views: {
                    'login@': {
                        templateUrl: '/templates/forgot/forgot.html',
                        controller: 'ForgotCtrl'
                    }
                },
                data: {
                    authorizedRoles: [USER_ROLES.all]
                }
            });
}]);

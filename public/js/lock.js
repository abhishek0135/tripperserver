var Lock = function () {

    return {
        //main function to initiate the module
        init: function () {

            $.backstretch([
		        "/img/media/bg1.jpg",
    		    "/img/media/bg2.jpg",
    		    "/img/media/bg3.jpg"
		        ], {
                fade: 1000,
                duration: 8000
            });
        }

    };

}();

jQuery(document).ready(function () {
    Lock.init();
});

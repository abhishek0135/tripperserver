var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var FCM = require('fcm-node');
var serverKey = 'AAAAWVWw9dA:APA91bEp-cmROqHRXciyYYCT-17gAmjugU5IyaaanobxlKGvyml37gtd5ef0I041Z8iUAEzLMhLDZC7Np4clRzNfBtdwSWo9Zfo7TDREA8rW44W-GHNEnbDslaS7ryItES59P2cgswJp'; // put your server key here
var fcm = new FCM(serverKey);
var baseUrl ="https://trippernew.s3.ap-south-1.amazonaws.com/" ;
var morgan      = require('morgan');
var nodemailer = require('nodemailer');
var mongoose    = require('mongoose');
var CryptoJS = require("crypto-js");
var convert = require('xml-js');
var md5 = require('md5');
var methodOverride = require('method-override')
var cors = require('cors');
var config      = require('./config/database'); // get db config file
require('dotenv').config();
require( 'dotenv' ).load();
var path = require('path');
var jsonxml = require('jsontoxml');
var port        = process.env.PORT || 80;
var Product        = require('./app/models/product'); 
var Complaint        = require('./app/models/complaint');
var Vendor        = require('./app/models/vendor');
var vendorCategory  = require('./app/models/vendorcategory'); 
var Notifications        = require('./app/models/notification'); 
var Electricity  = require('./app/models/electricity');
var Voucher  = require('./app/models/voucher');
var Gas  = require('./app/models/gas'); 
var DMT = require('./app/models/dmt'); 
var FastTag  = require('./app/models/fasttag'); 
var LifeInsurance  = require('./app/models/lifeInsurance');
var HealthInsurance  = require('./app/models/healthInsurance');
var LoanRepayment  = require('./app/models/loanrepayment');
var MunicipalTaxes  = require('./app/models/municipaltaxes');
var EducationFees  = require('./app/models/educationfees');
var LandlinePostpaid  = require('./app/models/landlinepostpaid');
var HousingSociety  = require('./app/models/housingsociety');
var BBPS = require('./app/models/bbps');
var Broadband = require('./app/models/broadband');
var Water = require('./app/models/waterbiller');
var Operators = require('./app/models/operator');
var Subscription = require('./app/models/subscription');
var AppliedVoucher = require('./app/models/appliedvoucher');
var Trend        = require('./app/models/trend'); 
var Transaction        = require('./app/models/transaction'); 
var Recharge        = require('./app/models/recharge'); 
var Offers        = require('./app/models/offers'); 
var ActiveSubscription        = require('./app/models/subscriptionOrder');
var Store = require('./app/models/store');
var Bank = require('./app/models/bank');
const request = require('request');
var twilio = require('twilio');
const crypto = require('crypto'),

       
encrypt = function (plainText, workingKey) {
    var m = crypto.createHash('md5');
        m.update(workingKey);
       var key = m.digest('buffer');
          var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';	
    var cipher = crypto.createCipheriv('aes-128-cbc', key, iv);
    var encoded = cipher.update(plainText,'utf8','hex');
    encoded += cipher.final('hex');
        return encoded;
};


decrypt = function (encText, workingKey) {
        var m = crypto.createHash('md5');
        m.update(workingKey)
        var key = m.digest('buffer');
    var iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f';	
    var decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
        var decoded = decipher.update(encText,'hex','utf8');
    decoded += decipher.final('utf8');
        return decoded;
};

const INITIALIZATION_VECTOR = '0000000000000000';
var User        = require('./app/models/user');
var Order        = require('./app/models/order');

var randomstring = require("randomstring");
var randomNumber = require('random-number');
var Insta = require('instamojo-nodejs');
var querystring = require('querystring');
var http = require('http');
const axios = require('axios');
var moment = require('moment');
var SubCategory        = require('./app/models/gas');


app.use(cors());
const cron = require("node-cron");





var multer = require( 'multer' );
var s3 = require( 'multer-storage-s3' );
var storage = s3({
    destination : function( req, file, cb ) {
        
        cb( null, '' );
        
    },
    filename    : function( req, file, cb ) {
        
        cb( null, randomstring.generate() +path.extname(file.originalname) );

        
    },
    bucket      : 'trippernew',
    region      : 'ap-south-1'
});
var uploadMiddleware = multer({ storage: storage });

var moment = require('moment');

function getNextSequence(name,fn) {
    var retseq=0;
   
    Id.findOneAndUpdate({_id: name}, {$inc: { seq: 1 }}, {new: true}, function(err, doc){
    if(err){
        console.log("Something wrong when updating data!");
    }

  
         retseq=doc.seq;
       
        fn(retseq);
     
});
   
}




Date.prototype.addHours = function(h) {    
   this.setTime(this.getTime() + (h*60*60*1000)); 
   return this;   
}

Date.prototype.addDays = function(days)
{
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

var hasOwnProperty = Object.prototype.hasOwnProperty;
var isEmpty=function (obj) {

    
    if (obj == null) return true;

    
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(morgan('dev'));
 app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,Authorization, Accept");
  next();
});

mongoose.connect("mongodb://root:0zRcJ8fNw4Gb@localhost/exp?authSource=admin", {useNewUrlParser: true});
//mongoose.connect(config.database);
// pass passport for configuration

route = require('./routes/routes');
var apiRoutes = express.Router();

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/assets/images'));
 app.use(express.static('uploads'));


app.get('/', function(req, res) {
     res.sendFile(path.join(__dirname, './public', 'index.html'));
   
    });
        
// connect the api routes under /api/*
app.use('/api', apiRoutes);

 
Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] == obj) {
            return true;
        }
    }
    return false;
}

// cron.schedule("0 07 13 * * *", function () {
//         console.log("Running Cron Job every 24 hours.");

// var accessCode= 'AVTM87KZ80ZB95QHDY';
// var reqId = randomstring.generate(35);
// var workingkey='C516828299E4BE70B6B0612ACFE795FD';
// var req = '<dmtServiceRequest> <requestType>BankList</requestType> <txnType>IMPS</txnType></dmtServiceRequest>';
// console.log(req);
// console.log("check please");

// var encdata= encrypt(req,workingkey);
// var bankArray = [];

//         request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
//             form: {
//           accessCode:accessCode,
//           requestId:reqId,
//           encRequest:encdata,
//           ver:'1.0',
//           instituteId:'IA83'
//             }
//           }, function (err, httpResponse, body) { 
              
           
      
//               console.log("Body ends");
            
//               if(err){
//                   console.log(err);
//               }
             
//               try {
            
//                 var decrypted=decrypt(body,workingkey);
//                 result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
//                  console.log(result2)
                      
//                       console.log("check asap");
//                       var senderDetailResponse = result2.dmtServiceResponse;
                     
//                       if(senderDetailResponse.responseCode._text=="000"){
              
//                           bankArray =  senderDetailResponse.bankList.bankInfoArray;
//                           console.log(bankArray);
//                           console.log("ABOVE IS BANK DETAILS");
//                           console.log("below is bankdata");
//                           console.log(bankArray[552]);
//                           console.log(bankArray.length);
//                           for(var i=0;i<552;i++){
                             
                          
                           
                               
                                 
//                                         console.log("HERE IT IS");
                             
//                                         // if(banker==null||banker==undefined){
//                                             var bankData={};
//                                             bankData = {
//                                                 'accountVerificationAllowed':bankArray[i].accountVerificationAllowed._text,
//                                                 'bankCode':bankArray[i].bankCode._text,
//                                                 'bankName':bankArray[i].bankName._text,
//                                                 'impsAllowed':bankArray[i].impsAllowed._text,
//                                                 'neftAllowed':bankArray[i].neftAllowed._text,
                    
//                                               }
//                                               Bank.addBank(bankData, function(err, bank){
//                                                 if(err){
//                                                     console.log(err)
                                                   
//                                                 //	throw err;
//                                                 }else{
// console.log(bankData);
//                                                     console.log(bank+i +"added")
                                                 
//                                                 }
                                                
                                                
                                               
//                                             });
                                         
                                           
                                        
//                                    // }else{
//                                         //     console.log(banker+i +"Already Added");
//                                         // }
                            
                            

                              
                             
//                           }
                       
//                       }else{
//                      console.log(senderDetailResponse)
//                       }
                
                   
//               }
//               catch (e) {
//                             console.log("error"+e);
//                           var decrypted = "";
                        
//                         }
                   
//                    });

//                 });



// **?? ***?? ** ?? Cron Job Function Add Billers

// cron.schedule("0 0 */3 * * *", function () {
//     console.log("Running Cron Job every 3 hours.");

//      var accessCode= 'AVTM87KZ80ZB95QHDY';
// var reqId = randomstring.generate(35);
//     var workingkey='C516828299E4BE70B6B0612ACFE795FD';
//     // console.log(originalText); // 'my message'

// var req='<?xml version="1.0" encoding="UTF-8"?><billerInfoRequest></billerInfoRequest>';
//    var encdata= encrypt(req,workingkey);
//     request.post('https://api.billavenue.com/billpay/extMdmCntrl/mdmRequest/xml', {
//       form: {
//     accessCode:accessCode,
//     requestId:reqId,
//     encRequest:encdata,
//     ver:'1.0',
//     instituteId:'IA83'
//       }
//     }, function (err, httpResponse, body) { 
        
//        console.log(body);

//         console.log("Body ends");
      
//         if(err){
//             console.log("above is error");
//             console.log(err);
         
//         }
       
//         try {
//             console.log("goes here");
//           var decrypted=decrypt(body,workingkey);
//          var result2=[];
        
//          var allDataBillerArray=[];
         
//            result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
//            console.log(result2)
//                 console.log(typeof(result2));
//                 console.log("check result2 above");
           
//                  allDataBillerArray=result2.billerInfoResponse.biller;
//                  console.log(allDataBillerArray[50].billerInputParams.paramInfo.paramName);
//                  console.log(typeof(allDataBillerArray));
//                  console.log("check asap1 allDataBillerArray type above");
//                  console.log(typeof(allDataBillerArray));
//                  console.log("check asap1 allDataBillerArray above");
//                  console.log(allDataBillerArray.length);
//                  console.log("check asap2 allDataBillerArray length");
//                  console.log(allDataBillerArray[55]);
//                  console.log("check asap2 some vendor");
               
//             for(let i=0;i<allDataBillerArray.length;i++){
//                 if(allDataBillerArray[i].billerCategory._text=="Gas"){
                
//                     console.log("Match");

//                     Gas.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err, biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var gasBillerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text, 
//                                     inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 Gas.addGas(gasBillerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data" + i);
//                                     }
//                                 });

//                             }else{
//                                 console.log("Don't save billerExist");
                            
//                             }
                           
//                         }
//                     });

                   
                
//                 }
//                 else if(allDataBillerArray[i].billerCategory._text=="Water"){
//                     // allWaterVendor=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");

//                     Water.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var waterBillerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 Water.addWater(waterBillerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data " + i);
//                                     }
//                                 });

//                             }else{

//                                 console.log("Don't save billerExist");
                               

//                             }
//                         }
//                     });

                    
//                 }
//                 else if(allDataBillerArray[i].billerCategory._text=="Electricity"){
                  
//                     console.log("Match");

//                     Electricity.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var electricityBillerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 Electricity.addElectricity(electricityBillerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data " +i);
//                                     }
//                                 });

//                             }else{
//                                 console.log("Don't save billerExist");
                             
//                             }
//                         }
//                     });
                    
//                 }
//                 else if(allDataBillerArray[i].billerCategory._text=="Broadband Postpaid"){
//                     // allBroadbandVendor=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");

//                     Broadband.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var broadbandBillerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 Broadband.addbroadband(broadbandBillerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data " +i);
//                                     }
//                                 });

//                             }else{
//                                 console.log("Don't save billerExist");
                           
//                             }
//                         }
//                     });
                   
//                 }
//                 else if(allDataBillerArray[i].billerCategory._text=="Fastag"){
//                     // allFastagVendor=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");

//                     FastTag.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var fastTagBillerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 FastTag.addFastTag(fastTagBillerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data " +i);
//                                     }
//                                 });

//                             }else{
//                                 console.log("Don't save billerExist");
                             
//                             }
//                         }
//                     });
                   
                    
//                 }else if(allDataBillerArray[i].billerCategory._text=='Loan Repayment'){
                    
//                     console.log("Match");
                    
//                     LoanRepayment.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var billerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 LoanRepayment.addLoanRepayment(billerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data " +i);
//                                     }
//                                 });

//                             }else{
//                                 console.log("Don't save billerExist");
                        
//                             }
//                         }
//                     });
                    
                 
                   
//             }else if(allDataBillerArray[i].billerCategory._text=='Health Insurance'){
               
//                     console.log("Match");

//                     HealthInsurance.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var billerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 HealthInsurance.addhealthInsurance(billerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data "+ i);
//                                     }
//                                 });

//                             }else{
//                                 console.log("Didn't save billerExist");
                             
//                             }
//                         }
//                     });
                    
//             }else if(allDataBillerArray[i].billerCategory._text=='Insurance'||allDataBillerArray[i].billerCategory._text=='Life Insurance'){
         
//                     console.log("Match");
//                     LifeInsurance.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var billerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 LifeInsurance.addlifeInsurance(billerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data " + i);
//                                     }
//                                 });

//                             }else{
//                                 console.log("Didn't save billerExist");
//                             }
//                         }
//                     });

                    
//             }else if(allDataBillerArray[i].billerCategory._text=='Education Fees'){

//                 console.log("Match");
                
//                 EducationFees.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                     if(err){
//                         console.log(err);
//                         console.log("goes in error");
//                     }else{
//                         console.log(biller);
//                         if(biller==null||biller==undefined){
//                             var billerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text,    
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                  inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
                        
//                             EducationFees.addEducationFees(billerData,function(err, newBiller){
//                                 if(err){
//                                     console.log(err);
//                                     console.log("Failed To add Data");
//                                 //	throw err;
//                                 }
//                                 else{
//                                     console.log(newBiller);
//                                     console.log("Added Data " + i);
//                                 }
//                             });
//                         }else{
//                             console.log("Didn't save billerExist");
//                         }
//                     }
//                 });
                   
                   
//             }else if(allDataBillerArray[i].billerCategory._text=='Municipal Taxes'){
               
//                     console.log("Match");

//                     MunicipalTaxes.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){

//                                 var billerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 MunicipalTaxes.addMunicipalTaxes(billerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data");
//                                     }
//                                 });

//                             }else{
//                                 console.log("Didn't save billerExist");
//                             }
//                         }
//                     });
                   
//             }else if(allDataBillerArray[i].billerCategory._text=='Landline Postpaid'){
               
                   
//                 LandlinePostpaid.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                     if(err){
//                         console.log(err);
//                         console.log("goes in error");
//                     }else{
//                         console.log(biller);
//                         if(biller==null||biller==undefined){
//                             var billerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text,    
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                  inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
                        
//                             LandlinePostpaid.addlandlinePostpaid(billerData,function(err, newBiller){
//                                 if(err){
//                                     console.log(err);
//                                     console.log("Failed To add Data");
//                                 //	throw err;
//                                 }
//                                 else{
//                                     console.log(newBiller);
//                                     console.log("Added Data" + i);
//                                 }
//                             });
//                         }else{
                           
//                         }
//                     }
//                 });
                   
//             }else if(allDataBillerArray[i].billerCategory._text=='Housing Society'){
             
//                     console.log("Match");
//                     HousingSociety.findOne({'billerId':allDataBillerArray[i].billerId._text},function(err,biller){
//                         if(err){
//                             console.log(err);
//                             console.log("goes in error");
//                         }else{
//                             console.log(biller);
//                             if(biller==null||biller==undefined){
//                                 var billerData = {
        
//                                     billerId:allDataBillerArray[i].billerId._text,        
//                                     billerName:allDataBillerArray[i].billerName._text,    
//                                     billerCategory: allDataBillerArray[i].billerCategory._text,
//                                      inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                     billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                     billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                     billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                     billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                     billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                    
//                                 }
                            
//                                 HousingSociety.addHousingSociety(billerData,function(err, newBiller){
//                                     if(err){
//                                         console.log(err);
//                                         console.log("Failed To add Data");
//                                     //	throw err;
//                                     }
//                                     else{
//                                         console.log(newBiller);
//                                         console.log("Added Data " +i);
//                                     }
//                                 });
//                             }else{
//                                 console.log("Didn't save billerExist");
//                             }
//                         }
//                     });
                    
//             }
//             }
//             console.log("ends")
//           } catch (e) {
//               console.log("error"+e);
//             var decrypted = "";
//           }
     
//      });


//     });

// **?? ***?? ** ?? Cron Job Function Ends





//Add a Broadband
apiRoutes.post('/addbroadband',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    Broadband.addbroadband(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end


//Add a Electricity
apiRoutes.post('/updateBiller',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var billerId=req.body.billerId;
   
   var billerCategory=req.body.billerCategory;                   
        
    var billerUrl=baseUrl+req.file.filename;
    console.log(billerUrl);
    console.log(billerId);
     
    
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    if(billerCategory=="Electricity"){
        Electricity.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }else if(billerCategory=="Fastag"){
        FastTag.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Broadband Postpaid"){
        Broadband.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Gas"){
        Gas.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Municipal Taxes"){
        MunicipalTaxes.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Education Fees"){
        EducationFees.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Landline Postpaid"){
        LandlinePostpaid.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Loan Repayment"){
        LoanRepayment.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Life Insurance"||billerCategory=="Insurance"){
        LifeInsurance.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Health Insurance"){
        HealthInsurance.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Housing Society"){
        HousingSociety.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
    else if(billerCategory=="Water"){
        Water.findOneAndUpdate({'billerId':billerId},{ $set: {billerUrl:billerUrl}},function(err, updatedbiller){
            if(err){
                console.log(err);
                res.json({success: false, msg: 'Failed to add Request'});
            //	throw err;
            }
            else{
                res.json({success:true,msg:'Vendor Category Added Successfully',data:updatedbiller});
            }
        });
    }
   

  
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end


//Add a Education Biller
apiRoutes.post('/addEducationBiller',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    EducationFees.addEducationFees(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end

//Add a Fastag
apiRoutes.post('/addfastag',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    FastTag.addFastTag(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end

//Add a Gas
apiRoutes.post('/addgasbiller',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    Gas.addGas(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end


//Add a healthInsurance
apiRoutes.post('/addhealthInsurance',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    HealthInsurance.addhealthInsurance(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end


//Add a HousingSociety
apiRoutes.post('/addhousingSociety',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    HousingSociety.addHousingSociety(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end


//Add a landlinepostpaid biller
apiRoutes.post('/addLandlinePostpaid',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    LandlinePostpaid.addlandlinePostpaid(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end



//Add a Life insurance
apiRoutes.post('/addlifeInsurance',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    LifeInsurance.addlifeInsurance(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end

//Add a Loan Repayment
apiRoutes.post('/addLoanRepayment',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
    billerId:req.body.billerId,
    billerName:req.body.billerName,
    billerCategory:req.body.billerCategory, 
    billerAdhoc:req.body.billerAdhoc, 
    billerCoverage:req.body.billerCoverage, 
    billerFetchRequiremet:req.body.billerFetchRequiremet, 
    inputParam:req.body.inputParam, 
    billerPaymentExactness:req.body.billerPaymentExactness, 
    billerSupportBillValidation:req.body.billerSupportBillValidation,                   
        
     billerUrl:baseUrl+req.file.filename
     
    }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    LoanRepayment.addLoanRepayment(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end


//Add a Municipal Taxes
apiRoutes.post('/addMunicipalTaxes',  uploadMiddleware.single('attachment'),  function( req, res, next ){

    var token = getToken(req.headers);
    var userId=req.body.userId;
    // var vendorCategory = req.body;
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
   var vendorCategoryData = {
   billerId:req.body.billerId,
   billerName:req.body.billerName,
   billerCategory:req.body.billerCategory, 
   billerAdhoc:req.body.billerAdhoc, 
   billerCoverage:req.body.billerCoverage, 
   billerFetchRequiremet:req.body.billerFetchRequiremet, 
   inputParam:req.body.inputParam, 
   billerPaymentExactness:req.body.billerPaymentExactness, 
   billerSupportBillValidation:req.body.billerSupportBillValidation,                   
       
    billerUrl:baseUrl+req.file.filename
    
   }
    

   

 

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    MunicipalTaxes.addMunicipalTaxes(vendorCategoryData,function(err, vendorcat){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Vendor Category Added Successfully',data:vendorcat});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Category Token Authentication failed'});
 }
});
//end









//sms settings
sendPartnerWelcomeMessage = function () {

    // if (user && user.phone && user.name) {
      const params = new URLSearchParams();
      params.append("numbers", [parseInt("91" + '7757095610')]);
      params.append(
        "message",
        `Hi vaibhav,
Thank you for your payment of 1000 against OTNS, Customer Number OTNS00005XXORT.
Transaction Reference ID : CC5656GH56FFD on 16th June 2020.`
      );
      tlClient.post("/send", params);
    // }
  }

  const tlClient = axios.create({
    baseURL: "https://api.textlocal.in/",
    params: {
      apiKey: "k+BLECel3mc-YFIABbvSbh4E2fBdXA8BWklk5tB0xv", //Text local api key
      sender: "TLTEST"
    }
  });




//login vendor JRI

apiRoutes.post('/jriLogin', function(req, res){


    var userId=req.body.userId;
    var SecurityKey=req.body.SecurityKey;
    
    var EmailId = req.body.EmailId;
    var Password=req.body.Password;
    var userName=req.body.userName;
    var APIChkSum = req.body.APIChkSum;
    var SystemReference = req.body.SystemReference;
   
    var APIChkVouch =  md5(EmailId+Password+SystemReference)

    User.findOne({ '_id':userId }, function(err, user) {
         
axios
.post('https://spi.justrechargeit.com/JRICorporateLogin.svc/securelogin/', {
 
 
    "EmailId":EmailId,
    "Password":Password,
    "userName":userName,
    "APIChkSum":APIChkSum,
    "SecurityKey":SecurityKey,
 
    
},
{
  headers: {
   'Content-Type':'application/json',
     
  }
})
.then(dataRes => {
//   console.log(`statusCode: ${res.statusCode}`)
console.log(dataRes.data);
  console.log("THIS is Voucher Array");
  axios
.post('https://api.gorecharge.in/vouchersystem/Product.svc/json/GetProductList', {
 
 
    "Email":EmailId,
    "Systemreferenceno":SystemReference,
    
    "APIChkSum":APIChkVouch,
    
 
    
},
{
  headers: {
   'Content-Type':'application/json',
     
  }
})
.then(response => {
//   console.log(`statusCode: ${res.statusCode}`)
console.log(response);
Subscription.find(function(err,subscriptions)
{
   if (err){
    
console.log(err);
  
  }else{
    console.log("THIS is RESOPONSE");
    Trend.find(function(err,trends)
{
   if (err){
    
console.log(err);
  
  }else{
    console.log("THIS is RESOPONSE");
    res.json({success: true,msg:'Request Sent Successfully',res:dataRes.data,voucher:response.data,user:user,subscriptions:subscriptions,trends:trends});
  }
});
  }
});

})
.catch(error => {
  console.error(error+"goes here");
  res.json({success: false,msg:'Request cannot Send Successfully',res:error});

})
 

})
.catch(error => {
  console.error(error+"goes in first hit");
  res.json({success: false,msg:'Request cannot Send Successfully',res:error});

})

});
});



//OTP MECHANISM CHANGING

var accountSid = 'ACa8048e8e69fe674e0fb2ed45878b1908'; // Your Account SID from www.twilio.com/console
var authToken = 'f5047c845be3ae443d0c4c8b8b2ded62';   // Your Auth Token from www.twilio.com/console
var client = new twilio(accountSid, authToken);


// otp api

apiRoutes.post('/requestOtp',function(req,res){
  
  


    var contactNum=req.body.contactNum;
   // var OTP=makeid(6);
        

    if(contactNum!=undefined){
        User.findOne({"contactNum":contactNum},function(err, user) {
            if (err){
                res.json({success: false, msg: 'No User Found.'});
            }else {
                console.log("goesHere");
                console.log(contactNum);
                client.verify.services.create({friendlyName: 'is your verification code'})
                .then(service => {console.log(service.sid);
                    console.log(service);
                console.log("comingHEre");
                var contact = '+91'+ contactNum.toString();
                console.log(contact);
                console.log(typeof(contact));
                client.verify.services(service.sid)
                .verifications
                .create({to: contact, channel: 'sms'})
                .then(verification => {
                    console.log(verification.status);
                    console.log("next resp");
                    res.json({success: true,msg:"Otp Sent",data:verification.status,sId:service.sid});
                });
             
             
                   
               
                // .then(twilioresp=>{
                //     res.json({success: true,msg:"Otp Sent",data:verification.status,sId:sid,twilioresp:twilioresp});
                // })
              
                   
                
             
              });
            }                        
        });
    }else {
        res.json({success: false,msg:'Authentication failed'});
    } 

    
});


apiRoutes.post('/verifyOtp',function(req,res){




    var sid = req.body.sid;
    var otp = req.body.otp;
    var contactNum = req.body.contactNum;
    console.log("comesHere");
    console.log("comesHere" +sid);
    console.log("comesHere"+otp);
    console.log("comesHere"+contactNum);
    

 


    client.verify.services(sid)
    .verificationChecks
    .create({to: contactNum, code: otp})
    .then(verification_check => {
        console.log(verification_check)
        console.log(verification_check.status);
        if(verification_check.status=="approved"){
            res.json({success: true,msg:"Otp verified"});
        }else{
            res.json({success: false,msg:"Otp not verified"});
        }
     
    });
    
});



apiRoutes.post('/claimRewardPoint', function(req, res){
    var userId = req.body.userId;
    var amount = req.body.amount;
var contactNum = req.body.contactNum;
var transactionRef = req.body.refId;
var date = moment().format("DD MMM YYYY");
var time = moment().format("hh:mm a");
    var transactionData ={
        userId:userId,
        contactNum:contactNum,
        date:date,
        time:time,
        amount:amount,
        transactionType:'Withdrawal Money',
        transactionRef:transactionRef
    }
  console.log(amount);
    if(contactNum!==undefined){
        User.findOne({ '_id': userId }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 var prevAmount=user.wallet;
                 var updatedAmount=prevAmount-amount;
                      User.update({'_id': userId}, { $set: {wallet:updatedAmount}}, function (err, user) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update Wallet'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    Transaction.addTransaction(transactionData,function(err, transaction){
                                        if(err){
                                            console.log(err);

                                            res.json({success: false, msg: 'Failed to add Transaction'});
                                        //	throw err;
                                        }
                                        else{
                                            res.json({success: true,msg:'10% Reward Points Claimed.',user:user,transaction:transaction});
                                        }
                                    });
                                   
                                }
                        });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});


apiRoutes.post('/creditRewardPoint', function(req, res){
    var userId = req.body.userId;
    var amount = req.body.amount;

var transactionId = req.body.refId;

   
  console.log(amount);
    if(userId!=undefined){
        User.findOne({ '_id': userId }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 var prevAmount=user.wallet;
                 var updatedAmount=prevAmount+amount;
                      User.update({'_id': userId}, { $set: {wallet:updatedAmount}}, function (err, user) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update Wallet'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    Transaction.findByIdAndRemove({_id: transactionId}, function(err, numberAffected, rawResponse) {
                                       //handle it
                                                if(err){
                                                res.json({success: false,msg:'Please try again later.'});
                                                // throw err;
                                            }
                                                else{
                                                    console.log("Claim CHECK NOW HERE ")
                                                    res.json({success: true,msg:'Ok'});
                                                }
                                    });
                                   
                                }
                        });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});


//get voucher api


apiRoutes.post('/getVoucher', function(req, res){

    var Email='lokeshb@gmx.com';
    var Password='123321';
    var SystemReference = req.body.SystemReference;
        var APIChkSum =  md5(Email+Password+SystemReference);
var productNo=req.body.productNo;
var productName=req.body.productName;
var validity=req.body.validity;
var description=req.body.description;
var howToUse=req.body.howToUse;
var Denomination=req.body.Denomination;
var Quantity = req.body.Quantity;
var timeStamp=req.body.timeStamp;
        var userId=req.body.userId;
        
       var transactionData={
           'userId':userId,
           'transactionRef':SystemReference,
           'transactionType':'Get Voucher',
           'amount':Denomination,
           'timeStamp':timeStamp,
           'ProductName':productName,
           'ProductNo':productNo,
           'Validity':validity,
           'Description':description,
           'HowToUse':howToUse

       }
    
        User.findOne({ '_id':userId }, function(err, user) {
             
    axios
    .post('http://api.justrechargeit.com/JRI_API_Coupon.svc/GetCouponsForClients', {
     
     
        "Email":Email,
        "Systemreferenceno":SystemReference,
        "APIChkSum":APIChkSum,
        "Quantity":Quantity,
        "Denomination":Denomination,
        "ProductNo":ProductNo
        
     
        
    },
    {
      headers: {
       'Content-Type':'application/json',
         
      }
    })
    .then(dataRes => {
    //   console.log(`statusCode: ${res.statusCode}`)
    console.log(dataRes);
      console.log("THIS is Success RESOPONSE");
      Voucher.addVoucher(transactionData,function(err, transaction){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Voucher'});
        //	throw err;
        }
        else{
            res.json({success: true,msg:'Voucher Added Successfully.',user:user,transaction:transaction});
        }
   
    
    })
    .catch(error => {
      console.error(error+"THIS is ERROR");
      res.json({success: false,msg:'Request cannot Send Successfully. Please try again in sometime.',res:error});
    
    })   
 });
    
    });
    });


    //Check Voucher Status

    //get voucher api


apiRoutes.post('/getVoucherStatus', function(req, res){

    var Email='lokeshb@gmx.com';
    var SystemReference = req.body.SystemReference;
  
       
        var contactNum=req.body.contactNum;
       
    
        User.findOne({ 'contactNum':contactNum }, function(err, user) {
             
    axios
    .post('https://api.gorecharge.in/demovoucher/Voucher.svc/json/GetVoucherStatus', {
     
     
        "Email":Email,
        "Systemreferenceno":SystemReference,
       
       
     
        
    },
    {
      headers: {
       'Content-Type':'application/json',
         
      }
    })
    .then(dataRes => {
    //   console.log(`statusCode: ${res.statusCode}`)
    console.log(dataRes);
      console.log("THIS is RESOPONSE");
      res.json({success: true,msg:'Request Sent Successfully',res:dataRes.data});
    
    })
    .catch(error => {
      console.error(error+"THIS is ERROR");
      res.json({success: false,msg:'Request cannot Send Successfully',res:error});
    
    })
    
    });
    });



   


         //Get DMT Sender Detail.

         apiRoutes.post('/dmtSenderDetails', function(req, res){
            var userId =req.body.userId;
            var senderMobile =req.body.senderMobile.toString();
            var transactionType =req.body.transactionType;
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = randomstring.generate(35);
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
   var req = '<dmtServiceRequest><requestType>SenderDetails</requestType><senderMobileNumber>'+senderMobile+'</senderMobileNumber><txnType>'+transactionType+'</txnType></dmtServiceRequest>';
  console.log(req);
  console.log("check");
  console.log(reqId);
  console.log(accessCode);
  console.log(workingkey);
    var encdata= encrypt(req,workingkey);

    User.find({'_id':userId},function(err,user){
        if(err){
            res.json({success:false,msg:"No Agent Exist"});
        }else{
            request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
                form: {
              accessCode:accessCode,
              requestId:reqId,
              encRequest:encdata,
              ver:'1.0',
              instituteId:'IA83'
                }
              }, function (err, httpResponse, body) { 
                  
                 // console.log(body);
          
                  console.log("Body ends");
                
                  if(err){
                      console.log(err);
                  }
                 
                  try {
                
                    var decrypted=decrypt(body,workingkey);
                    result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                     console.log(result2)
                          
                          console.log("check asap");
                          var senderDetailResponse = result2.dmtServiceResponse;
                          console.log(senderDetailResponse);
                          if(senderDetailResponse.responseCode._text=="000"){
                            res.json({success:true,msg:"Retrived Sender Details",data:senderDetailResponse});
                          }else{
                            res.json({success:false,msg:"Response",data:senderDetailResponse}); 
                          }
                    
                       
                  }
                  catch (e) {
                                console.log("error"+e);
                              var decrypted = "";
                              res.json({success:false,msg:"Failed Sender Details"});
                            }
                       
                       });
        }
    })
  
   
            });


// DMT Sender Registration.

apiRoutes.post('/dmtSenderRegister', function(req, res){
    var transactionType =req.body.transactionType;
    var senderMobile =req.body.senderMobile.toString();
    var senderName =req.body.senderName;
    var senderPin =req.body.pincode;
    var userId =req.body.userId;
var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
var workingkey='C516828299E4BE70B6B0612ACFE795FD';
var req = '<dmtServiceRequest><requestType>SenderRegister</requestType> <senderMobileNumber>'+senderMobile+'</senderMobileNumber> <txnType>IMPS</txnType><senderName>'+senderName+'</senderName> <senderPin>'+senderPin+'</senderPin></dmtServiceRequest>';
console.log(req);
console.log("check please");
var encdata= encrypt(req,workingkey);
console.log(reqId);
console.log("reqId")

User.find({'_id':userId},function(err,user){
    if(err){
        res.json({success:false,msg:"No Agent Exist"});
    }else{
        request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
            form: {
          accessCode:accessCode,
          requestId:reqId,
          encRequest:encdata,
          ver:'1.0',
          instituteId:'IA83'
            }
          }, function (err, httpResponse, body) { 
              
             // console.log(body);
      
              console.log("Body ends");
            
              if(err){
                  console.log(err);
              }
             
              try {
            
                var decrypted=decrypt(body,workingkey);
                result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                 console.log(result2)
                      
                      console.log("check asap");
                      var senderDetailResponse = result2.dmtServiceResponse;
                      console.log(senderDetailResponse);
                      if(senderDetailResponse.responseCode._text=="000"){
                        res.json({success:true,msg:"Retrived Verification Details",data:senderDetailResponse,transactionType:transactionType,reqId:reqId});
                      }else{
                        res.json({success:false,msg:"Response",data:senderDetailResponse,transactionType:transactionType,reqId:reqId}); 
                      }
                
                   
              }
              catch (e) {
                            console.log("error"+e);
                          var decrypted = "";
                          res.json({success:false,msg:"Failed Sender Details"});
                        }
                   
                   });
    }
})


    });


    //Verify New DMt User


    apiRoutes.post('/dmtVerifySender', function(req, res){
        var transactionType =req.body.transactionType;
        var senderMobile =req.body.senderMobile.toString();
        var customerOtp =req.body.customerOtp;
        var uniqueKey =req.body.uniqueKey;
        var userId =req.body.userId;

        
    var accessCode= 'AVTM87KZ80ZB95QHDY';
    var reqId = randomstring.generate(35);
    var workingkey='C516828299E4BE70B6B0612ACFE795FD';
    var req = '<dmtServiceRequest><requestType>VerifySender</requestType><senderMobileNumber>'+senderMobile+'</senderMobileNumber><txnType>'+transactionType+'</txnType><otp>'+customerOtp+'</otp><additionalRegData>'+uniqueKey+'</additionalRegData></dmtServiceRequest>';
    console.log(req);
console.log("check please");
    var encdata= encrypt(req,workingkey);
    User.find({'_id':userId},function(err,user){
        if(err){
            res.json({success:false,msg:"No Agent Exist"});
        }else{
            request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
                form: {
              accessCode:accessCode,
              requestId:reqId,
              encRequest:encdata,
              ver:'1.0',
              instituteId:'IA83'
                }
              }, function (err, httpResponse, body) { 
                  
                 // console.log(body);
          
                  console.log("Body ends");
                
                  if(err){
                      console.log(err);
                  }
                 
                  try {
                
                    var decrypted=decrypt(body,workingkey);
                    result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                     console.log(result2)
                          
                          console.log("check asap");
                          var senderDetailResponse = result2.dmtServiceResponse;
                          console.log(senderDetailResponse);
                          if(senderDetailResponse.responseCode._text=="000"){
                            res.json({success:true,msg:"Verified Sender Details",data:senderDetailResponse});
                          }else{
                            res.json({success:false,msg:"Response",data:senderDetailResponse}); 
                          }
                    
                       
                  }
                  catch (e) {
                                console.log("error"+e);
                              var decrypted = "";
                              res.json({success:false,msg:"Failed Sender Details"});
                            }
                       
                       });
        }
    })
    
        });



//Resend Otp DMT

        apiRoutes.post('/resendOtpDMt', function(req, res){
            var transactionType =req.body.transactionType;
            var senderMobile =req.body.senderMobile.toString();
            var userId =req.body.userId;
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = randomstring.generate(35);
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
        var req = '<dmtServiceRequest><requestType>ResendSenderOtp</requestType><senderMobileNumber>'+senderMobile+'</senderMobileNumber><txnType>'+transactionType+'</txnType></dmtServiceRequest>';
        console.log(req);
console.log("check please");
        
        var encdata= encrypt(req,workingkey);
        
        User.find({'_id':userId},function(err,user){
            if(err){
                res.json({success:false,msg:"No Agent Exist"});
            }else{
                request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
                    form: {
                  accessCode:accessCode,
                  requestId:reqId,
                  encRequest:encdata,
                  ver:'1.0',
                  instituteId:'IA83'
                    }
                  }, function (err, httpResponse, body) { 
                      
                     // console.log(body);
              
                      console.log("Body ends");
                    
                      if(err){
                          console.log(err);
                      }
                     
                      try {
                    
                        var decrypted=decrypt(body,workingkey);
                        result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                         console.log(result2)
                              
                              console.log("check asap");
                              var senderDetailResponse = result2.dmtServiceResponse;
                              console.log(senderDetailResponse);
                              if(senderDetailResponse.responseCode._text=="000"){
                                res.json({success:true,msg:"Verified Sender Details",data:senderDetailResponse});
                              }else{
                                res.json({success:false,msg:"Response",data:senderDetailResponse}); 
                              }
                        
                           
                      }
                      catch (e) {
                                    console.log("error"+e);
                                  var decrypted = "";
                                  res.json({success:false,msg:"Failed Sender Details"});
                                }
                           
                           });
            }
        })
            });


            //Get All Recipient

        apiRoutes.post('/getAllDmtRecipient', function(req, res){
            var transactionType =req.body.transactionType;
            var senderMobile =req.body.senderMobileNumber.toString();
            var userId =req.body.userId;
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = randomstring.generate(35);
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
        var req = '<dmtServiceRequest><requestType>AllRecipient</requestType><senderMobileNumber>'+senderMobile+'</senderMobileNumber><txnType>'+transactionType+'</txnType></dmtServiceRequest>';
        
        console.log(req);
console.log("check please");
        var encdata= encrypt(req,workingkey);
        
        User.find({'_id':userId},function(err,user){
            if(err){
                res.json({success:false,msg:"No Agent Exist"});
            }else{
                request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
                    form: {
                  accessCode:accessCode,
                  requestId:reqId,
                  encRequest:encdata,
                  ver:'1.0',
                  instituteId:'IA83'
                    }
                  }, function (err, httpResponse, body) { 
                      
                     // console.log(body);
              
                      console.log("Body ends");
                    
                      if(err){
                          console.log(err);
                      }
                     
                      try {
                    
                        var decrypted=decrypt(body,workingkey);
                        result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                         console.log(result2)
                              
                              console.log("check asap");
                              var senderDetailResponse = result2.dmtServiceResponse;
                              console.log(senderDetailResponse);
                              if(senderDetailResponse.responseCode._text=="000"){
                                res.json({success:true,msg:"Verified Sender Details",data:senderDetailResponse});
                              }else{
                                res.json({success:false,msg:"Response",data:senderDetailResponse}); 
                              }
                        
                           
                      }
                      catch (e) {
                                    console.log("error"+e);
                                  var decrypted = "";
                                  res.json({success:false,msg:"Failed Sender Details"});
                                }
                           
                           });
            }
        })
            });


                //Register Recipient

        apiRoutes.post('/registerRecipient', function(req, res){
            var userId=userId;
            var transactionType =req.body.transactionType;
            var senderMobile =req.body.senderMobile.toString();
            var recipientName =req.body.recipientName;
            var recipientMobile =req.body.recipientMobile.toString();
            var recipientBankCode =req.body.bankCode;
            var recipientBankAccount =req.body.recipientBankAccount;
            var recipientIfsc =req.body.recipientIfsc;
            
            
            console.log(transactionType);
            
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = randomstring.generate(35);
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
        var req = '<dmtServiceRequest><requestType>RegRecipient</requestType><senderMobileNumber>'+senderMobile+'</senderMobileNumber><txnType>'+transactionType+'</txnType><recipientName>'+recipientName+'</recipientName><recipientMobileNumber>'+recipientMobile+'</recipientMobileNumber><bankCode>'+recipientBankCode+'</bankCode><bankAccountNumber>'+recipientBankAccount+'</bankAccountNumber><ifsc>'+recipientIfsc+'</ifsc></dmtServiceRequest>';
        console.log(req);
        console.log(reqId);
console.log("check please");
        
        var encdata= encrypt(req,workingkey);
        
        User.find({'_id':userId},function(err,user){
            if(err){
                res.json({success:false,msg:"No Agent Exist"});
            }else{
                request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
                    form: {
                  accessCode:accessCode,
                  requestId:reqId,
                  encRequest:encdata,
                  ver:'1.0',
                  instituteId:'IA83'
                    }
                  }, function (err, httpResponse, body) { 
                      
                     // console.log(body);
              
                      console.log("Body ends");
                    
                      if(err){
                          console.log(err);
                      }
                     
                      try {
                    
                        var decrypted=decrypt(body,workingkey);
                        result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                         console.log(result2)
                              
                              console.log("check asap");
                              var senderDetailResponse = result2.dmtServiceResponse;
                              console.log(senderDetailResponse);
                              if(senderDetailResponse.responseCode._text=="000"){
                                res.json({success:true,msg:"Verified Sender Details",data:senderDetailResponse});
                              }else{
                                res.json({success:false,msg:"Response",data:senderDetailResponse}); 
                              }
                        
                           
                      }
                      catch (e) {
                                    console.log("error"+e);
                                  var decrypted = "";
                                  res.json({success:false,msg:"Failed Sender Details"});
                                }
                           
                           });
            }
        })
            });

       //Delete Recipient

       apiRoutes.post('/deleteRecipient', function(req, res){
        var transactionType =req.body.transactionType;
        var senderMobile =req.body.senderMobile;
        var recipientId =req.body.recipientId;
        var userId =req.body.userId;
    var accessCode= 'AVTM87KZ80ZB95QHDY';
    var reqId = randomstring.generate(35);
    var workingkey='C516828299E4BE70B6B0612ACFE795FD';
    var req = '<dmtServiceRequest><requestType>DelRecipient</requestType><senderMobileNumber>'+senderMobile+'</senderMobileNumber><txnType>'+transactionType+'</txnType><recipientId>'+recipientId+'</recipientId></dmtServiceRequest>';
    console.log(req);
console.log("check please");
    
    var encdata= encrypt(req,workingkey);
    
    User.find({'_id':userId},function(err,user){
        if(err){
            res.json({success:false,msg:"No Agent Exist"});
        }else{
            request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
                form: {
              accessCode:accessCode,
              requestId:reqId,
              encRequest:encdata,
              ver:'1.0',
              instituteId:'IA83'
                }
              }, function (err, httpResponse, body) { 
                  
                 // console.log(body);
          
                  console.log("Body ends");
                
                  if(err){
                      console.log(err);
                  }
                 
                  try {
                
                    var decrypted=decrypt(body,workingkey);
                    result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                     console.log(result2)
                          
                          console.log("check asap");
                          var senderDetailResponse = result2.dmtServiceResponse;
                          console.log(senderDetailResponse);
                          if(senderDetailResponse.responseCode._text=="000"){
                            res.json({success:true,msg:"Verified Sender Details",data:senderDetailResponse});
                          }else{
                            res.json({success:false,msg:"Response",data:senderDetailResponse}); 
                          }
                    
                       
                  }
                  catch (e) {
                                console.log("error"+e);
                              var decrypted = "";
                              res.json({success:false,msg:"Failed Sender Details"});
                            }
                       
                       });
        }
    })
        });
            
//Get Bank List

apiRoutes.post('/getBankList', function(req, res){
    
    var transactionType =req.body.transactionType;
    var userId =req.body.userId;
   

User.find({'_id':userId},function(err,user){
    if(err){
        res.json({success:false,msg:"No Agent Exist"});
    }else{
        Bank.find({}, function(err, banks){
            if(err){
                res.json({success:false,msg:"Failed to get Bank Details"});
            }else{
                res.json({success:true,msg:"Retrieved Bank Details", data:banks});
            }
        })
    }
})
    });


//Verify Bank Account

apiRoutes.post('/verifyBankAccount', function(req, res){
    var userId =req.body.userId;
    var senderMobile =req.body.senderMobile.toString();
    var bankCode =req.body.bankCode;
    var bankAccountNumber =req.body.recipientBankAccount;
    console.log(typeof(bankAccountNumber))
    var bankIfsc =req.body.recipientIfsc;
  
var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
var workingkey='C516828299E4BE70B6B0612ACFE795FD';
var req = '<dmtServiceRequest> <agentId>CC01IA83MOBA00000001</agentId> <requestType>VerifyBankAcct</requestType> <senderMobileNumber>'+senderMobile+'</senderMobileNumber> <bankCode>'+bankCode+'</bankCode> <bankAccountNumber>'+bankAccountNumber+'</bankAccountNumber> <ifsc>'+bankIfsc+'</ifsc><initChannel>AGT</initChannel></dmtServiceRequest>';
console.log(req);
console.log(reqId);
console.log("check please");

var encdata= encrypt(req,workingkey);

User.find({'_id':userId},function(err,user){
    if(err){
        res.json({success:false,msg:"No Agent Exist"});
    }else{
        request.post('https://api.billavenue.com/billpay/dmt/dmtServiceReq/xml', {
            form: {
          accessCode:accessCode,
          requestId:reqId,
          encRequest:encdata,
          ver:'1.0',
          instituteId:'IA83'
            }
          }, function (err, httpResponse, body) { 
              
             // console.log(body);
      
              console.log("Body ends");
            
              if(err){
                  console.log(err);
              }
             
              try {
            
                var decrypted=decrypt(body,workingkey);
                result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                 console.log(result2)
                      
                      console.log("check asap");
                      var senderDetailResponse = result2.dmtServiceResponse;
                      console.log(senderDetailResponse);
                      if(senderDetailResponse.responseCode._text=="000"){
                        res.json({success:true,msg:"Verified Sender Details",data:senderDetailResponse});
                      }else{
                        res.json({success:false,msg:"Response",data:senderDetailResponse}); 
                      }
                
                   
              }
              catch (e) {
                            console.log("error"+e);
                          var decrypted = "";
                          res.json({success:false,msg:"Failed Sender Details"});
                        }
                   
                   });
    }
})
    });


    //Transaction Refund Request DMT

apiRoutes.post('/transactionRefundDmt', function(req, res){
    var userId =req.body.userId;
    var txnId =req.body.txnId;
var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
var workingkey='C516828299E4BE70B6B0612ACFE795FD';
var req = '<dmtTransactionRequest><agentId>CC01IA83MOBA00000001</agentId><initChannel>AGT</initChannel><requestType>TxnRefund</requestType><txnId>'+txnId+'</txnId></dmtTransactionRequest>';
console.log(req);
console.log("check please");

var encdata= encrypt(req,workingkey);

request.post('https://api.billavenue.com/billpay/dmt/dmtTransactionReq/xml', {
form: {
accessCode:accessCode,
requestId:reqId,
encRequest:encdata,
ver:'1.0',
instituteId:'IA83'
}
}, function (err, httpResponse, body) { 

// console.log(body);

console.log("Body ends");

if(err){
    console.log(err);
}

try {

  var decrypted=decrypt(body,workingkey);
  result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
   console.log(result2)
        
        console.log("check asap");
        var senderDetailResponse = result2.dmtServiceResponse;
        console.log(senderDetailResponse);
    //  billerFetchResponse = result2.billFetchResponse.billerResponse;
    //  console.log(billerFetchResponse);
    //  console.log("checknow");
     
}
catch (e) {
              console.log("error"+e);
            var decrypted = "";
            
          }
     
     });
    });

    //Transaction Refund Verify

apiRoutes.post('/transactionRefundVerify', function(req, res){
    var amount =req.body.amount;
    var mobileNumber =req.body.mobileNumber;
var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
var workingkey='C516828299E4BE70B6B0612ACFE795FD';
var req = '<dmtTransactionRequest><agentId>CC01IA83MOBA00000001</agentId><initChannel>AGT</initChannel><requestType>VerifyRefundOtp</requestType><txnId>'+txnId+'</txnId><uniqueRefId>'+uniqueRefId+'</uniqueRefId><otp>'+otp+'</otp></dmtTransactionRequest>';
console.log(req);
console.log("check please");

var encdata= encrypt(req,workingkey);

request.post('https://api.billavenue.com/billpay/dmt/dmtTransactionReq/xml', {
form: {
accessCode:accessCode,
requestId:reqId,
encRequest:encdata,
ver:'1.0',
instituteId:'IA83'
}
}, function (err, httpResponse, body) { 

// console.log(body);

console.log("Body ends");

if(err){
    console.log(err);
}

try {

  var decrypted=decrypt(body,workingkey);
  result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
   console.log(result2)
        
        console.log("check asap");
        var senderDetailResponse = result2.dmtServiceResponse;
        console.log(senderDetailResponse);
    //  billerFetchResponse = result2.billFetchResponse.billerResponse;
    //  console.log(billerFetchResponse);
    //  console.log("checknow");
     
}
catch (e) {
              console.log("error"+e);
            var decrypted = "";
            
          }
     
     });
    });

     // Fund Transfer

apiRoutes.post('/fundTransfer', function(req, res){
    var txnAmount =req.body.txnAmount;
    var senderMobile =req.body.senderMobile.toString();
    var recipientId =req.body.recipientId;
    var transactionType =req.body.transactionType;
    var userId=req.body.userId;
    var recipientName=req.body.recipientName;
    var convFee = req.body.convFee.toString();
    var totalamt = req.body.convFee/100;
var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
var workingkey='C516828299E4BE70B6B0612ACFE795FD';
var req = "<dmtTransactionRequest> <requestType>FundTransfer</requestType> <senderMobileNo>"+senderMobile+"</senderMobileNo> <agentId>CC01IA83MOBA00000001</agentId> <initChannel>AGT</initChannel> <recipientId>"+recipientId+"</recipientId> <txnAmount>"+txnAmount+"</txnAmount> <convFee>"+convFee+"</convFee> <txnType>"+transactionType+"</txnType></dmtTransactionRequest>"
// var req = '<dmtTransactionRequest><requestType>FundTransfer</requestType><senderMobileNo>'+senderMobile+'</senderMobileNo><agentId>CC01IA83MOBA00000001</agentId><initChannel>AGT</initChannel><recipientId>'+recipientId+'</recipientId><txnAmount>'+txnAmount+'</txnAmount><convFee>'+convFees+'</convFee><txnType>'+transactionType+'</txnType></dmtTransactionRequest>';
console.log(req);
console.log("check please");

var encdata= encrypt(req,workingkey);
User.find({'_id':userId},function(err,user){
    if(err){
        res.json({success:false,msg:"No Agent Exist"});
    }else{
        request.post('https://api.billavenue.com/billpay/dmt/dmtTransactionReq/xml', {
            form: {
          accessCode:accessCode,
          requestId:reqId,
          encRequest:encdata,
          ver:'1.0',
          instituteId:'IA83'
            }
          }, function (err, httpResponse, body) { 
              
             // console.log(body);
      
              console.log("Body ends");
            
              if(err){
                  console.log(err);
              }
             
              try {
            
                var decrypted=decrypt(body,workingkey);
                result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                 console.log(result2)
                      
                      console.log("check asap");
                      var dmtTransactionResponse = result2.dmtTransactionResponse;
                      console.log(dmtTransactionResponse);
                      
                      if(dmtTransactionResponse.responseCode._text=="000"){
                        console.log("check" + dmtTransactionResponse.uniqueRefId._text)
                          var txnData = {
                            txnAmount :totalamt,
                            senderMobile :senderMobile,
                            recipientId :recipientId,
                            transactionType :transactionType,
                            userId :userId,
                            recipientName:recipientName,
                            convFee:parseInt(convFee)/100,
                            txnRefId:dmtTransactionResponse.uniqueRefId._text
                          }
                    
                        DMT.addDmt(txnData, function(err, order){
                            if(err){
                                
                                res.json({success:false,msg:"Failed To add request",data:dmtTransactionResponse}); 
                            //	throw err;
                            }else{
                                var prevBalance = user.wallet;
                                var newBalance = prevBalance - totalamt;
                                User.update({'_id': userId}, { $set: {wallet:newBalance}}, function (err, updateuser) {
                                    //handle it
                                             if(err){
                                             res.json({success: false,msg:'Failed to Update'});
                                             //throw err;
                                             console.log(err);
                                         }else{
                                            res.json({success:true,msg:"Fund Transferred Successfully",data:dmtTransactionResponse,user:updateuser});
                                         }
                                        });
                               
                            }
                            
                            
                           
                        });
                       
                      }else{
                        res.json({success:false,msg:"Response",data:dmtTransactionResponse}); 
                      }
                
                   
              }
              catch (e) {
                            console.log("error"+e);
                          var decrypted = "";
                          res.json({success:false,msg:"Failed Sender Details"});
                        }
                   
                   });
    }
})

    });

//     apiRoutes.post('/addDMT', function(req, res){
//        var txnData=req.body;
//     DMT.addDmt(txnData, function(err, order){
//         if(err){
            
//             res.json({success:false,msg:"Failed To add request"}); 
//         //	throw err;
//         }else{
//             res.json({success:true,msg:"Fund Transferred Successfully"});
//             var prevBalance = user.wallet;
//             var newBalance = prevBalance - totalamt;
//             // User.update({'_id': userId}, { $set: {wallet:newBalance}}, function (err, updateuser) {
//             //     //handle it
//             //              if(err){
//             //              res.json({success: false,msg:'Failed to Update'});
//             //              //throw err;
//             //              console.log(err);
//             //          }else{
//             //             res.json({success:true,msg:"Fund Transferred Successfully",data:dmtTransactionResponse,user:updateuser});
//             //          }
//             //         });
           
//         }
        
        
       
//     });
// });


   

    // Get Cust Conv Fees

apiRoutes.post('/DmtConvFees', function(req, res){
    var txnAmount =req.body.txnAmount.toString();
    var mobileNumber =req.body.mobileNumber;
var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
var workingkey='C516828299E4BE70B6B0612ACFE795FD';
var req = '<dmtTransactionRequest><requestType>GetCCFFee</requestType><agentId>CC01IA83MOBA00000001</agentId><txnAmount>'+txnAmount+'</txnAmount></dmtTransactionRequest>';
console.log(req);
console.log("check please");

var encdata= encrypt(req,workingkey);

request.post('https://api.billavenue.com/billpay/dmt/dmtTransactionReq/xml', {
form: {
accessCode:accessCode,
requestId:reqId,
encRequest:encdata,
ver:'1.0',
instituteId:'IA83'
}
}, function (err, httpResponse, body) { 

// console.log(body);

console.log("Body ends");

if(err){
    console.log(err);
}

try {

  var decrypted=decrypt(body,workingkey);
  result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
   console.log(result2)
        
        console.log("check asap");
        var dmtTransactionResponse = result2.dmtTransactionResponse;
        console.log(dmtTransactionResponse);
        if(dmtTransactionResponse.responseCode._text=="000"){
            res.json({success:true,data:dmtTransactionResponse});
        }else{
            res.json({success:false,data:dmtTransactionResponse});
        }
    //  billerFetchResponse = result2.billFetchResponse.billerResponse;
    //  console.log(billerFetchResponse);
    //  console.log("checknow");
     
}
catch (e) {
              console.log("error"+e);
            var decrypted = "";
            
          }
     
     });
    });


     // Track DMT Transaction Status

apiRoutes.post('/multiTransactionStatus', function(req, res){
    // var amount =req.body.amount;
    var uniqueRefId =req.body.uniqueRefId;
var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
var workingkey='C516828299E4BE70B6B0612ACFE795FD';
var req = '<dmtTransactionRequest><agentId>CC01IA83MOBA00000001</agentId><initChannel>AGT</initChannel><requestType>MultiTxnStatus</requestType><uniqueRefId>'+uniqueRefId+'</uniqueRefId></dmtTransactionRequest>';
console.log(req);
console.log("check please");

var encdata= encrypt(req,workingkey);

request.post('https://api.billavenue.com/billpay/dmt/dmtTransactionReq/xml', {
form: {
accessCode:accessCode,
requestId:reqId,
encRequest:encdata,
ver:'1.0',
instituteId:'IA83'
}
}, function (err, httpResponse, body) { 

// console.log(body);

console.log("Body ends");

if(err){
    console.log(err);
}

try {

  var decrypted=decrypt(body,workingkey);
  result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
   console.log(result2)
        
        console.log("check asap");
        var senderDetailResponse = result2.dmtTransactionResponse;
        console.log(senderDetailResponse);
    //  billerFetchResponse = result2.billFetchResponse.billerResponse;
    //  console.log(billerFetchResponse);
    //  console.log("checknow");
    if(senderDetailResponse.responseCode._text=="000"){
        
        res.json({success:true,msg:"Fetched Status Successfully", data:senderDetailResponse});
    }else{
        res.json({success:true,msg:"Unable To Fetch Try Again Later.", data:senderDetailResponse})
    }
    
     
}
catch (e) {
              console.log("error"+e);
            var decrypted = "";
            
          }
     
     });
    });
  

    


          //Transaction Status

          apiRoutes.post('/transactionStatusMobileBbps', function(req, res){
            var amount =req.body.amount;
            var mobileNumber =req.body.mobileNumber;
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = randomstring.generate(35);
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
   var req = '<?xml version="1.0" encoding="UTF-8"?> <transactionStatusReq><trackType>MOBILE_NO</trackType> <trackValue>9876543210</trackValue> <fromDate>2016-09-02</fromDate> <toDate>2016-09-06</toDate></transactionStatusReq>'
  
    var encdata= encrypt(req,workingkey);
  
    request.post('https://api.billavenue.com/billpay/transactionStatus/fetchInfo/xml', {
      form: {
    accessCode:accessCode,
    requestId:reqId,
    encRequest:encdata,
    ver:'1.0',
    instituteId:'IA83'
      }
    }, function (err, httpResponse, body) { 
        
       // console.log(body);

        console.log("Body ends");
      
        if(err){
            console.log(err);
        }
       
        try {
      
          var decrypted=decrypt(body,workingkey);
          result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
           console.log(result2)
                
                console.log("check asap");
            //  billerFetchResponse = result2.billFetchResponse.billerResponse;
            //  console.log(billerFetchResponse);
            //  console.log("checknow");
             
        }
        catch (e) {
                      console.log("error"+e);
                    var decrypted = "";
                    
                  }
             
             });
            });

        //end


        //Transaction Status ref id

        apiRoutes.post('/transactionStatusBbps', function(req, res){
        
        var userId =req.body.userId;
      
        var transactionRefId = req.body.transactionRefId;

    
    var accessCode= 'AVTM87KZ80ZB95QHDY';
    var reqId = randomstring.generate(35);
    var workingkey='C516828299E4BE70B6B0612ACFE795FD';
var req = '<?xml version="1.0" encoding="UTF-8"?> <transactionStatusReq><trackType>TRANS_REF_ID</trackType><trackValue>'+transactionRefId+'</trackValue> </transactionStatusReq>'

var encdata= encrypt(req,workingkey);

request.post('https://api.billavenue.com/billpay/transactionStatus/fetchInfo/xml', {
  form: {
accessCode:accessCode,
requestId:reqId,
encRequest:encdata,
ver:'1.0',
instituteId:'IA83'
  }
}, function (err, httpResponse, body) { 
    
   // console.log(body);

    console.log("Body ends");
  
    if(err){
        console.log(err);
    }
   
    try {
  
      var decrypted=decrypt(body,workingkey);
      result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
       console.log(result2)
            
            console.log("check asap");
            if(result2.transactionStatusResp.responseCode._text=="000"){
                billerFetchResponse = result2.transactionStatusResp.transaction;
                console.log(billerFetchResponse);
                console.log("checknow");
                res.json({success: true, msg: "Retrieved Txn Details", data:result2.transactionStatusResp.txnList});
            }else{
                res.json({success: false, msg: result2.transactionStatusResp.errorInfo.error.errorMessage._text});
            }
       
         
    }
    catch (e) {
                  console.log("error"+e);
                var decrypted = "";
                
              }
         
         });
        });

        //end

        //Complaint Tracking

        apiRoutes.post('/trackbbpsComplaint', function(req, res){
            var userId =req.body.userId;
            var complaintType =req.body.complaintType;
            
            var complaintId =req.body.complaintId;
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = randomstring.generate(35);
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
        var req ='<?xml version="1.0" encoding="UTF-8"?> <complaintTrackingReq><complaintType>'+complaintType+'</complaintType><complaintId>'+complaintId+'</complaintId> </complaintTrackingReq>';
   
  
    var encdata= encrypt(req,workingkey);
  
    request.post('https://api.billavenue.com/billpay/extComplaints/track/xml', {
      form: {
    accessCode:accessCode,
    requestId:reqId,
    encRequest:encdata,
    ver:'1.0',
    instituteId:'IA83'
      }
    }, function (err, httpResponse, body) { 
        
       // console.log(body);

        console.log("Body ends");
      
        if(err){
            console.log(err);
        }
       
        try {
      
          var decrypted=decrypt(body,workingkey);
          result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
           console.log(result2)
                
                console.log("check asap");
             billerFetchResponse = result2.billFetchResponse.complaintTrackingResp;
             console.log(billerFetchResponse);
             console.log("checknow");
             if(billFetchResponse.respCode._text=="000"){
                res.json({success: true, msg: 'Status Fetched Successfully',data:billerFetchResponse});
             }else{
                res.json({success: false, msg: 'Failed to add Request'});
             }
        }
        catch (e) {
                      console.log("error"+e);
                    var decrypted = "";
                    
                  }
             
             });
            });

        //end


        //Complaint Registration  Transaction Type

        apiRoutes.post('/registerComplaintTransaction', function(req, res){
            var userId =req.body.userId;
            var requestId = req.body.requestId;
            var billerId = req.body.billerId
            var complaintType =req.body.complaintType;
            var complaintDescription = req.body.complaintDescription;
            var servReason = req.body.servReason;
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = randomstring.generate(35);
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
        var req = '<?xml version="1.0" encoding="UTF-8"?> <complaintRegistrationReq><complaintType>'+complaintType+'</complaintType> <participationType>AGENT</participationType> <agentId>CC01IA83MOBA00000001</agentId> <txnRefId>'+requestId+'</txnRefId><billerId>'+billerId+'<billerId><complaintDesc>'+complaintDescription+'</complaintDesc> <servReason>'+servReason+'</servReason> <complaintDisposition /></complaintRegistrationReq>'
  
    var encdata= encrypt(req,workingkey);
  
    request.post('https://api.billavenue.com/billpay/extComplaints/register/xml', {
      form: {
    accessCode:accessCode,
    requestId:reqId,
    encRequest:encdata,
    ver:'1.0',
    instituteId:'IA83'
      }
    }, function (err, httpResponse, body) { 
        
       // console.log(body);

        console.log("Body ends");
      
        if(err){
            console.log(err);
        }
       
        try {
      
          var decrypted=decrypt(body,workingkey);
          result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
           console.log(result2)
                
                console.log("check asap");
             billerFetchResponse = result2.complaintRegistrationResp;
             console.log(billerFetchResponse);
             console.log("checknow");
             if(billFetchResponse.respCode._text=="000"){
                var complaintData = {
                    "billerId":billerId,
                    "complaintDescription":complaintDescription,
                    "complaintType":complaintType,
                    "servReason":servReason,
                    "requestId":reqId.txnRefId._text,
                    "complaintId":billFetchResponse.complaintId._text,
                    "complaintAssigned":billFetchResponse.complaintAssigned._text,
               
                "userId":userId,
            
               
                }
                Complaint.addComplaint(complaintData, function(err, complaint){
                    if(err){
                        
                        res.json({success: false, msg: 'Failed to add Request'});
                    //	throw err;
                    }else{
                        res.json({success: true, msg: "Complaint Registered Successfully",data:complaint});
                    }
                    
                    
                   
                });
            }else{
                res.json({success: false, msg: 'Failed to add Request'});
            }
            
             
        }
        catch (e) {
                      console.log("error"+e);
                    var decrypted = "";
                    
                  }
             
             });
            });

        //end


        //Complaint Registration  Service Type

        apiRoutes.post('/registerComplaintService', function(req, res){
            var amount =req.body.amount;
            var mobileNumber =req.body.mobileNumber;
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = randomstring.generate(35);
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
   var req = '<?xml version="1.0" encoding="UTF-8"?><complaintRegistrationReq><complaintType>Service</complaintType>'
  
    var encdata= encrypt(req,workingkey);
  
    request.post('https://api.billavenue.com/billpay/extComplaints/register/xml', {
      form: {
    accessCode:accessCode,
    requestId:reqId,
    encRequest:encdata,
    ver:'1.0',
    instituteId:'IA83'
      }
    }, function (err, httpResponse, body) { 
        
       // console.log(body);

        console.log("Body ends");
      
        if(err){
            console.log(err);
        }
       
        try {
      
          var decrypted=decrypt(body,workingkey);
          result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
           console.log(result2)
                
                console.log("check asap");
            //  billerFetchResponse = result2.billFetchResponse.billerResponse;
            //  console.log(billerFetchResponse);
            //  console.log("checknow");
             
        }
        catch (e) {
                      console.log("error"+e);
                    var decrypted = "";
                    
                  }
             
             });
            });

        //end

//BILL PAY REQUEST ***************** BBPS (some error)
        apiRoutes.post('/paybbpsBill', function(req, res){
            
            var userId=req.body.userId;
            var billerAdhoc=req.body.billerAdhoc;
            var contactNum =req.body.contactNum;
            var billerId = req.body.billerId;
            var paramName = req.body.paramName;
            var paramValue = req.body.paramValue;
            var billAmount = req.body.billAmount;
            var billDate = req.body.billDate;
            var dueDate = req.body.dueDate;
            var billPeriod = req.body.billPeriod;
            var billNumber = req.body.billNumber;
            var consumerName = req.body.consumerName;
            var billerName = req.body.billerName;
            var paymentMode=req.body.paymentMode;
           
            var convFees=req.body.convFees;


            var billerResponse =  req.body.billerResponse;
            console.log(billerResponse);
console.log(billerId +"billerId & biller resp conv. ends");
console.log(paramName+"paramName");
console.log(paramValue+"paramValue");
console.log(billAmount+"billAmount");
console.log(billDate+"billDate");
console.log(dueDate+"dueDate");
console.log(billPeriod+"billPeriod");
console.log(billNumber+"billNumber");
console.log(consumerName+"consumerName");
console.log(billerName+"billerName");
console.log(convFees+"convFees");
convFees=(convFees).toString();
        var accessCode= 'AVTM87KZ80ZB95QHDY';
        var reqId = req.body.requestId;
        var workingkey='C516828299E4BE70B6B0612ACFE795FD';
        var req = '<?xml version="1.0" encoding="UTF-8"?><billPaymentRequest><agentId>CC01IA83MOBA00000001</agentId><billerAdhoc>'+billerAdhoc+'</billerAdhoc><agentDeviceInfo><ip>13.232.172.92</ip><initChannel>MOB</initChannel><imei>448674528976410</imei><app>TripperApp</app><os>Android</os></agentDeviceInfo><customerInfo><customerMobile>'+contactNum+'</customerMobile><customerEmail></customerEmail><customerAdhaar></customerAdhaar><customerPan></customerPan></customerInfo><billerId>'+billerId+'</billerId><inputParams>'+paramValue+'</inputParams><'+billerResponse+'<amountInfo><amount>'+billAmount+'</amount><currency>356</currency><custConvFee>'+convFees+'</custConvFee><amountTags></amountTags></amountInfo><paymentMethod><paymentMode>Credit Card</paymentMode><quickPay>N</quickPay><splitPay>N</splitPay></paymentMethod><paymentInfo><info><infoName>CardNum</infoName><infoValue>420078XXXXXX5678</infoValue></info><info><infoName>AuthCode</infoName><infoValue>123456789</infoValue></info></paymentInfo></billPaymentRequest>';
//    var req = '<?xml version="1.0" encoding="UTF-8"?><billPaymentRequest><agentId>CC01IA83MOBA00000001</agentId><billerAdhoc>true</billerAdhoc><agentDeviceInfo><ip>13.232.172.92</ip><initChannel>MOB</initChannel><imei>448674528976410</imei><app>TripperApp</app><os>Android</os></agentDeviceInfo><customerInfo><customerMobile>'+contactNum+'</customerMobile></customerInfo><billerId>'+billerId+'</billerId><inputParams><input><paramName>'+paramName+'</paramName><paramValue>'+paramValue+'</paramValue></input></inputParams><billerResponse><billAmount>'+billAmount+'</billAmount><billDate>'+billDate+'</billDate><billNumber>'+billNumber+'</billNumber><billPeriod>'+billPeriod+'</billPeriod><customerName>'+consumerName+'</customerName><dueDate>'+dueDate+'</dueDate></billerResponse></billPaymentRequest>'
  
    var encdata= encrypt(req,workingkey);
  
    request.post('https://api.billavenue.com/billpay/extBillPayCntrl/billPayRequest/xml', {
      form: {
    accessCode:accessCode,
    requestId:reqId,
    encRequest:encdata,
    ver:'1.0',
    instituteId:'IA83'
      }
    }, function (err, httpResponse, body) { 
        
       // console.log(body);

        console.log("Body ends");
      
        if(err){
            console.log(err);
        }
       
        try {
      
          var decrypted=decrypt(body,workingkey);
          result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
           console.log(result2)
                
                console.log("check asap");
            //  billerFetchResponse = result2.billFetchResponse.billerResponse;
            //  console.log(billerFetchResponse);
            //  console.log("checknow");
            if(result2.ExtBillPayResponse.responseCode._text=="000"){

                var orderData = {
                    "billerId":billerId,
                    "amount":billAmount,
                    "billDate":billDate,
                    "dueDate":dueDate,
                    "contactNum":contactNum,
                    "billPeriod":billPeriod,
                    "billNumber":billNumber,
                    "consumerName":consumerName,
                    "requestId":reqId.txnRefId._text,
                    "paramName":paramName,
                    "paramValue":paramValue,
                    "paymentMode":paymentMode,
                    "convfee":parseFloat(convFees),
                "userId":userId,
                "billerName":billerName,
               
                }
                BBPS.addBbps(orderData, function(err, order){
                    if(err){
                        
                        res.json({success: false, msg: 'Failed to add Request'});
                    //	throw err;
                    }else{
                        var isAgentPayment = req.body.isAgentPayment;
                        if(isAgentPayment){
             User.findOne({'_id':userId},function(err,user){
                 if(err){
                    res.json({success: false, msg:"Didnt Find User. Bill Paid"});
                 }else{
                    var prevBalance=user.wallet;
                    var newBalance=prevBalance-billAmount;
                    User.update({'_id': userId}, { $set: {wallet:newBalance}}, function (err, updateuser) {
                        //handle it
                                 if(err){
                                 res.json({success: false,msg:'Failed to Update'});
                                 //throw err;
                                 console.log(err);
                             }else{
                                res.json({success: true, msg: "Bill Paid Successfully", res:result2.ExtBillPayResponse,order:order});
                             }
                            });
                 }
             })
                        }else{
                            if(req.body.convFees==0){
                                User.findOne({'_id':userId},function(err,user){
                                    if(err){
                                       res.json({success: false, msg:"Didnt Find User. Bill Paid"});
                                    }else{
                                       var freeTransactions=user.freeTransactions;
                                       var newFreeTransactions=freeTransactions-1;
                                       User.update({'_id': userId}, { $set: {freeTransactions:newFreeTransactions}}, function (err, updateuser) {
                                           //handle it
                                                    if(err){
                                                    res.json({success: false,msg:'Failed to Update'});
                                                    //throw err;
                                                    console.log(err);
                                                }else{
                                                   res.json({success: true, msg: "Bill Paid Successfully", res:result2.ExtBillPayResponse,order:order});
                                                }
                                               });
                                    }
                                })
                                res.json({success: true, msg: "Bill Paid Successfully", res:result2.ExtBillPayResponse,order:order});
                            }else{
                                res.json({success: true, msg: "Bill Paid Successfully", res:result2.ExtBillPayResponse,order:order});
                            }
                            
                        }
                        
                    }
                    
                    
                   
                });
                
               }else{
                console.log(result2.ExtBillPayResponse.errorInfo.error);
                console.log("error");
                res.json({success: false, msg: result2.ExtBillPayResponse.errorInfo.error.errorMessage._text});
               }
            
             
        }
        catch (e) {
                      console.log("error"+e);
                    var decrypted = "";
                    
                  }
             
             });
            });


    //Bill Fetch API ***************** BBPS

     apiRoutes.post('/getBill', function(req, res){
        var billFetchResponse =[];
var os = 'Android';
var contactNum=req.body.contactNum;
var billerId= req.body.billerId;
var paramName= req.body.paramName;
var paramValue= req.body.paramValue;
console.log(os);
console.log(contactNum);
console.log(billerId);
console.log(paramName);
console.log(paramValue);
var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
    var workingkey='C516828299E4BE70B6B0612ACFE795FD';
    // var req='<billFetchRequest><agentId>CC01IA83MOBA00000001</agentId><agentDeviceInfo><ip>13.232.172.92</ip><initChannel>MOB</initChannel><imei>448674528976410</imei><app>AIAPP</app><os>Android</os></agentDeviceInfo><customerInfo><customerMobile>9898990084</customerMobile><customerEmail></customerEmail><customerAdhaar></customerAdhaar><customerPan></customerPan></customerInfo><billerId>OTME00005XXZ43</billerId><inputParams><input><paramName>a</paramName><paramValue>10</paramValue></input><input><paramName>a b</paramName><paramValue>20</paramValue></input><input><paramName>a b c</paramName><paramValue>30</paramValue></input><input><paramName>a b c d</paramName><paramValue>40</paramValue></input><input><paramName>a b c d e</paramName><paramValue>50</paramValue></input></inputParams></billFetchRequest>';
    var req='<billFetchRequest><agentId>CC01IA83MOBA00000001</agentId><agentDeviceInfo><ip>13.232.172.92</ip><initChannel>MOB</initChannel><imei>448674528976410</imei><app>TripperApp</app><os>'+os+'</os></agentDeviceInfo><customerInfo><customerMobile>'+contactNum+'</customerMobile></customerInfo><billerId>'+billerId+'</billerId><inputParams>'+paramValue+'</inputParams></billFetchRequest>';

var encdata= encrypt(req,workingkey);

request.post('https://api.billavenue.com/billpay/extBillCntrl/billFetchRequest/xml', {
  form: {
accessCode:accessCode,
requestId:reqId,
encRequest:encdata,
ver:'1.0',
instituteId:'IA83'
  }
}, function (err, httpResponse, body) { 
    
   // console.log(body);

    console.log("Body ends");
  
    if(err){
        console.log(err);
       
        var decrypted = "";
        res.json({success: false, msg: 'Bill Info Could Not Fetch Successfully',res:err});
    }
   
    else {
  
      var decrypted=decrypt(body,workingkey);
      console.log(decrypted);
      console.log("decrypted ends here");
      var mySubString = decrypted.substring(
        decrypted.lastIndexOf("<billerResponse>") + 1, 
        decrypted.lastIndexOf("</billFetchResponse>")
    );
    console.log(mySubString);
    console.log("mySubString ends here");
      result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
       console.log(result2);
       console.log("result");
       if(result2.billFetchResponse.responseCode._text=="000"){
        res.json({success: true, msg: "Bill Details Fetched", res:result2.billFetchResponse,reqId:reqId,mySubString:mySubString});
       }else{
        console.log(result2.billFetchResponse.errorInfo.error);
        console.log("error");
        res.json({success: false, msg: result2.billFetchResponse.errorInfo.error.errorMessage._text});
       }
     
        //     console.log("check asap");
        //     billFetchResponse = result2.billFetchResponse.billerResponse;
        //     console.log(billFetchResponse);
        //     console.log("checknow");
        //  res.json({success: true, msg: 'Bill Info Fetched Successfully',res:billFetchResponse});
    }
   
         
         });
        });



        //Get BBPS Order info


        apiRoutes.post('/getBbpsbill', function(req, res){
           
 
    
            var userId=req.body.userId;
           
        var billNumber = req.body.billNumber;
            if(userId!=undefined){
                User.findOne({ '_id': userId }, function(err, user) {
                 
                    if(!user){
                        res.json({success: false, msg: 'No user exist'});
                    }
                          
                     else 
                     
                     {
                        BBPS.findOne({'billNumber':billNumber},function(err, bill) {
                 
                            if(!billers){
                                res.json({success: false, msg: 'No Biller exist'});
                            }
                                  
                             else 
                             
                             {
                                res.json({success: true, msg:  'Bill Found', data:bill});
                             }
                            });
                     }
                    });
                   
                }
            });



             //Get Sender Transactions

             apiRoutes.post('/senderTransactions', function(req, res){
           
 
    
                var userId=req.body.userId;
                var senderMobile=req.body.senderMobile;
            
                if(userId!=undefined){
                    User.findOne({ '_id': userId }, function(err, user) {
                     
                        if(!user){
                            res.json({success: false, msg: 'No user with this Contact Number exist'});
                        }
                              
                         else 
                         
                         {
                            DMT.find({'userId':userId,'senderMobile':senderMobile},function(err, transactions) {
                     
                                if(!transactions){
                                    res.json({success: false, msg: 'No Biller exist'});
                                }
                                      
                                 else 
                                 
                                 {
                                    res.json({success: true, msg:  'Transactions Fetched', transactions:transactions});
                                 }
                                });
                         }
                        });
                       
                    }
                });

    

    //Get INDIVIDUAL BILLER INFO ***************** BBPS

        apiRoutes.post('/getHousingBiller', function(req, res){
           
 
    
    var contactNum=req.body.contactNum;
   

    if(contactNum!=undefined){
        User.findOne({ 'contactNum': contactNum }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                HousingSociety.find(function(err, billers) {
         
                    if(!billers){
                        res.json({success: false, msg: 'No Biller exist'});
                    }
                          
                     else 
                     
                     {
                        res.json({success: true, msg:  'Biller Found', res:billers});
                     }
                    });
             }
            });
           
        }
    });


     //Get recharge orders

     apiRoutes.post('/getRechargeOrders', function(req, res){
           
 
    
        var contactNum=req.body.contactNum;
        var userId=req.body.userId;
        var rechargeType = req.body.rechargeType;
    var offers=[];
        if(contactNum!=undefined){
            User.findOne({ '_id': userId }, function(err, user) {
             
                if(!user){
                    res.json({success: false, msg: 'No user with this Contact Number exist'});
                }
                      
                 else 
                 
                 {
                    Recharge.find({"userId":userId,'rechargeType':rechargeType}, function(err, recharges) {
             
                        if(err){
                            res.json({success: false, msg: 'Some error'});
                        }
                              
                         else 
                         
                         {
                            res.json({success: true, msg:  'Retrieved Found', recharges:recharges,offers:offers});
                         }
                        });
                 }
                });
               
            }
        });


        //Get Complaints

        apiRoutes.post('/getregisteredComplaints', function(req, res){
           
 
        
            var userId=req.body.userId;
           
        
            if(userId!=undefined){
                User.findOne({ '_id': userId }, function(err, user) {
                 
                    if(!user){
                        res.json({success: false, msg: 'No user with this Contact Number exist'});
                    }
                          
                     else 
                     
                     {
                        Complaint.find({'userId':userId},function(err, complaints) {
                 
                            if(!complaints){
                                res.json({success: false, msg: 'No Complaints exist'});
                            }
                                  
                             else 
                             
                             {
                                res.json({success: true, msg:  'Complaints Found', data:complaints});
                             }
                            });
                     }
                    });
                   
                }
            });

    //Education Biller

    apiRoutes.post('/getEducationBiller', function(req, res){
           
 
        
        var contactNum=req.body.contactNum;
       
    
        if(contactNum!=undefined){
            User.findOne({ 'contactNum': contactNum }, function(err, user) {
             
                if(!user){
                    res.json({success: false, msg: 'No user with this Contact Number exist'});
                }
                      
                 else 
                 
                 {
                    EducationFees.find(function(err, billers) {
             
                        if(!billers){
                            res.json({success: false, msg: 'No Biller exist'});
                        }
                              
                         else 
                         
                         {
                            res.json({success: true, msg:  'Biller Found', res:billers});
                         }
                        });
                 }
                });
               
            }
        });

        //Landline Biller

        
    apiRoutes.post('/landlineBiller', function(req, res){
           
 
        
        var contactNum=req.body.contactNum;
       
    
        if(contactNum!=undefined){
            User.findOne({ 'contactNum': contactNum }, function(err, user) {
             
                if(!user){
                    res.json({success: false, msg: 'No user with this Contact Number exist'});
                }
                      
                 else 
                 
                 {
                    LandlinePostpaid.find(function(err, billers) {
             
                        if(!billers){
                            res.json({success: false, msg: 'No Biller exist'});
                        }
                              
                         else 
                         
                         {
                            res.json({success: true, msg:  'Biller Found', res:billers});
                         }
                        });
                 }
                });
               
            }
        });


        apiRoutes.post('/getActiveSubscription', function(req, res){
           
 
        
            var userId=req.body.userId;
            var subscriptionId=req.body.subscriptionId;
            
        
            if(userId!=undefined){
                User.findOne({ '_id': userId }, function(err, user) {
                 
                    if(!user){
                        res.json({success: false, msg: 'No user with this Contact Number exist'});
                    }
                          
                     else 
                     
                     {
                        ActiveSubscription.find({'userId':userId,'subscriptionRef':subscriptionId},function(err, subscription) {
                 
                            if(subscription.length==0){
                                res.json({success: false, msg: 'Not Subscribed!',res:subscription});
                            }
                                  
                             else 
                             
                             {
                                res.json({success: true, msg:  'Subscribed!', res:subscription});
                             }
                            });
                     }
                    });
                   
                }
            });


        apiRoutes.post('/buySubscription', function(req, res){
           
 
        
            var contactNum=req.body.contactNum;
           
            var userId=req.body.userId;
            var amount=req.body.amount;
            var refId=req.body.refId;
            var freeTransactions=req.body.freeTransactions;
            var validity=req.body.validity;
            var startDate=req.body.startDate;
            var endDate=req.body.endDate;
            var freeTransactions=req.body.freeTransactions;
            var subscriptionId=req.body.subscriptionId;
            

var orderData = {
'userId':userId,
'amount':amount,
'refId':refId,
'freeTransaction':freeTransactions,
'validity':validity,
'startDate':startDate,
'endDate':endDate,
'subscriptionRef':subscriptionId
}
            if(userId!=undefined){
                User.findOne({ '_id': userId }, function(err, user) {
                 
                    if(!user){
                        res.json({success: false, msg: 'No user with this Contact Number exist'});
                    }
                          
                     else 
                     
                     {
                        ActiveSubscription.addsubscriptionOrder(orderData,function(err, order){
                                                                if(err){
                                                                    console.log(err);
                                                                    
                                                             
                                                                }
                                                                else{
                                                                    User.update({'_id': userId}, { $set: {
                                                                        freeTransactions:freeTransactions,
                                                                        isSubscribed:true}}, function (err, user) {
                                                                        //handle it
                                                                                 if(err){
                                                                                 res.json({success: false,msg:'Failed to Update Details.'});
                                                                                 //throw err;
                                                                                 console.log(err);
                                                                             }
                                                                                 else{
                                                                     
                                                                                             res.json({success: true,msg:'Subscription Activated!',user:user,order:order});
                                                                                      
                                                                                    
                                                                                 }
                                                                         });
                                                                }
                                                            });
                     }
                    });
                   
                }
            });

        
    

        apiRoutes.post('/fetchBbpsBills', function(req, res){
           
 
        
            var userId=req.body.userId;
           
        var allTransactionsArray=[];
            if(userId!=undefined){
                User.findOne({ '_id': userId }, function(err, user) {
                 
                    if(!user){
                        res.json({success: false, msg: 'No user with this Contact Number exist'});
                    }
                          
                     else 
                     
                     {
                        BBPS.find({'userId':userId},function(err, transactions) {
                 
                            if(!transactions){
                                res.json({success: false, msg: 'No Transaction exist'});
                            }
                                  
                             else 
                             
                             {
                                if(transactions.length!=0){
                                    transactions.forEach(element => {
                                        element.amount = element.amount / 100;
                                        element._doc.type="BBPS";
                                        allTransactionsArray.push(element);
                                    });
                                }
                                Recharge.find({'userId':userId},function(err, recharges) {
                 
                                    if(!transactions){
                                        res.json({success: false, msg: 'No Transaction exist'});
                                    }
                                          
                                     else 
                                     
                                     {
                                        if(recharges.length!=0){
                                            recharges.forEach(element => {
                                                element._doc.type="Recharge";
                                                allTransactionsArray.push(element);
                                            });
                                        }
                                        Voucher.find({'userId':userId},function(err, vouchers) {
                 
                                            if(!transactions){
                                                res.json({success: false, msg: 'No Transaction exist'});
                                            }
                                                  
                                             else 
                                             
                                             {
                                                if(vouchers.length!=0){
                                                    vouchers.forEach(element => {
                                                        element._doc.type="Voucher";
                                                        allTransactionsArray.push(element);
                                                    });
                                                }
                                            
                                                res.json({success: true, msg:  'Transactions Found', data:allTransactionsArray});
                                             }
                                            });
                                     
                                     }
                                    });
                             
                             }
                            });
                     }
                    });
                   
                }
            });

            apiRoutes.post('/updateAgentProfile', function(req, res){
           
 
        
                var userId=req.body.userId;
                var fullName = req.body.fullName;
                var address = req.body.address;
                var dob = req.body.dob;

                 var companyName = req.body.companyName;
                 var EmailId = req.body.EmailId;
               
               var panCard = req.body.panCard;
               var aadharCard=req.body.aadharCard;
               var gstCertificate=req.body.gstCertificate;
            console.log(fullName);
            console.log(address);
            console.log(dob);

            console.log(companyName);
            console.log(EmailId);
            console.log(panCard);
            console.log(aadharCard);
            console.log(gstCertificate);
                if(userId!=undefined){
                    User.findOne({ '_id': userId }, function(err, user) {
                     
                        if(!user){
                            res.json({success: false, msg: 'No user with this Idexist'});
                        }
                              
                         else 
                         
                         {
                            User.findOneAndUpdate({'_id':userId},{$set : {
                                fullName:fullName,
                                add:address,
                                dob:dob,
                                companyName:companyName,
                                emailId:EmailId,
                                aadhar:aadharCard,
                                pancard:panCard,
                                gstCertificate:gstCertificate,
                                isVerified:true,
                                emailIdRegistered:true

                            }},function(err, updatedData) {
                     
                                if(!updatedData){
                                    res.json({success: false, msg: 'No Update exist'});
                                }
                                      
                                 else 
                                 
                                 {
                                     User.find({'_id':userId},function(err,userDetails){
                                         if(err){
                                            res.json({success: false, msg: 'No Update exist'});
                                         }else{
                                             console.log(userDetails);
                                            res.json({success: true, msg:  'Update Found', user:userDetails});
                                         }
                                     })
                                   
                      
                                 }
                                });
                         }
                        });
                       
                    }
                });


            


        //Loan Repayment Biller

        apiRoutes.post('/loanBiller', function(req, res){
           
 
        
            var contactNum=req.body.contactNum;
           
        
            if(contactNum!=undefined){
                User.findOne({ 'contactNum': contactNum }, function(err, user) {
                 
                    if(!user){
                        res.json({success: false, msg: 'No user with this Contact Number exist'});
                    }
                          
                     else 
                     
                     {
                        LoanRepayment.find(function(err, billers) {
                 
                            if(!billers){
                                res.json({success: false, msg: 'No Biller exist'});
                            }
                                  
                             else 
                             
                             {
                                res.json({success: true, msg:  'Biller Found', res:billers});
                             }
                            });
                     }
                    });
                   
                }
            });


            //Life Insurance Biller

            apiRoutes.post('/lifeBiller', function(req, res){
           
 
        
                var contactNum=req.body.contactNum;
               
            
                if(contactNum!=undefined){
                    User.findOne({ 'contactNum': contactNum }, function(err, user) {
                     
                        if(!user){
                            res.json({success: false, msg: 'No user with this Contact Number exist'});
                        }
                              
                         else 
                         
                         {
                            LifeInsurance.find(function(err, billers) {
                     
                                if(!billers){
                                    res.json({success: false, msg: 'No Biller exist'});
                                }
                                      
                                 else 
                                 
                                 {
                                    res.json({success: true, msg:  'Biller Found', res:billers});
                                 }
                                });
                         }
                        });
                       
                    }
                });


                 //Health Insurance Biller

            apiRoutes.post('/healthBiller', function(req, res){
           
 
        
                var contactNum=req.body.contactNum;
               
            
                if(contactNum!=undefined){
                    User.findOne({ 'contactNum': contactNum }, function(err, user) {
                     
                        if(!user){
                            res.json({success: false, msg: 'No user with this Contact Number exist'});
                        }
                              
                         else 
                         
                         {
                            HealthInsurance.find(function(err, billers) {
                     
                                if(!billers){
                                    res.json({success: false, msg: 'No Biller exist'});
                                }
                                      
                                 else 
                                 
                                 {
                                    res.json({success: true, msg:  'Biller Found', res:billers});
                                 }
                                });
                         }
                        });
                       
                    }
                });


                 //Municipal Taxes Biller

            apiRoutes.post('/municipalBiller', function(req, res){
           
 
        
                var contactNum=req.body.contactNum;
               
            
                if(contactNum!=undefined){
                    User.findOne({ 'contactNum': contactNum }, function(err, user) {
                     
                        if(!user){
                            res.json({success: false, msg: 'No user with this Contact Number exist'});
                        }
                              
                         else 
                         
                         {
                            MunicipalTaxes.find(function(err, billers) {
                     
                                if(!billers){
                                    res.json({success: false, msg: 'No Biller exist'});
                                }
                                      
                                 else 
                                 
                                 {
                                    res.json({success: true, msg:  'Biller Found', res:billers});
                                 }
                                });
                         }
                        });
                       
                    }
                });

                apiRoutes.post('/updateElectricity', function(req, res){
           
 console.log(JSON.stringify(req.body));
        console.log("COMING HERE");
        console.log(req.body);
                    var userId=req.body.userId;
                   console.log(userId);
                var billerId = req.body.billerId;
                var active = req.body.isActive;
                console.log(active);
                console.log(billerId);
                    if(userId!=undefined){
                        User.findOne({ '_id': userId }, function(err, user) {
                            console.log("COMING HERE2");
                            if(!user){
                                res.json({success: false, msg: 'No user with this Contact Number exist'});
                            }
                                  
                             else 
                             
                             {
                                console.log("COMING HERE3");
                                Electricity.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                         
                                    if(err){
                                        res.json({success: false, msg: 'No Biller exist'});
                                    }
                                          
                                     else 
                                     
                                     {
                                        res.json({success: true, msg:  'Biller Updated', res:biller});
                                     }
                                    });
                             }
                            });
                           
                        }
                    });

                    apiRoutes.post('/updateBroadband', function(req, res){
           
                        console.log(JSON.stringify(req.body));
                               console.log("COMING HERE");
                               console.log(req.body);
                                           var userId=req.body.userId;
                                          console.log(userId);
                                       var billerId = req.body.billerId;
                                       var active = req.body.isActive;
                                       console.log(active);
                                       console.log(billerId);
                                           if(userId!=undefined){
                                               User.findOne({ '_id': userId }, function(err, user) {
                                                   console.log("COMING HERE2");
                                                   if(!user){
                                                       res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                   }
                                                         
                                                    else 
                                                    
                                                    {
                                                       console.log("COMING HERE3");
                                                       Broadband.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                
                                                           if(err){
                                                               res.json({success: false, msg: 'No Biller exist'});
                                                           }
                                                                 
                                                            else 
                                                            
                                                            {
                                                               res.json({success: true, msg:  'Biller Updated', res:biller});
                                                            }
                                                           });
                                                    }
                                                   });
                                                  
                                               }
                                           });

                                           apiRoutes.post('/updateEducation', function(req, res){
           
                                            console.log(JSON.stringify(req.body));
                                                   console.log("COMING HERE");
                                                   console.log(req.body);
                                                               var userId=req.body.userId;
                                                              console.log(userId);
                                                           var billerId = req.body.billerId;
                                                           var active = req.body.isActive;
                                                           console.log(active);
                                                           console.log(billerId);
                                                               if(userId!=undefined){
                                                                   User.findOne({ '_id': userId }, function(err, user) {
                                                                       console.log("COMING HERE2");
                                                                       if(!user){
                                                                           res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                       }
                                                                             
                                                                        else 
                                                                        
                                                                        {
                                                                           console.log("COMING HERE3");
                                                                           EducationFees.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                    
                                                                               if(err){
                                                                                   res.json({success: false, msg: 'No Biller exist'});
                                                                               }
                                                                                     
                                                                                else 
                                                                                
                                                                                {
                                                                                   res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                }
                                                                               });
                                                                        }
                                                                       });
                                                                      
                                                                   }
                                                               });


                                                               apiRoutes.post('/updatefastag', function(req, res){
           
                                                                console.log(JSON.stringify(req.body));
                                                                       console.log("COMING HERE");
                                                                       console.log(req.body);
                                                                                   var userId=req.body.userId;
                                                                                  console.log(userId);
                                                                               var billerId = req.body.billerId;
                                                                               var active = req.body.isActive;
                                                                               console.log(active);
                                                                               console.log(billerId);
                                                                                   if(userId!=undefined){
                                                                                       User.findOne({ '_id': userId }, function(err, user) {
                                                                                           console.log("COMING HERE2");
                                                                                           if(!user){
                                                                                               res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                           }
                                                                                                 
                                                                                            else 
                                                                                            
                                                                                            {
                                                                                               console.log("COMING HERE3");
                                                                                               FastTag.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                        
                                                                                                   if(err){
                                                                                                       res.json({success: false, msg: 'No Biller exist'});
                                                                                                   }
                                                                                                         
                                                                                                    else 
                                                                                                    
                                                                                                    {
                                                                                                       res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                    }
                                                                                                   });
                                                                                            }
                                                                                           });
                                                                                          
                                                                                       }
                                                                                   });

                                                                                   apiRoutes.post('/updateGas', function(req, res){
           
                                                                                    console.log(JSON.stringify(req.body));
                                                                                           console.log("COMING HERE");
                                                                                           console.log(req.body);
                                                                                                       var userId=req.body.userId;
                                                                                                      console.log(userId);
                                                                                                   var billerId = req.body.billerId;
                                                                                                   var active = req.body.isActive;
                                                                                                   console.log(active);
                                                                                                   console.log(billerId);
                                                                                                       if(userId!=undefined){
                                                                                                           User.findOne({ '_id': userId }, function(err, user) {
                                                                                                               console.log("COMING HERE2");
                                                                                                               if(!user){
                                                                                                                   res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                                               }
                                                                                                                     
                                                                                                                else 
                                                                                                                
                                                                                                                {
                                                                                                                   console.log("COMING HERE3");
                                                                                                                   Gas.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                                            
                                                                                                                       if(err){
                                                                                                                           res.json({success: false, msg: 'No Biller exist'});
                                                                                                                       }
                                                                                                                             
                                                                                                                        else 
                                                                                                                        
                                                                                                                        {
                                                                                                                           res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                                        }
                                                                                                                       });
                                                                                                                }
                                                                                                               });
                                                                                                              
                                                                                                           }
                                                                                                       });

                                                                                                       apiRoutes.post('/updateWater', function(req, res){
           
                                                                                                        console.log(JSON.stringify(req.body));
                                                                                                               console.log("COMING HERE");
                                                                                                               console.log(req.body);
                                                                                                                           var userId=req.body.userId;
                                                                                                                          console.log(userId);
                                                                                                                       var billerId = req.body.billerId;
                                                                                                                       var active = req.body.isActive;
                                                                                                                       console.log(active);
                                                                                                                       console.log(billerId);
                                                                                                                           if(userId!=undefined){
                                                                                                                               User.findOne({ '_id': userId }, function(err, user) {
                                                                                                                                   console.log("COMING HERE2");
                                                                                                                                   if(!user){
                                                                                                                                       res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                                                                   }
                                                                                                                                         
                                                                                                                                    else 
                                                                                                                                    
                                                                                                                                    {
                                                                                                                                       console.log("COMING HERE3");
                                                                                                                                       Water.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                                                                
                                                                                                                                           if(err){
                                                                                                                                               res.json({success: false, msg: 'No Biller exist'});
                                                                                                                                           }
                                                                                                                                                 
                                                                                                                                            else 
                                                                                                                                            
                                                                                                                                            {
                                                                                                                                               res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                                                            }
                                                                                                                                           });
                                                                                                                                    }
                                                                                                                                   });
                                                                                                                                  
                                                                                                                               }
                                                                                                                           });

                                                                                                       apiRoutes.post('/updateHealth', function(req, res){
           
                                                                                                        console.log(JSON.stringify(req.body));
                                                                                                               console.log("COMING HERE");
                                                                                                               console.log(req.body);
                                                                                                                           var userId=req.body.userId;
                                                                                                                          console.log(userId);
                                                                                                                       var billerId = req.body.billerId;
                                                                                                                       var active = req.body.isActive;
                                                                                                                       console.log(active);
                                                                                                                       console.log(billerId);
                                                                                                                           if(userId!=undefined){
                                                                                                                               User.findOne({ '_id': userId }, function(err, user) {
                                                                                                                                   console.log("COMING HERE2");
                                                                                                                                   if(!user){
                                                                                                                                       res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                                                                   }
                                                                                                                                         
                                                                                                                                    else 
                                                                                                                                    
                                                                                                                                    {
                                                                                                                                       console.log("COMING HERE3");
                                                                                                                                       HealthInsurance.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                                                                
                                                                                                                                           if(err){
                                                                                                                                               res.json({success: false, msg: 'No Biller exist'});
                                                                                                                                           }
                                                                                                                                                 
                                                                                                                                            else 
                                                                                                                                            
                                                                                                                                            {
                                                                                                                                               res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                                                            }
                                                                                                                                           });
                                                                                                                                    }
                                                                                                                                   });
                                                                                                                                  
                                                                                                                               }
                                                                                                                           });

                                                                                                                           apiRoutes.post('/updateHousing', function(req, res){
           
                                                                                                                            console.log(JSON.stringify(req.body));
                                                                                                                                   console.log("COMING HERE");
                                                                                                                                   console.log(req.body);
                                                                                                                                               var userId=req.body.userId;
                                                                                                                                              console.log(userId);
                                                                                                                                           var billerId = req.body.billerId;
                                                                                                                                           var active = req.body.isActive;
                                                                                                                                           console.log(active);
                                                                                                                                           console.log(billerId);
                                                                                                                                               if(userId!=undefined){
                                                                                                                                                   User.findOne({ '_id': userId }, function(err, user) {
                                                                                                                                                       console.log("COMING HERE2");
                                                                                                                                                       if(!user){
                                                                                                                                                           res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                                                                                       }
                                                                                                                                                             
                                                                                                                                                        else 
                                                                                                                                                        
                                                                                                                                                        {
                                                                                                                                                           console.log("COMING HERE3");
                                                                                                                                                           HousingSociety.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                                                                                    
                                                                                                                                                               if(err){
                                                                                                                                                                   res.json({success: false, msg: 'No Biller exist'});
                                                                                                                                                               }
                                                                                                                                                                     
                                                                                                                                                                else 
                                                                                                                                                                
                                                                                                                                                                {
                                                                                                                                                                   res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                                                                                }
                                                                                                                                                               });
                                                                                                                                                        }
                                                                                                                                                       });
                                                                                                                                                      
                                                                                                                                                   }
                                                                                                                                               });

                                                                                                                                               apiRoutes.post('/updatelandline', function(req, res){
           
                                                                                                                                                console.log(JSON.stringify(req.body));
                                                                                                                                                       console.log("COMING HERE");
                                                                                                                                                       console.log(req.body);
                                                                                                                                                                   var userId=req.body.userId;
                                                                                                                                                                  console.log(userId);
                                                                                                                                                               var billerId = req.body.billerId;
                                                                                                                                                               var active = req.body.isActive;
                                                                                                                                                               console.log(active);
                                                                                                                                                               console.log(billerId);
                                                                                                                                                                   if(userId!=undefined){
                                                                                                                                                                       User.findOne({ '_id': userId }, function(err, user) {
                                                                                                                                                                           console.log("COMING HERE2");
                                                                                                                                                                           if(!user){
                                                                                                                                                                               res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                                                                                                           }
                                                                                                                                                                                 
                                                                                                                                                                            else 
                                                                                                                                                                            
                                                                                                                                                                            {
                                                                                                                                                                               console.log("COMING HERE3");
                                                                                                                                                                               LandlinePostpaid.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                                                                                                        
                                                                                                                                                                                   if(err){
                                                                                                                                                                                       res.json({success: false, msg: 'No Biller exist'});
                                                                                                                                                                                   }
                                                                                                                                                                                         
                                                                                                                                                                                    else 
                                                                                                                                                                                    
                                                                                                                                                                                    {
                                                                                                                                                                                       res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                                                                                                    }
                                                                                                                                                                                   });
                                                                                                                                                                            }
                                                                                                                                                                           });
                                                                                                                                                                          
                                                                                                                                                                       }
                                                                                                                                                                   });

                                                                                                                                                                   apiRoutes.post('/updatelife', function(req, res){
           
                                                                                                                                                                    console.log(JSON.stringify(req.body));
                                                                                                                                                                           console.log("COMING HERE");
                                                                                                                                                                           console.log(req.body);
                                                                                                                                                                                       var userId=req.body.userId;
                                                                                                                                                                                      console.log(userId);
                                                                                                                                                                                   var billerId = req.body.billerId;
                                                                                                                                                                                   var active = req.body.isActive;
                                                                                                                                                                                   console.log(active);
                                                                                                                                                                                   console.log(billerId);
                                                                                                                                                                                       if(userId!=undefined){
                                                                                                                                                                                           User.findOne({ '_id': userId }, function(err, user) {
                                                                                                                                                                                               console.log("COMING HERE2");
                                                                                                                                                                                               if(!user){
                                                                                                                                                                                                   res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                                                                                                                               }
                                                                                                                                                                                                     
                                                                                                                                                                                                else 
                                                                                                                                                                                                
                                                                                                                                                                                                {
                                                                                                                                                                                                   console.log("COMING HERE3");
                                                                                                                                                                                                   LifeInsurance.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                                                                                                                            
                                                                                                                                                                                                       if(err){
                                                                                                                                                                                                           res.json({success: false, msg: 'No Biller exist'});
                                                                                                                                                                                                       }
                                                                                                                                                                                                             
                                                                                                                                                                                                        else 
                                                                                                                                                                                                        
                                                                                                                                                                                                        {
                                                                                                                                                                                                           res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                                                                                                                        }
                                                                                                                                                                                                       });
                                                                                                                                                                                                }
                                                                                                                                                                                               });
                                                                                                                                                                                              
                                                                                                                                                                                           }
                                                                                                                                                                                       });


                                                                                                                                                                                       apiRoutes.post('/updateloan', function(req, res){
           
                                                                                                                                                                                        console.log(JSON.stringify(req.body));
                                                                                                                                                                                               console.log("COMING HERE");
                                                                                                                                                                                               console.log(req.body);
                                                                                                                                                                                                           var userId=req.body.userId;
                                                                                                                                                                                                          console.log(userId);
                                                                                                                                                                                                       var billerId = req.body.billerId;
                                                                                                                                                                                                       var active = req.body.isActive;
                                                                                                                                                                                                       console.log(active);
                                                                                                                                                                                                       console.log(billerId);
                                                                                                                                                                                                           if(userId!=undefined){
                                                                                                                                                                                                               User.findOne({ '_id': userId }, function(err, user) {
                                                                                                                                                                                                                   console.log("COMING HERE2");
                                                                                                                                                                                                                   if(!user){
                                                                                                                                                                                                                       res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                                                                                                                                                   }
                                                                                                                                                                                                                         
                                                                                                                                                                                                                    else 
                                                                                                                                                                                                                    
                                                                                                                                                                                                                    {
                                                                                                                                                                                                                       console.log("COMING HERE3");
                                                                                                                                                                                                                       LoanRepayment.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                                                                                                                                                
                                                                                                                                                                                                                           if(err){
                                                                                                                                                                                                                               res.json({success: false, msg: 'No Biller exist'});
                                                                                                                                                                                                                           }
                                                                                                                                                                                                                                 
                                                                                                                                                                                                                            else 
                                                                                                                                                                                                                            
                                                                                                                                                                                                                            {
                                                                                                                                                                                                                               res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                           });
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                   });
                                                                                                                                                                                                                  
                                                                                                                                                                                                               }
                                                                                                                                                                                                           });


                                                                                                                                                                                                           apiRoutes.post('/updateMunicipal', function(req, res){
           
                                                                                                                                                                                                            console.log(JSON.stringify(req.body));
                                                                                                                                                                                                                   console.log("COMING HERE");
                                                                                                                                                                                                                   console.log(req.body);
                                                                                                                                                                                                                               var userId=req.body.userId;
                                                                                                                                                                                                                              console.log(userId);
                                                                                                                                                                                                                           var billerId = req.body.billerId;
                                                                                                                                                                                                                           var active = req.body.isActive;
                                                                                                                                                                                                                           console.log(active);
                                                                                                                                                                                                                           console.log(billerId);
                                                                                                                                                                                                                               if(userId!=undefined){
                                                                                                                                                                                                                                   User.findOne({ '_id': userId }, function(err, user) {
                                                                                                                                                                                                                                       console.log("COMING HERE2");
                                                                                                                                                                                                                                       if(!user){
                                                                                                                                                                                                                                           res.json({success: false, msg: 'No user with this Contact Number exist'});
                                                                                                                                                                                                                                       }
                                                                                                                                                                                                                                             
                                                                                                                                                                                                                                        else 
                                                                                                                                                                                                                                        
                                                                                                                                                                                                                                        {
                                                                                                                                                                                                                                           console.log("COMING HERE3");
                                                                                                                                                                                                                                           MunicipalTaxes.findOneAndUpdate({'billerId': billerId}, { $set: {'isActive':active}}, function(err,biller) {
                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                               if(err){
                                                                                                                                                                                                                                                   res.json({success: false, msg: 'No Biller exist'});
                                                                                                                                                                                                                                               }
                                                                                                                                                                                                                                                     
                                                                                                                                                                                                                                                else 
                                                                                                                                                                                                                                                
                                                                                                                                                                                                                                                {
                                                                                                                                                                                                                                                   res.json({success: true, msg:  'Biller Updated', res:biller});
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                               });
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                       });
                                                                                                                                                                                                                                      
                                                                                                                                                                                                                                   }
                                                                                                                                                                                                                               });
          
  
     apiRoutes.post('/getBillerInfo', function(req, res){                                                                                                                                                                                                                          
     var accessCode= 'AVTM87KZ80ZB95QHDY';
var reqId = randomstring.generate(35);
    var workingkey='C516828299E4BE70B6B0612ACFE795FD';
    // console.log(originalText); // 'my message'
var billerId = req.body.billerId;
var req='<?xml version="1.0" encoding="UTF-8"?> <billerInfoRequest><billerId>'+billerId+'</billerId> </billerInfoRequest>';
   var encdata= encrypt(req,workingkey);
    request.post('https://api.billavenue.com/billpay/extMdmCntrl/mdmRequest/xml', {
      form: {
    accessCode:accessCode,
    requestId:reqId,
    encRequest:encdata,
    ver:'1.0',
    instituteId:'IA83'
      }
    }, function (err, httpResponse, body) { 
        
       console.log(body);

        console.log("Body ends");
      
        if(err){
            console.log("above is error");
            console.log(err);
         
        }
       
        try {
            console.log("goes here");
          var decrypted=decrypt(body,workingkey);
        
           result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
           console.log(result2.billerInfoResponse)
        if(result2.billerInfoResponse.responseCode._text=="000"){
            
            res.json({success: true,msg:'fetch Authentication Success',data:result2.billerInfoResponse.biller.billerInputParams});
        }else{
            res.json({success: false,msg:'Unable to fetch Details',data:result2.billerInfoResponse.errorInfo.error.errorMessage});

        }
    
                console.log("check ends")
              } catch (e) {
                  console.log("error"+e);
                var decrypted = "";
              }
         
         
     });
    });
   
//  below is saved data in db ******* DO NOT REMOVE **************************

//  var accessCode= 'AVTM87KZ80ZB95QHDY';
// var reqId = randomstring.generate(35);
//     var workingkey='C516828299E4BE70B6B0612ACFE795FD';
//     // console.log(originalText); // 'my message'

// var req='<?xml version="1.0" encoding="UTF-8"?><billerInfoRequest></billerInfoRequest>';
//    var encdata= encrypt(req,workingkey);
//     request.post('https://api.billavenue.com/billpay/extMdmCntrl/mdmRequest/xml', {
//       form: {
//     accessCode:accessCode,
//     requestId:reqId,
//     encRequest:encdata,
//     ver:'1.0',
//     instituteId:'IA83'
//       }
//     }, function (err, httpResponse, body) { 
        
//        console.log(body);

//         console.log("Body ends");
      
//         if(err){
//             console.log("above is error");
//             console.log(err);
         
//         }
       
//         try {
//             console.log("goes here");
//           var decrypted=decrypt(body,workingkey);
//          var result2=[];
//          var allGasVendor=[];
//          var allWaterVendor=[];
//          var allElectricityVendor=[];
//          var allBroadbandVendor=[];
//          var allFastagVendor=[];
//          var allLoanRepaymentVendor=[];
//          var allDataBillerArray=[];
//          var allHealthInsuranceArray=[];
//          var allLifeInsuranceArray=[];
//          var allMunicipalTaxesArray=[];
//          var allEducationFeesArray=[];
//          var allHousingSocietyArray=[];
//            result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
//            console.log(result2)
//                 console.log(typeof(result2));
//                 console.log("check result2 above");

//                  allDataBillerArray=result2.billerInfoResponse.biller;
//                  console.log(allDataBillerArray[50].billerInputParams.paramInfo.paramName);
//                  console.log(typeof(allDataBillerArray));
//                  console.log("check asap1 allDataBillerArray type above");
//                  console.log(typeof(allDataBillerArray));
//                  console.log("check asap1 allDataBillerArray above");
//                  console.log(allDataBillerArray.length);
//                  console.log("check asap2 allDataBillerArray length");
//                  console.log(allDataBillerArray[55]);
//                  console.log("check asap2 some vendor");
//                 for(let i=0;i<allDataBillerArray.length;i++){
//                       console.log(allDataBillerArray[i]);
//                 }
//             for(let i=0;i<allDataBillerArray.length;i++){
//                 if(allDataBillerArray[i].billerCategory._text=="Gas"){
                
//                     console.log("Match");
// // #changes start
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var gasBillerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var gasBillerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                   
                
//                     Gas.addGas(gasBillerData,function(err, GasBillerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(GasBillerData);
//                             // console.log("Added Data");
//                         }
//                     });
                
//                 }
//                 else if(allDataBillerArray[i].billerCategory._text=="Water"){
//                     // allWaterVendor=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");

// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var waterBillerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var waterBillerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     Water.addWater(waterBillerData,function(err, WaterBillerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             console.log(WaterBillerData);
//                             console.log("Added Data");
//                         }
//                     });
//                 }
//                 else if(allDataBillerArray[i].billerCategory._text=="Electricity"){
//                     // allElectricityVendor=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var electricityBillerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var electricityBillerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     Electricity.addElectricity(electricityBillerData,function(err, ElectricityBillerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(ElectricityBillerData);
//                             // console.log("Added Data");
//                         }
//                     });
//                 }
//                 else if(allDataBillerArray[i].billerCategory._text=="Broadband Postpaid"){
//                     // allBroadbandVendor=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var broadbandBillerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var broadbandBillerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     Broadband.addbroadband(broadbandBillerData,function(err, BroadbandBillerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(BroadbandBillerData);
//                             // console.log("Added Data");
//                         }
//                     });
//                 }
//                 else if(allDataBillerArray[i].billerCategory._text=="Fastag"){
//                     // allFastagVendor=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var fastTagBillerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var fastTagBillerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     FastTag.addFastTag(fastTagBillerData,function(err, FastTagBillerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(FastTagBillerData);
//                             // console.log("Added Data");
//                         }
//                     });
                    
//                 }else if(allDataBillerArray[i].billerCategory._text=='Loan Repayment'){
//                 allLoanRepaymentVendor=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var billerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var billerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     LoanRepayment.addLoanRepayment(billerData,function(err, billerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(billerData);
//                             // console.log("Added Data");
//                         }
//                     });
//             }else if(allDataBillerArray[i].billerCategory._text=='Health Insurance'){
//                  allHealthInsuranceArray=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//     console.log("goes in input Param Array check here now");
  
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// console.log(inputParam);
// var billerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var billerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName, 
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     HealthInsurance.addhealthInsurance(billerData,function(err, billerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(billerData);
//                             // console.log("Added Data");
//                         }
//                     });
//             }else if(allDataBillerArray[i].billerCategory._text=='Insurance'||allDataBillerArray[i].billerCategory._text=='Life Insurance'){
//                 allLifeInsuranceArray=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var billerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var billerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     LifeInsurance.addlifeInsurance(billerData,function(err, billerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(billerData);
//                             // console.log("Added Data");
//                         }
//                     });
//             }else if(allDataBillerArray[i].billerCategory._text=='Education Fees'){
//                 allEducationFeesArray=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var billerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var billerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     EducationFees.addEducationFees(billerData,function(err, billerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(billerData);
//                             // console.log("Added Data");
//                         }
//                     });
//             }else if(allDataBillerArray[i].billerCategory._text=='Municipal Taxes'){
//                 allMunicipalTaxesArray=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var billerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var billerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
//                     MunicipalTaxes.addMunicipalTaxes(billerData,function(err, billerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(billerData);
//                             // console.log("Added Data");
//                         }
//                     });
//             }else if(allDataBillerArray[i].billerCategory._text=='Landline Postpaid'){
//                 allLandlinePostpaidArray=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var billerData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var billerData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     LandlinePostpaid.addlandlinePostpaid(billerData,function(err, billerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(billerData);
//                             // console.log("Added Data");
//                         }
//                     });
//             }else if(allDataBillerArray[i].billerCategory._text=='Housing Society'){
//                 allHousingSocietyArray=allDataBillerArray[i];
//                     // console.log(allDataBillerArray[i]);
//                     console.log("Match");
// var inputParam=[]
//                 if(allDataBillerArray[i].billerInputParams.length!=0){
//                     var inputParam = allDataBillerArray[i].billerInputParams.paramInfo;
// var housingData = {
        
//                             billerId:allDataBillerArray[i].billerId._text,        
//                             billerName:allDataBillerArray[i].billerName._text, 
//                             inputParam:inputParam,   
//                             billerCategory: allDataBillerArray[i].billerCategory._text,
//                             billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                             billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                             billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                             billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                             billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                            
//                         }
//                 }else{
   
//     var housingData = {
        
//                                 billerId:allDataBillerArray[i].billerId._text,        
//                                 billerName:allDataBillerArray[i].billerName._text, 
//                                 inputParam:allDataBillerArray[i].billerInputParams.paramInfo.paramName,   
//                                 billerCategory: allDataBillerArray[i].billerCategory._text,
//                                 billerAdhoc:allDataBillerArray[i].billerAdhoc._text,
//                                 billerCoverage:allDataBillerArray[i].billerCoverage._text,
//                                 billerFetchRequiremet:allDataBillerArray[i].billerFetchRequiremet._text,
//                                 billerPaymentExactness:allDataBillerArray[i].billerPaymentExactness._text,
//                                 billerSupportBillValidation: allDataBillerArray[i].billerSupportBillValidation._text
                                
//                             }
//                 }
                
//                     HousingSociety.addHousingSociety(housingData,function(err, billerData){
//                         if(err){
//                             console.log(err);
//                             console.log("Failed To add Data");
//                         //	throw err;
//                         }
//                         else{
//                             // console.log(billerData);
//                             // console.log("Added Data");
//                         }
//                     });
//             }
//             }
//             console.log("ends")
//           } catch (e) {
//               console.log("error"+e);
//             var decrypted = "";
//           }
     
//      });

    //*********************************************************/
//Above is commented function after storing to db except water billers
//*********************************************************************/

//fetch Electricity billers here


apiRoutes.post('/electricityBiller', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    console.log(contactNum);
  
    if(contactNum!=undefined){
        User.findOne({ 'contactNum': contactNum }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                Electricity.find(function(err, electricityBillerInfo) {
                        
            
                    if (err){
                        console.log("error");
                        console.log(err);
                        res.json({success: false, msg: 'Unable To Fetch Electricity Biller',res:err});
                    }else{
                       
                        
                        var arrayElectricityBiller=[];
                        for(var i=0;i<electricityBillerInfo.length;i++)
                        {
                            arrayElectricityBiller.push(electricityBillerInfo[i]._doc);
                        }
                        res.json({success: true, msg: 'Electricity Biller Request Sent Successfully',res:arrayElectricityBiller});
                        
                    }
                });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//fetch Broadband billers here


apiRoutes.post('/broadbandBiller', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    console.log(contactNum);
  
    if(contactNum!=undefined){
        User.findOne({ 'contactNum': contactNum }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                Broadband.find(function(err, broadbandBillerInfo) {
                        
            
                    if (err){
                        console.log("error");
                        console.log(err);
                        res.json({success: false, msg: 'Unable To Fetch Broadband Biller',res:err});
                    }else{
                       
                        
                        var arrayBroadbandBiller=[];
                        for(var i=0;i<broadbandBillerInfo.length;i++)
                        {
                            arrayBroadbandBiller.push(broadbandBillerInfo[i]._doc);
                        }
                        res.json({success: true, msg: 'Broadband Biller Request Sent Successfully',res:arrayBroadbandBiller});
                        
                    }
                });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//fetch Gas billers here


apiRoutes.post('/gasBiller', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    console.log(contactNum);
  
    if(contactNum!=undefined){
        User.findOne({ 'contactNum': contactNum }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                Gas.find(function(err, gasBillerInfo) {
                        
            
                    if (err){
                        console.log("error");
                        console.log(err);
                        res.json({success: false, msg: 'Unable To Fetch Gas Biller',res:err});
                    }else{
                       
                        
                        var arrayGasBiller=[];
                        for(var i=0;i<gasBillerInfo.length;i++)
                        {
                            arrayGasBiller.push(gasBillerInfo[i]._doc);
                        }
                        res.json({success: true, msg: 'Gas Biller Request Sent Successfully',res:arrayGasBiller});
                        
                    }
                });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//fetch FastTag billers here


apiRoutes.post('/fastTagBiller', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    console.log(contactNum);
  
    if(contactNum!=undefined){
        User.findOne({ 'contactNum': contactNum }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                FastTag.find(function(err, fastTagBillerInfo) {
                        
            
                    if (err){
                        console.log("error");
                        console.log(err);
                        res.json({success: false, msg: 'Unable To Fetch FastTag Biller',res:err});
                    }else{
                       
                        
                        var arrayFastTagBiller=[];
                        for(var i=0;i<fastTagBillerInfo.length;i++)
                        {
                            arrayFastTagBiller.push(fastTagBillerInfo[i]._doc);
                        }
                        res.json({success: true, msg: 'FastTag Biller Request Sent Successfully',res:arrayFastTagBiller});
                       
                    }
                });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//fetch Water billers here


apiRoutes.post('/waterBiller', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    console.log(contactNum);
  
    if(contactNum!=undefined){
        User.findOne({ 'contactNum': contactNum }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                Water.find(function(err, waterBillerInfo) {
                        
            
                    if (err){
                        console.log("error");
                        console.log(err);
                        res.json({success: false, msg: 'Unable To Fetch Water Biller',res:err});
                    }else{
                       
                        
                        var arrayWaterBiller=[];
                        for(var i=0;i<waterBillerInfo.length;i++)
                        {
                            arrayWaterBiller.push(waterBillerInfo[i]._doc);
                        }
                        res.json({success: true, msg: 'Water Biller Request Sent Successfully',res:arrayWaterBiller});
                        
                    }
                });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});
// MOBILE RECHARGE API


apiRoutes.post('/mobileRecharge', function(req, res){

   
   var paythroughWallet = req.body.paythroughWallet;
    var SecurityKey=req.body.SecurityKey;
    var md5Key=req.body.ApiChkSum;
    var date = moment().format("DD MMM YYYY HH:mm");
    console.log(date);
    console.log("Check Above Date");
    var CorporateId = parseInt(req.body.CorporateId);
    console.log(CorporateId);
    console.log(typeof(CorporateId));
    var AuthKey=req.body.AuthKey;
    var Mobile=req.body.Mobile;
    var Amount = req.body.Amount;
    var ServiceType = req.body.ServiceType;
    var IsPostpaid = req.body.IsPostpaid;
    var userId=req.body.userId;
    var SystemReference = req.body.SystemReference;
   
    var lowerCaseMd5 =  md5(CorporateId+AuthKey+Mobile+Amount+SystemReference+md5Key)
  
    console.log(md5Key);
   
    console.log(lowerCaseMd5 +"this is final");
    if(paythroughWallet===false){
        axios
.post('https://spi.justrechargeit.com/JRICorporateRecharge.svc/Instantrecharge', {
 
 
    "CorporateId":CorporateId,
    "AuthKey":AuthKey,
    "Mobile":Mobile,
    "APIChkSum":lowerCaseMd5,
    "SecurityKey":SecurityKey,
    "ServiceType":ServiceType,
    "IsPostpaid":IsPostpaid,
    
    "SystemReference":SystemReference,
    "Amount":Amount,
 
    
},
{
  headers: {
   'Content-Type':'application/json',
     
  }
})
.then(dataRes => {
//   console.log(`statusCode: ${res.statusCode}`)
console.log(dataRes.data);
console.log("THIS is RESOPONSE");

    var rechargeData={
        rechargeType:ServiceType,
        contactNum:Mobile,
        paymentMethod:'Razorpay',
        amount:Amount,
        transactionRef:SystemReference,
        userId:userId,
        date:date
        }
      Recharge.addRecharge(rechargeData,function(err, recharge){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Recharge Detail'});
        //	throw err;
        }
        else{
            res.json({success: true,msg:'Recharge Request Processed Successfully',res:dataRes.data,recharge:recharge});
        }
    });

 
  
  

})
.catch(error => {
  console.error(error+"THIS is ERROR");
  res.json({success: false,msg:'Recharge Request cannot Send Successfully',res:datares.data});

})
    }else{
        if(userId!==undefined){

            User.findOne({ '_id': userId }, function(err, user) {
             
                if(!user){
                    res.json({success: false, msg: 'No user with this user id exist'});
                }
                      
                 else 
                 
                 {
                     var prevBalance=user.wallet;
                     var newBalance=prevBalance-Amount;
                          User.update({'_id': userId}, { $set: {wallet:newBalance}}, function (err, updateuser) {
                           //handle it
                                    if(err){
                                    res.json({success: false,msg:'Failed to Update'});
                                    //throw err;
                                    console.log(err);
                                }
                                    else{
                                        axios
                                        .post('https://spi.justrechargeit.com/JRICorporateRecharge.svc/Instantrecharge', {
                                         
                                         
                                            "CorporateId":CorporateId,
                                            "AuthKey":AuthKey,
                                            "Mobile":Mobile,
                                            "APIChkSum":lowerCaseMd5,
                                            "SecurityKey":SecurityKey,
                                            "ServiceType":ServiceType,
                                            "IsPostpaid":IsPostpaid,
                                            
                                            "SystemReference":SystemReference,
                                            "Amount":Amount,
                                         
                                            
                                        },
                                        {
                                          headers: {
                                           'Content-Type':'application/json',
                                             
                                          }
                                        })
                                        .then(dataRes => {
                                        //   console.log(`statusCode: ${res.statusCode}`)
                                        console.log(dataRes.data);
                                          console.log("THIS is RESOPONSE");
                                          var rechargeData={
                                            rechargeType:ServiceType,
                                            contactNum:Mobile,
                                            paymentMethod:'Wallet',
                                            amount:Amount,
                                            transactionRef:SystemReference,
                                            userId:userId,
                                            date:date
                                            }
                                          Recharge.addRecharge(rechargeData,function(err, recharge){
                                            if(err){
                                                console.log(err);
                                                res.json({success: false, msg: 'Failed to add Recharge Detail'});
                                            //	throw err;
                                            }
                                            else{
                                                res.json({success: true,msg:'Recharge Request Processed Successfully',res:dataRes.data,recharge:recharge,user:updateuser});
                                            }
                                        });
                                       
                                        
                                        })
                                        .catch(error => {
                                          console.error(error+"THIS is ERROR");
                                          res.json({success: false,msg:'Recharge Request cannot Send Successfully',res:error});
                                        
                                        })
                                    }
                            });
                     
                }
            });
       
        }
        else{
            res.json({success: false,msg:'Unable to fetch UserId failed'});
        }
    }



});


//Update user Image

apiRoutes.post('/updateUserImage', function(req, res){
	  
   
    var userId=req.body.userId;
   
    var userImg=req.body.userImg;
    console.log(userImg);
    console.log(userId);
    // var token = req.body.Authorization;
    // console.log(token);
   // token= "JWT "+token;
    // var isauth= route.memberinfo(token,userId);
    if(userId!==undefined){
        User.findOne({ '_id': userId }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                      User.findOneAndUpdate({'_id': userId}, { $set: {user_img:userImg}}, function (err, updateuser) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    console.log("should be updated")
                                    res.json({success: true,msg:'User Successfully Updated',user:updateuser});
                                }
                        });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Update user Password

apiRoutes.post('/updatePassword', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    console.log(contactNum);
    var password=req.body.password;
    console.log(password);
    // var token = req.body.Authorization;
    // console.log(token);
   // token= "JWT "+token;
    // var isauth= route.memberinfo(token,userId);
    if(userId!==undefined){
        User.findOne({ 'contactNum': contactNum }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                      User.update({'contactNum': contactNum}, { $set: {password:password}}, function (err, user) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    res.json({success: true,msg:'User Successfully Updated',user:user});
                                }
                        });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Get User Details

apiRoutes.post('/getUserDetails', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    var userId=req.body.userId;
   console.log(contactNum);
   console.log(userId);
    
    if(contactNum!==undefined){
         User.findOne({ '_id':userId }, function(err, user) {
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }else{
                Transaction.find({'userId':userId}, function(err, transactions){
                    if(err){
                        console.log(err);
                        res.json({success: false, msg: 'Failed to add Transaction'});
                    //	throw err;
                    }
                    else{
                        res.json({success: true,msg:'Response Fetched Successfully',user:user,transactions:transactions});
                    }
                });
             }
             
    });
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Get User transaction Details

apiRoutes.post('/getUserOrders', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    var userId = req.body.userId;
    // var isauth= route.memberinfo(token,userId);
    
    if(contactNum!=undefined){
        User.findOne({ '_id':userId }, function(err, user) {
             
            if(err){
                
               res.json("failed to fetch data");
                
            }else{
        Order.find({ 'userId':userId }, function(err, orders) {
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }else{
                res.json({success:true,msg:'User orders fetch successfully',orders:orders});
             }
          
             
       
            
            });
        }
    });
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});



//Update UserName

apiRoutes.post('/updateUserName', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    var userId=req.body.userId;
    console.log(contactNum);
    var fullName=req.body.fullName;

    if(contactNum!==undefined){
        User.findOne({ '_id': userId }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                      User.update({'_id': userId}, { $set: {fullName:fullName}}, function (err, user) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update User Details'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    
                                            res.json({success: true,msg:'User Updated Successfully',user:user});
                                     
                                }
                        });
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//Update email

apiRoutes.post('/updateEmail', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    var userId=req.body.userId;
    console.log(contactNum);
    var email=req.body.email;
    
    if(contactNum!==undefined){
        User.findOne({ '_id': userId }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                      User.update({'_id': userId}, { $set: {'emailId':email,
                      "emailIdRegistered":true}}, function (err, user) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update User Details'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    
                                            res.json({success: true,msg:'User Updated Successfully',user:user});
                                     
                                }
                        });
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//Update Aadhar

apiRoutes.post('/updateAadhar', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    var userId=req.body.userId;
    console.log(contactNum);
    var aadhar=req.body.aadhar;
    
    if(contactNum!==undefined){
        User.findOne({ '_id': userId }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                      User.update({'_id': userId}, { $set: {"aadhar":aadhar,
                        "aadharRegistered":true}}, function (err, user) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update User Details'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    
                                            res.json({success: true,msg:'User Updated Successfully',user:user});
                                     
                                }
                        });
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//Update Pan

apiRoutes.post('/updatePan', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    var userId=req.body.userId;
    console.log(contactNum);
    var pancard=req.body.pancard;
    
    if(contactNum!==undefined){
        User.findOne({ '_id': userId }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 
                      User.update({'_id': userId}, { $set: {"pancard":pancard,
                        "panRegistered":true}}, function (err, user) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update User Details'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    
                                            res.json({success: true,msg:'User Updated Successfully',user:user});
                                     
                                }
                        });
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//Add money to wallet

apiRoutes.post('/addMoney', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    var userId=req.body.userId;
    console.log(contactNum);
    var amount=req.body.amount;
    // var paymentId=req.body.paymentId;
    var date=req.body.date;
    var time=req.body.time;
    var transactionRef=req.body.transactionRef;
    console.log(amount);
 
    var transactionData ={
        userId:userId,
        contactNum:contactNum,
        date:date,
        time:time,
        amount:amount,
        transactionType:'Add Money',
        transactionRef:transactionRef
    }
    console.log(transactionData)
    
    // var token = req.body.Authorization;
    // console.log(token);
   // token= "JWT "+token;
    // var isauth= route.memberinfo(token,userId);
    if(contactNum!==undefined){
        User.findOne({ '_id': userId }, function(err, user) {
         
            if(!user){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 var prevAmount=user.wallet;
                 var updatedAmount=prevAmount+amount;
                      User.update({'_id': userId}, { $set: {wallet:updatedAmount}}, function (err, user) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update Wallet'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    Transaction.addTransaction(transactionData,function(err, transaction){
                                        if(err){
                                            console.log(err);

                                            res.json({success: false, msg: 'Failed to add Transaction'});
                                        //	throw err;
                                        }
                                        else{
                                            res.json({success: true,msg:'Money Added Successfully.',user:user,transaction:transaction});
                                        }
                                    });
                                   
                                }
                        });
                 
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});




//Send money wallet

apiRoutes.post('/sendMoney', function(req, res){
	  
   
    var contactNum=req.body.contactNum;
    var sendingNumber = req.body.sendingNumber;
    var recievingNumber = req.body.recievingNumber;
    console.log(contactNum);
    
    console.log(sendingNumber);
    console.log(recievingNumber);
    var amount=req.body.amount;
    var transactionRef=req.body.transactionRef;
    console.log(amount);
    var transactionData ={
        userId:userId,
        paymentId:paymentId,
        amount:amount,
        transactionType:'Send Money',
        transactionRef:transactionRef
    }
    if(contactNum!==undefined){
        User.findOne({ 'contactNum': recievingNumber }, function(err, ruser) {
            User.findOne({ 'contactNum': sendingNumber }, function(err, suser) {
            if(!ruser || !suser){
                res.json({success: false, msg: 'No user with this Contact Number exist'});
            }
                  
             else 
             
             {
                 var prevAmount=ruser.wallet;
                 var recieverAmount=prevAmount+amount;
                 var previousAmount=suser.wallet;
                 var senderAmount=previousAmount-amount;
                      User.update({'contactNum': recievingNumber}, { $set: {wallet:recieverAmount}}, function (err, ruser) {
                        User.update({'contactNum': sendingNumber}, { $set: {wallet:senderAmount}}, function (err, suser) {
                        //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    Transaction.addTransaction(transactionData,function(err, transaction){
                                        if(err){
                                            console.log(err);
                                            res.json({success: false, msg: 'Failed to add Transaction'});
                                        //	throw err;
                                        }
                                        else{
                                            res.json({success: true,msg:'User Successfully Updated',suser:suser,transaction:transaction});
                                        }
                                    });
                                   
                                }
                        });
                    });
            }
        });
    });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//Use Offer

apiRoutes.post('/useOffer', function(req, res){
    var couponUserArray =[];
    var userId=req.body.userId;
    var couponCode = req.body.couponCode;
    var couponId = req.body.couponId;
    User.findOne({'_id':userId},function(err,user){

        if(user!=undefined||user!=null){
            Offers.findOne({'_id':couponId}, function(err,coupon){
                if(coupon!=null||coupon!=undefined){
if(coupon.couponCode==couponCode){
    couponUserArray = coupon.userIds;
    for(var i=0;i<couponUserArray.length;i++){
        if(couponUserArray[i].userId==userId && couponUserArray[i].isApplied){
            res.json({success:false,msg:"Coupon Already Applied"});
            break;
        }else if(couponUserArray[i].userId==userId && !couponUserArray[i].isApplied){
            res.json({success:false,msg:"Coupon Successfully Applied"});
            break;
        }
        else{
            if(i==couponUserArray.length-1){
                var couponData = {
                    'userId':userId,
                    'isApplied':false
                }
                
                couponUserArray.push(couponData);

                Offers.findOneAndUpdate({'_id':couponId},{ $set : {
                    'userIds':couponUserArray
                }}, function(err, offerUpdated){
                    if(err){
                        res.json({success:false,msg:"Error Occured"});
                    }else{
                        res.json({success:false,msg:"Coupon Successfully Applied"});
                    }
                })

            }

        }
    }
 
}else{
    res.json({success:false,msg:"Not Active"});
}
                }else{
                    res.json({success:false,msg:"failed to fetch coupon"});
                }
            })
        }else{
            res.json({success:false,msg:"No user exist"});
        }
    })

});


//Forgot Password

apiRoutes.post('/forgotPassword', function(req, res){
	  
    // var userId=req.body.userId;
    var contactNum=req.body.contactNum;
    
    // var isauth= route.memberinfo(token,userId);
    
    if(contactNum!==undefined){
         User.findOne({ 'contactNum':contactNum }, function(err, user) {
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }else{
        
                 var pass=user.password;
                 var email= user.emailId;
                 var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                      user: 'iamrathor@gmail.com',
                      pass: 'abhinav123456'
                    }
                  });
                  
                  var mailOptions = {
                    from: 'iamrathor@gmail.com',
                    to: email,
                    subject: 'This is your password',
                    text: pass
                  };
                  
                  transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                      console.log(error);
                    } else {
                      console.log('Email sent: ' + info.response);
                    }
                  });
                  res.json({success:true,msg:'Password Sent to your Email.',user:user});
             }
            // City.find()
             
       
            
             
    });
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//Send Notifications

apiRoutes.post( '/sendNotification', function( req, response ) {

 var options = req.body;
 console.log(options);
var selectOption=req.body.selectOption;
var title = req.body.title;
    var description = req.body.description;
    var deviceKey = req.body.deviceKey;

  
console.log(selectOption+"SELECT OPTIONS");
if(selectOption=="AllUser"){
    var notificationData = {
        title:title,
        description:description,
       
        selectOption:selectOption,
        sentAll:true
    }
    userArr=[];
    var title = req.body.title;
    var description = req.body.description;
    User.find(function(err,users)
            {
               if (err){
                
            console.log(err);
              
              }else{
userArr = users;
for(var i=0;i<userArr.length;i++){
    axios
    .post('https://fcm.googleapis.com/fcm/send', {
    "notification":{
    "title":title,
    "body":description,
    "sound":"default",
    "click_action":"FCM_PLUGIN_ACTIVITY",
    "icon":"fcm_push_icon"
    },
    "data":{
    "forceStart":"1",
    "landing_page":"notifications",
    "userId":userArr[i]._id,
    "status":true,
    "moreData":description
    },
    "to":userArr[i].deviceId,
    "priority":"high",
    "restricted_package_name":""
    },
    {
    headers: {
    'Content-Type':'application/json',
    'Authorization': 'key=AAAA9R-oXmY:APA91bFHtWaWPpr_-rVnV9Mnk61SY8w1NaWs_MCQhdBhLUNzhicQPJkJOIukKvFfU3un8ON0i7sbVuXBBCMePizywVdgGsw_Cdbt6-488lIZBiyRhV6PQT1wFJpIZAOWGPraldG4zAdu' 
    }
    })
    .then(res => {
        console.log(`statusCode: ${res.statusCode}`);
        if(i==userArr.length-1){
            Notifications.addNotification(notificationData, function(err, notification){
                if(err){
                    
                    response.json({success: false,msg:'Unable to Save Notification.'});
                //	throw err;
                }else{
                    response.json({success: true,msg:'Notification Sent successfully',notification:notification});
                }
               
               
               
            });
        }else{

        }
        // console.log(res)
       
        })
        .catch(error => {
        console.error(error);

        })
              }
            }
            });
}else{
    var notificationData = {
        title:title,
        description:description,
        deviceKey:deviceKey,
        selectOption:selectOption,
        sentAll:false
    }
    

   
axios
.post('https://fcm.googleapis.com/fcm/send', {
"notification":{
"title":title,
"body":description,
"sound":"default",
"click_action":"FCM_PLUGIN_ACTIVITY",
"icon":"fcm_push_icon"
},
"data":{

    "forceStart":"1",
    "landing_page":"notifications",
    
    "status":true,
    "moreData":description
},
"to":deviceKey,
"priority":"high",
"restricted_package_name":""
},
{
headers: {
'Content-Type':'application/json',
'Authorization': 'key=AAAA9R-oXmY:APA91bFHtWaWPpr_-rVnV9Mnk61SY8w1NaWs_MCQhdBhLUNzhicQPJkJOIukKvFfU3un8ON0i7sbVuXBBCMePizywVdgGsw_Cdbt6-488lIZBiyRhV6PQT1wFJpIZAOWGPraldG4zAdu' 
}
})
.then(res => {
console.log(`statusCode: ${res.statusCode}`)
// console.log(res)
console.log("goeshere");
Notifications.addNotification(notificationData, function(err, notification){
    if(err){
        
        response.json({success: false,msg:'Unable to Save Notification.'});
    //	throw err;
    }else{
       console.log("goesinside");
        response.json({success: true,msg:'Notification Sent successfully',notification:notification});
    }
   
  
   
});

})
.catch(error => {
console.error(error);
response.json({success: false,msg:'Unable to send Notification.'});
})
}


});




//Prime Service Booking
apiRoutes.post('/bookPrimeService', function(req, res){


   
    
    
    var token = req.body.token;
    var userId=req.body.userId;
    var userName=req.body.userName;
 
    var selectedItems= req.body.selectedItems;
    var isauth= route.memberinfo(token,userId);
   
    token= "JWT "+token;

    var randomstring2 = require("randomstring");
       var cdata = {
        orderId :randomstring2.generate(8),
        eventDate : selectedItems.dateofEvent,
        guests:selectedItems.noOfGuest,
        orderType:selectedItems.orderType,
        description:selectedItems.description,
        vendorName:"Admin",
        vendorId:"adminId",
        userId : userId,
        userName : userName,
        time:selectedItems.time
        

       };
    
    if(isauth){
        User.findOne({ '_id':userId }, function(err, user) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }
        
            else
            {
                cdata.add=user.add;
                cdata.contactNum=user.contactNum
                Prime.addPrime(cdata, function(err, primeOrder){
                    if(err){
                        
                        res.json({success: false, msg: 'Failed to add Request'});
                    //	throw err;
                    }
                   
                    res.json({success: true,msg:'Request Sent Successfully',primeOrder:primeOrder});
                   
                });
            }
        });

    }
    else{
        res.json({success: false,msg:'Failed to add Request Token Authentication failed'});
    }
});

//Prime Vendor Booking
apiRoutes.post('/bookPrimeVendor', function(req, res){


   
    
    
    var token = req.body.token;
    var userId=req.body.userId;
    var userName=req.body.userName;
 
    var selectedItems= req.body.selectedItems;
    var isauth= route.memberinfo(token,userId);
  var vendorId=selectedItems.vendorId;
 
    token= "JWT "+token;

    var randomstring2 = require("randomstring");
       var cdata = {
        orderId :randomstring2.generate(8),
        eventDate : selectedItems.dateofEvent,
        guests:selectedItems.noOfGuest,
        orderType:selectedItems.orderType,
        description:selectedItems.description,
        vendorName:selectedItems.vendorName,
        vendorId:selectedItems.vendorId,
        userId : userId,
        userName : userName
        

       };
    
    if(isauth){
        User.findOne({ '_id':userId }, function(err, user) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }
        
            else
            {
                cdata.add=user.add;
                cdata.contactNum=user.contactNum
                Prime.addPrime(cdata, function(err, primeOrder){
                    if(err){
                        
                        res.json({success: false, msg: 'Failed to add Request'});
                    //	throw err;
                    }
                   
                    res.json({success: true,msg:'Request Sent Successfully',primeOrder:primeOrder});
                   
                });
            }
        });
        Vendor.findOne({ '_id':vendorId }, function(err, vendor) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }else{
                var cOrderId=cdata.orderId;
                var tAmt = cdata.totalAmount;
                var vendorDevice = vendor.deviceId;
                axios
                .post('https://fcm.googleapis.com/fcm/send', {
                  "notification":{
                    "title":"New Order Recieved",
                    "body":"Order Id: "+cOrderId+" Amount: " +tAmt,
                    "sound":"default",
                    "click_action":"FCM_PLUGIN_ACTIVITY",
                    "icon":"fcm_push_icon"
                  },
                  "data":{
                    "landing_page":"tabs/tab2",
                    "price":"$3,000.00"
                  },
                    "to":vendorDevice,
                    "priority":"high",
                    "restricted_package_name":""
                },
                {
                  headers: {
                   'Content-Type':'application/json',
                    'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
                  }
              })
                .then(res => {
                  console.log(`statusCode: ${res.statusCode}`)
                  console.log(res)
                })
                .catch(error => {
                  console.error(error)
                })
            }
        });

    }
    else{
        res.json({success: false,msg:'Failed to add Request Token Authentication failed'});
    }
});

//review vendor

apiRoutes.post('/review', function(req, res){


   
    var vendorId=req.body.vendId;
    
    var token = req.body.token;

    var userId=req.body.userId;
   
    var rate = req.body.rate;
    
    
    var isauth= route.memberinfo(token,userId);
    

       if(isauth){
      
        Vendor.findOne({ '_id':vendorId }, function(err, vendor) {
        if(err){
            console.log(err);
        res.json({success: false,msg:'Review Failed To Update'});
        throw err;
    }
      
                var prevRate = vendor.rate;
                if(typeof prevRate !== "undefined"){
                var updatedRate = (prevRate+rate)/2;
                var n = updatedRate.toFixed(2);
                }else{
                    var prevRate=0;
                }
                
           Vendor.update({'_id': vendorId}, { $set:{

               'rate':n,
               
             
           
               
               
               
           }}, function(err, numberAffected, rawResponse) {
              //handle it
                       if(err){
                           console.log(err);
                       res.json({success: false,msg:'Review Failed To Update'});
                       throw err;
                   }
                       else{
                           
                           res.json({success: true,msg:'Review Updated Successfully'});
                       }
                    });
           


                });
          
          
       }
       else{
           res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
       }

});

//review order

apiRoutes.post('/reviewOrder', function(req, res){


  
    var orderId=req.body.orderId;
    
    var token = req.body.token;
    var userId=req.body.userId;
    var userName=req.body.userName;
    if(req.body.feedback===undefined||req.body.feedback===null){
        var feedback ="No Feedback Given"
    }else{
        var feedback= req.body.feedback;
    }
    
    var rate = req.body.rate;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

       if(isauth){

           Order.update({ '_id':orderId}, { $set:{
               "rate":rate,
               "feedback":feedback,
               "isReviewed":true,
               "isRate":true,
               "isFeedback":true

               
           }}, function(err, numberAffected, rawResponse) {
              //handle it
                       if(err){
                           console.log(err);
                       res.json({success: false,msg:'Review Failed To Update'});
                       throw err;
                   }
                       else{
                            
                           res.json({success: true,msg:'Review Updated Successfully'});
                       }
                    });
           


                // }
          
          
       }
       else{
           res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
       }

});

//review order

apiRoutes.post('/setOrder', function(req, res){


   
    var orderId=req.body.orderId;
    
    var token = req.body.token;
    var userId=req.body.userId;
    var userName=req.body.userName;
    var feedback= req.body.feedback;
    var rate = req.body.rate;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

       if(isauth){

           Order.update({ '_id':orderId}, { $set:{
               
               "isReviewed":true,
               

               
           }}, function(err, numberAffected, rawResponse) {
              //handle it
                       if(err){
                           console.log(err);
                       res.json({success: false,msg:'Review Failed To Update'});
                       throw err;
                   }
                       else{
                            
                           res.json({success: true,msg:'Review Updated Successfully'});
                       }
                    });
           


                // }
          
          
       }
       else{
           res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
       }

});


//cancel order

apiRoutes.post('/cancelOrder', function(req, res){
	  

    var userId=req.body.userId;
    var vendorId=req.body.vendorId;
   
    var token=req.body.tokenstr;
   
    var orderId = req.body.orderId;
   
    var status = req.body.status;
    var review = true;
    
    
    
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
        Order.update({orderId: orderId}, { $set:{
            "status":status,
            "isReviewed":review,
            "isCancelled":true
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Order Failed To Update'});
                    throw err;
                }
                   
        });

        Vendor.findOne({ '_id':vendorId }, function(err, vendor) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }else{
                var orderId = req.body.orderId;
                
                var vendorDevice = vendor.deviceId;
                var tAmt=req.body.totalAmount;
              
                axios
                .post('https://fcm.googleapis.com/fcm/send', {
                  "notification":{
                    "title":"Order CANCELLED",
                    "body":"Order Id: "+orderId+" Amount: " +tAmt,
                    "sound":"default",
                    "click_action":"FCM_PLUGIN_ACTIVITY",
                    "icon":"fcm_push_icon"
                  },
                  "data":{
                    "landing_page":"tabs/tab2",
                    "price":"$3,000.00"
                  },
                    "to":vendorDevice,
                    "priority":"high",
                    "restricted_package_name":""
                },
                {
                  headers: {
                   'Content-Type':'application/json',
                    'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
                  }
              })
                .then(res => {
                  console.log(`statusCode: ${res.statusCode}`)
                  console.log(res)
                })
                .catch(error => {
                  console.error(error)
                })
            }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//cancel Appointment

apiRoutes.post('/cancelAppointment', function(req, res){
	  

    var userId=req.body.userId;
    var vendorId=req.body.vendorId;
    // console.log("HEY THIS IS VENDOR ID "+vendorId);
    var token=req.body.tokenstr;
  
    var orderId = req.body.orderId;
   
    var status = req.body.status;
    var review = true;
    
    
    
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
        Prime.update({orderId: orderId}, { $set:{
            "status":status,
            "isReviewed":review,
            "isCancelled":true
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Order Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Order Updated Successfully'});
                    }
        });

        Vendor.findOne({ '_id':vendorId }, function(err, vendor) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }else{
                var orderId = req.body.orderId;
                
                var vendorDevice = vendor.deviceId;
               
              
                axios
                .post('https://fcm.googleapis.com/fcm/send', {
                  "notification":{
                    "title":"Order CANCELLED",
                    "body":"Order Id: "+orderId+". ",
                    "sound":"default",
                    "click_action":"FCM_PLUGIN_ACTIVITY",
                    "icon":"fcm_push_icon"
                  },
                  "data":{
                    "landing_page":"tabs/tab2",
                    "price":"$3,000.00"
                  },
                    "to":vendorDevice,
                    "priority":"high",
                    "restricted_package_name":""
                },
                {
                  headers: {
                   'Content-Type':'application/json',
                    'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
                  }
              })
                .then(res => {
                  console.log(`statusCode: ${res.statusCode}`)
                  console.log(res)
                })
                .catch(error => {
                  console.error(error)
                })
            }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//laundryorder

apiRoutes.post('/laundryOrder', function(req, res){


   
    
  
    var token = req.body.token;
    var userId=req.body.userId;
    var userName=req.body.userName;
    var time = req.body.time;
   
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;

    var randomstring2 = require("randomstring");
       var cdata = {
        orderId :randomstring2.generate(8),
        time :time,
        userId : userId,
        userName : userName

       };
    
    if(isauth){
        User.findOne({ '_id':userId }, function(err, user) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }
        
            else
            {
                cdata.add=user.add;
                Laundry.addLaundry(cdata, function(err, laundry){
                    if(err){
                        
                        res.json({success: false, msg: 'Failed to add Request'});
                    //	throw err;
                    }
                   
                    res.json({success: true,msg:'Request Sent Successfully',laundry:laundry});
                   
                });
            }
        });
    }
    else{
        res.json({success: false,msg:'Failed to add Request Token Authentication failed'});
    }
});


//Accept order
apiRoutes.post('/acceptOrder/:vendorId/:token', function(req, res){
	  

    var vendorId=req.params.vendorId;
   
    var token=req.params.token;
  
    var orderId = req.body.orderId;
   
    var status = req.body.status;
    var userId =req.body.userId;
   
    
    
    var isauth= route.memberinfo(token,vendorId);
    
    if(isauth){
        Order.update({orderId: orderId}, { $set:{
            "status":status
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Order Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Order Updated Successfully'});
                    }
        });

        User.findOne({ '_id':userId }, function(err, user) {
            var userDevice = user.deviceId;
            console.log(userDevice);
            console.log(userId)
            
             
            if(err){
                
               res.json("failed to fetch data");
                
            }else{
                var orderId = req.body.orderId;
                var tAmt = req.body.totalAmount;
                
              console.log(userDevice);
              console.log(tAmt);
              
                axios
                .post('https://fcm.googleapis.com/fcm/send', {
                  "notification":{
                    "title":"Order Accepted",
                    "body":"Order Id: "+orderId+" Amount: " +tAmt,
                    "sound":"default",
                    "click_action":"FCM_PLUGIN_ACTIVITY",
                    "icon":"fcm_push_icon"
                  },
                  "data":{
                    "landing_page":"tabs/tab2",
                    "price":"$3,000.00"
                  },
                    "to":userDevice,
                    "priority":"high",
                    "restricted_package_name":""
                },
                {
                  headers: {
                   'Content-Type':'application/json',
                    'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
                  }
              })
                .then(res => {
                  console.log(`statusCode: ${res.statusCode}`)
                  console.log(res)
                })
                .catch(error => {
                  console.error(error)
                })
            }
        });
     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//Get Vendor device token

apiRoutes.post('/setVendorDeviceId/', function(req, res){
	  

    var vendorId=req.body.vendorId;
  
    var token=req.body.token;
   
    
    var tokenDevice = req.body.tokenDevice;
   
    
    var isauth= route.memberinfo(token,vendorId);
    
    if(isauth){
        Vendor.update({'_id': vendorId}, { $set:{
            "deviceId":tokenDevice
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Vendor Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Vendor Updated Successfully'});
                    }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//RejectOrder
apiRoutes.post('/rejectOrder/:vendorId/:token', function(req, res){
	  

    var vendorId=req.params.vendorId;
  
    var token=req.params.token;
   
    var orderId = req.body.orderId;
   
    var status = req.body.status;
    var userId =req.body.userId;
    
    
    
    var isauth= route.memberinfo(token,vendorId);
    
    if(isauth){
        Order.update({orderId: orderId}, { $set:{
            "status":status,
            "isCancelled":true
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Order Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Order Updated Successfully'});
                    }
        });

        User.findOne({ '_id':userId }, function(err, user) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }else{
                var orderId = req.body.orderId;
                
                var userDevice = user.deviceId;
                var tAmt=req.body.totalAmount;
              
                axios
                .post('https://fcm.googleapis.com/fcm/send', {
                  "notification":{
                    "title":"Order Rejected",
                    "body":"Order Id: "+orderId+" Amount: " +tAmt,
                    "sound":"default",
                    "click_action":"FCM_PLUGIN_ACTIVITY",
                    "icon":"fcm_push_icon"
                  },
                  "data":{
                    "landing_page":"tabs/tab2",
                    "price":"$3,000.00"
                  },
                    "to":userDevice,
                    "priority":"high",
                    "restricted_package_name":""
                },
                {
                  headers: {
                   'Content-Type':'application/json',
                    'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
                  }
              })
                .then(res => {
                  console.log(`statusCode: ${res.statusCode}`)
                  console.log(res)
                })
                .catch(error => {
                  console.error(error)
                })
            }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//GetNotification

apiRoutes.post('/getNotifications', function(req, res){
	
    var userId=req.body.userId;
   
    var deviceKey=req.body.deviceKey;
    console.log(deviceKey)
    
    if(userId){

        User.findOne({'_id':userId},function(err,user){
            if(err){
                res.json({success: false,msg:'No user Found'});
            }else{
                Notifications.find({'deviceKey':deviceKey},function(err,notification){
                    if(err){
                        res.json({success: false,msg:'Unable to fetch'});
                    }else{
                        res.json({success: true,msg:'Fetched Notification',data:notification});
                    }
                });
            }
        })
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//end

//AskReview

apiRoutes.post('/askReview/:vendorId/:token', function(req, res){
	  

    var vendorId=req.params.vendorId;
   
    var token=req.params.token;
    
    var orderId = req.body.orderId;
    
    var status = req.body.status;
    
    
    
    var isauth= route.memberinfo(token,vendorId);
    
    if(isauth){
        Order.update({orderId: orderId}, { $set:{
            "status":status,
            
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Order Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Order Updated Successfully'});
                    }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//Completed Order
apiRoutes.post('/completeOrder/:vendorId/:token', function(req, res){
	  

    var vendorId=req.params.vendorId;
    
    var token=req.params.token;
    
    var orderId = req.body.orderId;
    
    var status = req.body.status;
    var userId =req.body.userId;
    
    
    var isauth= route.memberinfo(token,vendorId);
    
    if(isauth){
        Order.update({orderId: orderId}, { $set:{
            "status":status,
            
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Order Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Order Updated Successfully'});
                    }
        });
        
        User.findOne({ '_id':userId }, function(err, user) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }else{
                var orderId = req.body.orderId;
                var userDevice = user.deviceId;
                console.log(userDevice);
                var tAmt=req.body.totalAmount;
              
                axios
                .post('https://fcm.googleapis.com/fcm/send', {
                  "notification":{
                    "title":"Order Delivered",
                    "body":"Order Id: "+orderId+" Amount: " +tAmt,
                    "sound":"default",
                    "click_action":"FCM_PLUGIN_ACTIVITY",
                    "icon":"fcm_push_icon"
                  },
                  "data":{
                    "landing_page":"tabs/tab2",
                    "price":"$3,000.00"
                  },
                    "to":userDevice,
                    "priority":"high",
                    "restricted_package_name":""
                },
                {
                  headers: {
                   'Content-Type':'application/json',
                    'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
                  }
              })
                .then(res => {
                  console.log(`statusCode: ${res.statusCode}`)
                  console.log(res)
                })
                .catch(error => {
                  console.error(error)
                })
            }
        });



     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Delivery Status
apiRoutes.post('/deliveryStatus/:vendorId/:token', function(req, res){
	  

    var vendorId=req.params.vendorId;
    var token=req.params.token;
    var storeId = req.body.storeId;
    var deliveryStatus = req.body.deliveryStatus;
        var delivery=deliveryStatus;
        var isauth= route.memberinfo(token,vendorId);
    
    if(isauth){
        Store.update({_id: storeId}, { $set:{
            'deliveryStatus':delivery
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Status Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Status Updated Successfully'});
                    }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
   
});

//store settings

apiRoutes.post('/storeSettings/:vendorId/:token', function(req, res){
     
    var vendorId=req.params.vendorId;
    var token=req.params.token;
    var isauth= route.memberinfo(token,vendorId);
    
    var storeId = req.body.storeId;
    var operationDays = req.body.obj;
    var timings = req.body.timings;
        
       
    
    if(isauth){
        Store.update({_id: storeId}, { $set:{
            'operationDays':operationDays,
            'Timings':timings
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Status Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Status Updated Successfully'});
                    }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
   
});

apiRoutes.post('/getOperator', function(req, res){
     
    var contactNum=req.body.contactNum;
  

console.log(res);
Operators.find({}, function(err,operators){
if(err){

}else{
    
  axios.get('https://api.datayuge.com/v1/lookup/'+contactNum, {
    params: {
      foo: 'bar'
    }
  }).then((response)=>{
    res.json({success:true,data:response.data,operators:operators,msg:"Browse Plans"});
}).catch((err)=> {
    
    console.log(err)
    res.json({success:false,data:err,operators:operators,msg:"Please Select Operator"});
})
}
});


   
});



//get order

apiRoutes.get('/getOrders/:userId/:token' , function(req,res){

    var userId=req.params.userId;
    var token=req.params.token;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

    if(isauth){
        Order.find({userId:userId},function(err,orders)
        
        {
            res.json({success:true,orders:orders});
        });
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});

//get Prime Products

apiRoutes.get('/getAppointment/:userId/:token' , function(req,res){

    var userId=req.params.userId;
    var token=req.params.token;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

    if(isauth){
        Prime.find(function(err,primeOrders)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }else{
                                                var odata=[];
                                                       for(var k=0; k<primeOrders.length;k++){
                                                           if(primeOrders[k].userId===userId){
                                                               console.log(primeOrders[k].userId);
                                                               console.log(userId);
                                                               odata.push(primeOrders[k]);
                                                         
                                                           }else{
                                                               console.log("Dont add to list filter");
                                                           }
                                                        }
                                                     
                                                          
                                                      

                                              }
            res.json({success:true,odata:odata});
        });
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});


apiRoutes.get('/getUserDashboard/:userId', function(req, res){
	  
    var userId=req.params.userId;
    var token=req.params.token;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

    if(userId){
        
        
         User.findOne({ '_id':userId }, function(err, user) {
             
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }
             if(!(isEmpty(user)))
                 {
             var accessLevelName =user.accessLevelName;
           
                     
                   
            if (user.isSuperAdmin==true || accessLevelName=="superadmin")
            {
                       
                 
                        
            
           

            User.find(function(err,users)
            {
               if (err){
                
            console.log(err);
              
              }
              Recharge.find(function(err,orders)
              {
                 if (err){
                  
              console.log(err);
                
                }

                Operators.find(function(err,operators)
                {
                   if (err){
                    
                console.log(err);
                  
                  }

                Voucher.find(function(err,voucherTxn)
                {
                   if (err){
                    
                console.log(err);
                  
                  }

                  Subscription.find(function(err,subscriptions)
                  {
                     if (err){
                      
                  console.log(err);
                    
                    }

                  BBPS.find(function(err,bbpsTxn)
                {
                   if (err){
                    
                console.log(err);
                  
                  }

                  DMT.find(function(err,dmtTxn)
                  {
                     if (err){
                      
                  console.log(err);
                    
                    }

                Notifications.find(function(err,notifications)
                {
                   if (err){
                    
                console.log(err);
                  
                  }
                

                Electricity.find(function(err,electricity)
                {
                   if (err){
                    
                console.log(err);
                  
                  }

                  Gas.find(function(err,gas)
                {
                   if (err){
                    
                console.log(err);
                  
                  }
                  FastTag.find(function(err,fasttag)
              {
                 if (err){
                  
              console.log(err);
                
                }
                LifeInsurance.find(function(err,lifeInsurance)
              {
                 if (err){
                  
              console.log(err);
                
                }
    
                HealthInsurance.find(function(err,healthInsurance)
              {
                 if (err){
                  
              console.log(err);
                
                }

                LoanRepayment.find(function(err,loanRepayment)
                {
                   if (err){
                    
                console.log(err);
                  
                  }
                  MunicipalTaxes.find(function(err,municipalTaxes)
              {
                 if (err){
                  
              console.log(err);
                
                }
                
                 
                
                EducationFees.find(function(err,educationFees)
                {
                   if (err){
                    
                console.log(err);
                  
                   }
                   LandlinePostpaid.find(function(err,landlinePostpaid)
              {
                 if (err){
                  
              console.log(err);
                
                }

                HousingSociety.find(function(err,housingSociety)
                {
                   if (err){
                    
                console.log(err);
                  
                  }

                  Broadband.find(function(err,broadband)
                  {
                     if (err){
                      
                  console.log(err);
                    
                    }

                    Water.find(function(err,water)
                    {
                       if (err){
                        
                    console.log(err);
                      
                      }

                      Offers.find(function(err,offers)
                      {
                         if (err){
                          
                      console.log(err);
                        
                        }
                        Trend.find(function(err,topBanner)
                        {
                           if (err){
                            
                        console.log(err);
                          
                          }

                          Transaction.find(function(err,transactions)
                        {
                           if (err){
                            
                        console.log(err);
                          
                          }
             
                                   

                                                                     //JRI BALANCE CHECK

                                                    var Email='lokeshb@gmx.com';
                                                    var Password='123321';
                                                    var systemReference=makeid(15);
                                                        var ApiChkSum = md5(Email+Password+systemReference)
                                                        axios
                                                        .post('https://api.gorecharge.in/vouchersystem/Voucher.svc/json/GetBalance', {
                                                         
                                                         
                                                            "Email":Email,
                                                            "Systemreferenceno":systemReference,
                                                            "APIChkSum":ApiChkSum,
                                                           
                                                            
                                                         
                                                            
                                                        },
                                                        {
                                                          headers: {
                                                           'Content-Type':'application/json',
                                                             
                                                          }
                                                        })
                                                        .then(dataRes => {
                                                        //   console.log(`statusCode: ${res.statusCode}`)
                                                        console.log(dataRes);
                                                          console.log("THIS is Success RESOPONSE" +dataRes);
                                                          var accessCode= 'AVTM87KZ80ZB95QHDY';
                                                          var reqId = randomstring.generate(35);

                                                          var workingkey='C516828299E4BE70B6B0612ACFE795FD';
                                                     var req = '<?xml version="1.0" encoding="UTF-8"?><depositDetailsRequest><fromDate>2020-09-10</fromDate><toDate>2020-09-15</toDate><transType>CR</transType><agents><agentId>CC01IA83MOBA00000001</agentId></agents></depositDetailsRequest>'
                                                    
                                                      var encdata= encrypt(req,workingkey);
                                                      
                                                      request.post('https://api.billavenue.com/billpay/enquireDeposit/fetchDetails/xml', {
                                                        form: {
                                                      accessCode:accessCode,
                                                      requestId:reqId,
                                                      encRequest:encdata,
                                                      ver:'1.0',
                                                      instituteId:'IA83'
                                                        }
                                                      }, function (err, httpResponse, body) { 
                                                          
                                                         // console.log(body);
                                                  
                                                          console.log("Body ends");
                                                        
                                                          if(err){
                                                              console.log(err);
                                                          }
                                                         
                                                          try {
                                                        
                                                            var decrypted=decrypt(body,workingkey);
                                                            result2 = JSON.parse(convert.xml2json(decrypted, {compact: true, spaces: 4}));
                                                             console.log(result2)
                                                             var bbpsBalance= result2.DepositEnquiryResponse.currentBalance._text;
                                                             console.log(bbpsBalance);
                                                                
                                                                  console.log("check asap");
                                                             console.log(reqId);
                                                             console.log(req);
                                                              res.json({success:true,water:water,broadband:broadband,housingSociety:housingSociety,user:user,landlinePostpaid:landlinePostpaid,educationFees:educationFees,municipalTaxes:municipalTaxes,loanRepayment:loanRepayment,healthInsurance:healthInsurance,
                                                                lifeInsurance:lifeInsurance,fasttag:fasttag,users:users,gas:gas, notifications:notifications,bbpsTxn:bbpsTxn,subscriptions:subscriptions,
                                                                electricity:electricity,orders:orders,offers:offers,topBanner:topBanner,voucherTxn:voucherTxn,dmtTxn:dmtTxn,
                                                                transactions:transactions,operators:operators,jriRes:dataRes.data.Balance,bbpsBalance:bbpsBalance});
                                                               
                                                          }
                                                          catch (e) {
                                                                        console.log("error"+e);
                                                                        console.log(e);
                                                                      var decrypted = "";
                                                                      
                                                                    }
                                                               
                                                               });
                                                         
                                                         
                                                     }).catch(error => {
                                                        console.error(error+"THIS is ERROR");
                                                       
                                                      
                                                      }) 
                         
                                                    });
                        });
                    });
                    });
                    });
                    });
                });
                });
            });
                    });
                });
                });
            });
                });
            });
        });
    });
    });
});
});
});
});
});
                         
            }
            //for admin            
                        
             
             
                 }
             else
                 {
                     res.json({success: false,msg:'users does not exist'});
                 }
    });
         
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});

//fetch offers
apiRoutes.post('/fetchVouchers', function(req, res){
    var userId=req.body.userId;
    User.find({'_id':userId}, function(err,user){
        if(err){
            res.json({success: false,msg:'users does not exist'});
        }else{
            Offers.find(function(err,offers)
            {
               if (err){
                
            console.log(err);
              
              }
              else{
                res.json({success: true,msg:'Vouchers Fetched',offers:offers});
              }
            })
        }
    })

                                                });


                                                //test offers
apiRoutes.post('/testVoucher', function(req, res){
    var userId=req.body.userId;
    var offerId=req.body.offerId;
    var amount=req.body.amount;
    var couponCode=req.body.couponCode;
    var applyData={
        'offerId':offerId,
        'userId':userId,
        'amount':amount,
        'couponCode':couponCode
    }
    User.find({'_id':userId}, function(err,user){
        if(err){
            res.json({success: false,msg:'users does not exist'});
        }else{
            AppliedVoucher.find({'offerId':offerId,'userId':userId},function(err,offer)
            {
               if (err){
                
            console.log(err);
            res.json({success: false,msg:'Vouchers is Not Applicable'});
              }
              else{
                 console.log(offer);
               if(offer.length==0){
                AppliedVoucher.addApplyVoucher(applyData,function(err, voucher){
                    if(err){
                        console.log(err);
                        res.json({success: false, msg: 'Failed to Apply Coupon'});
                    //	throw err;
                    }
                    else{
                        res.json({success:true,msg:'Coupon code applied.',data:voucher});
                    }
                });

               }else{
                console.log(offer[0]);
                console.log("CHECK DIFF");
                if(offer[0]._doc.isActive){
                    res.json({success:false,msg:'Coupon code already used.'});
                }else{
                    res.json({success:true,msg:'Coupon code applied.',data:offer[0]});
                }
               }
                
              }
            })
        }
    })

});


    //Redeem Unused Coupon Code

    apiRoutes.post('/redeemVoucher', function(req, res){
        var voucherId = req.body.voucherId;
        var userId = req.body.userId;
        
        AppliedVoucher.update({'_id': voucherId}, { $set: {'isActive':false}}, 
    function (err, product) {
        //handle it
                 if(err){
                 res.json({success: false,msg:'err'});
                 //throw err;
                 console.log(err);
             }
                 else{
                     res.json({success: true,msg:'Coupon removed.'});
                 }
         });

   });

apiRoutes.get('/getUserDashboard/:userId/:token', function(req, res){
	
    var userId=req.params.userId;
    var token=req.params.token;
    var isauth= route.memberinfo(token,userId);
    token= "JWT "+token;
    

    if(userId){
        
        
         User.findOne({ '_id':userId }, function(err, user) {
             
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }
             if(!(isEmpty(user)))
                 {
             var accessLevelName =user.accessLevelName;
           
                     
                   
            if (user.isSuperAdmin==true || accessLevelName=="superadmin")
            {
                       
                 
                        
            
           

            User.find(function(err,users)
            {
               if (err){
                
            console.log(err);
              
              }

                                    Order.find(function(err,orders)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }

                                              Operators.find(function(err,operators)
                                              {
                                                 if (err){
                                                  
                                              console.log(err);
                                                
                                                }
                                              
                             
                                              Electricity.find(function(err,electricity)
                                              {
                                                 if (err){
                                                  
                                              console.log(err);
                                                
                                                }
                                                FastTag.find(function(err,fasttag)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }
                                              LifeInsurance.find(function(err,lifeInsurance)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }
                                  
                                              HealthInsurance.find(function(err,healthInsurance)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }

                                              LoanRepayment.find(function(err,loanRepayment)
                                              {
                                                 if (err){
                                                  
                                              console.log(err);
                                                
                                                }
                                                MunicipalTaxes.find(function(err,municipalTaxes)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }
                                              
                                               
                                              
                                              EducationFees.find(function(err,educationFees)
                                              {
                                                 if (err){
                                                  
                                              console.log(err);
                                                
                                                 }
                                                 LandlinePostpaid.find(function(err,landlinePostpaid)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }

                                              HousingSociety.find(function(err,housingSociety)
                                              {
                                                 if (err){
                                                  
                                              console.log(err);
                                                
                                                }

                                                Broadband.find(function(err,broadband)
                                                {
                                                   if (err){
                                                    
                                                console.log(err);
                                                  
                                                  }

                                                  Water.find(function(err,water)
                                                  {
                                                     if (err){
                                                      
                                                  console.log(err);
                                                    
                                                    }

                                                    //JRI BALANCE CHECK

                                                    var Email='lokeshb@gmx.com';
                                                   
                                                var systemReference=this.makeid(15);
                                                    var ApiChkSum = md5(Email,systemReference)
                                                    axios
                                                    .post('https://api.gorecharge.in/vouchersystem/Voucher.svc/json/GetBalance', {
                                                     
                                                     
                                                        "Email":Email,
                                                        "Systemreferenceno":systemReference,
                                                        "APIChkSum":ApiChkSum,
                                                       
                                                        
                                                     
                                                        
                                                    },
                                                    {
                                                      headers: {
                                                       'Content-Type':'application/json',
                                                         
                                                      }
                                                    })
                                                    .then(dataRes => {
                                                    //   console.log(`statusCode: ${res.statusCode}`)
                                                    console.log(dataRes);
                                                      console.log("THIS is Success RESOPONSE" +dataRes);
                                                     
                                                      res.json({success:true,water:water,broadband:broadband,housingSociety:housingSociety,user:user,landlinePostpaid:landlinePostpaid,educationFees:educationFees,municipalTaxes:municipalTaxes,loanRepayment:loanRepayment,healthInsurance:healthInsurance,
                                                        lifeInsurance:lifeInsurance,fasttag:fasttag,operators:operators,jriRes:dataRes,
                                                        electricity:electricity,orders:orders,offers:offers,topBanner:topBanner});
                                                 }).catch(error => {
                                                    console.error(error+"THIS is ERROR");
                                                   
                                                  
                                                  })   

                                    

                                        
                                            
                                                
              
                         
                
                        });
                    });
                    });
                });
                    });
                });
            });
        });
    });
    });
});
});
});
});

                         
            }
            //for admin            
                        
             
             
                 }
             else
                 {
                     res.json({success: false,msg:'users does not exist'});
                 }
    });
         
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

//get vendor dashboard
apiRoutes.get('/getVendorDashboard/:vendorId/:token', function(req, res){
	if(res.success===true){
        var vendorId=req.params.vendorId;
    }
    var vendorId=req.params.vendorId;
    
    var token=req.params.token;
    var isauth= route.memberinfo(token,vendorId);
    token= "JWT "+token;
    

    if(isauth){
        
        
         Vendor.findOne({ '_id':vendorId }, function(err, vendor) {
             
             
             if(err){
                 
                res.json("failed to fetch data");
                 
             }
             if(!(isEmpty(vendor)))
                 {
             var accessLevelName =vendor.accessLevelName;
           
                     
                   
            if (accessLevelName=="vendor")
            {
                       
                     Product.find(function(err, products) {
                        
            
            if (err){
                console.log("error");
                res.send(err);
            }
            var produ=[];
            for(var f=0;f<products.length;f++){
                if(products[f].productVendorId===vendorId){
                    produ.push(products[f]);
                }
            }

            SubCategory.find(function(err,subcategories)
                          {
                             if (err){
                                console.log(err);
                             }
                             var subcat=[]
                             for(var v=0;v<subcategories.length;v++){
                                if(subcategories[v].subCategoryVendorId===vendorId){
                                    subcat.push(subcategories[v]);
                                }else{
                                    console.log("server Error");
                                }
                            }
            

                           
                         
                            Category.find(function(err, category) {
                                if(err)
                                    {
                                            console.log("Error");
                                    }
                                    var categ=[];
                                    for(var e=0;e<category.length;e++){
                                        if(category[e].categoryVendorId===vendorId){
                                            categ.push(category[e]);
                                        }
                                    }
                                    var fdata=[];
                                    for(var j=0; j<categ.length;j++){
                                        var prod = [];
                                        var cat = {};
                                        for(var k =0; k<produ.length; k++){
                                            if(categ[j].categoryName==produ[k].categoryName){
                                                prod.push(products[k]);


                                            }
                                        }
                                        cat.categ=category[j].categoryName;
                                        cat.produ=prod;
                                        fdata.push(cat);
                                        
                                        
                
                                    }
                                    

                                    
                             Store.find(function(err,storeD)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }
                                              var storeDet= []
                                              for(var u=0;u<storeD.length;u++){
                                                  if(storeD[u].storeVendorId===vendorId){
                                                    storeDet.push(storeD[u]);
                                                  }else{
                                                      console.log("server Error");
                                                  }
                                              }
                                            
                                              if(vendor.vendorCategoryName==="Home Cleaning"||vendor.vendorCategoryName==="Home Cleaning"
                                              || vendor.vendorCategoryName==="Auto-Service"||vendor.vendorCategoryName==="Laundry"){
                                                var pOrder=[];
                                                var odata=[];
                                                var comp=[];
                                                var newOrder=[];
                                               
                                               Prime.find(function(err,primeOrders)
                                               {
                                                  if (err){
                                                     console.log(err);
                                                  }
                                                  for(var x=0;x<primeOrders.length;x++){
                                                    
                   
                                                       if(primeOrders[x].vendorId===vendorId){
                                                           pOrder.push(primeOrders[x]);
                                                        }else{
                                                        }
                                                    
                                                }
                                                for(var k=0; k<pOrder.length;k++){
                                                    if(pOrder[k].status==="Accepted"){
                                                        odata.push(pOrder[k]);
                                                    }else if(pOrder[k].status==="Completed"){
                                                        comp.push(pOrder[k]);
                                                    
                                                    }else{
                                                        newOrder.push(pOrder[k]);
                                                    }
                                                }
                                               
                                                
              
                         
                 res.json({success:true,produ:produ,categ:categ,fdata:fdata,subcat:subcat,storeDet:storeDet,odata:odata,comp:comp,newOrder:newOrder,vendor:vendor});
                });

                                        
            }else{
                Order.find(function(err,orders)
                {
                   if (err){
                      console.log(err);
                   }
                   var filterOrder = [];
                   for(var x=0;x<orders.length;x++){
                       for(var y=0;y<orders[x].orderDesc.length;y++){

                          if(orders[x].orderDesc[y].productVendorId===vendorId){
                              filterOrder.push(orders[x]);
                           }else{
                               console.log("Server Error");
                           }
                       }
                       
                   }
                  
                   var odata=[];
                   var comp=[];
                   var newOrder=[];
                          for(var k=0; k<filterOrder.length;k++){
                              if(filterOrder[k].status==="Accepted"){
                                  odata.push(filterOrder[k]);
                              }else if(filterOrder[k].status==="Completed" || filterOrder[k].status==="CANCELLED" || filterOrder[k].status==="Rejected"){
                                  comp.push(filterOrder[k]);
                              
                              }else{
                                  newOrder.push(filterOrder[k]);
                              }
                          }

                          res.json({success:true,produ:produ,categ:categ,fdata:fdata,subcat:subcat,storeDet:storeDet,odata:odata,comp:comp,newOrder:newOrder,fOrder:filterOrder,vendor:vendor});
                        });
            }                  
                 
                    });
                   
                });
            });
        });
            
                       
                         
            }
            //for admin            
                        
             else if (user.isSuperAdmin==true){
                 
                 User.find(function(err, user) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
                res.send(err);
            }
                    Product.find(function(err, products) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err){
                res.send(err);
            }
                        
                              
                                        Category.find(function(err,categories)
                                            {
                                               if (err){
                                                
                                            console.log(err);
                                              
                                              }
                                              Order.find(function(err,orders)
                                              {
          
                                                   
                                    res.json({success: true,users: user,products:products,orders:orders,categories:categories});
                                            });
                                   
                                        
                                     }) ;   
                                    });
        });
            
             }
             
                 }
             else
                 {
                     res.json({success: false,msg:'Vendor does not exist'});
                 }
    });
         
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }
});

















apiRoutes.post('/updateUserDetails/:userId', function(req, res){
	  
    var token = getToken(req.headers);
    var userIdupdate=req.body.userId;
       var userId=req.params.userId;
    var isauth= route.memberinfo(token,userId);
    var user =req.body;
    var isPasswordChange=req.body.pwdmode;
    
    if(userId){

        if(isPasswordChange)
            {
                User.findOne({ '_id': userIdupdate }, function(err, user) {
         
         if(!user){
             res.json({success: false, msg: 'No user with this email id exist'});
         }
         
         else{
             
        
            
                    var pass = req.body.pwd;
             
                    user.password=pass;
         
             
                    user.save(function(err,user){
                 
             
                    User.update({ _id:user._id }, { $set: { password:user.password}}, function (err, user)
                                {
                        if (err) throw err;
                       else{
                                    res.json({success: true,msg:'Password Successfully Updated'});
                                }
                                });
                        

        }); 
                
                
            }
         });
            }
                      
             else
             
             {
                 
                      User.update({'_id': userIdupdate}, user, function(err, numberAffected, rawResponse) {
                       //handle it
                                if(err){
                                res.json({success: false,msg:'Failed to Update'});
                                //throw err;
                                console.log(err);
                            }
                                else{
                                    res.json({success: true,msg:'User Successfully Updated'});
                                }
                        });
                 
                 
             }
 
        
      
     
    
        
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//update vendorDetails

apiRoutes.post('/updateVendorDetails/:vendorId', function(req, res){
	  
   
    var vendorId=req.params.vendorId;
    console.log(vendorId);
    var isAddress=req.body.isAddress;
    var token = req.body.Authorization;
    console.log(token);
   // token= "JWT "+token;
    var isauth= route.memberinfo(token,vendorId);
    if(isauth){
        Vendor.findOne({ '_id': vendorId }, function(err, vendor) {
         
            if(!vendor){
                res.json({success: false, msg: 'No vendor with this Contact Number exist'});
            }
            
            else{
        if(isAddress === false)
            {
               
                    var address = req.body.add;
                   
         
             
                    Vendor.update({'_id': vendorId}, { $set: {'vendorAddress':address}}, function (err, vendor) {
                        //handle it
                                 if(err){
                                 res.json({success: false,msg:'Failed to Update Address'});
                                 //throw err;
                                 console.log(err);
                             }
                                 else{
                                     res.json({success: true,msg:'Address Successfully Updated'});
                                 }
                         });
                
                
           
            }
                    
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});


//update KYC

apiRoutes.post('/updateVendorKYC/:vendorId', function(req, res){
	  
   
    var vendorId=req.params.vendorId;
    console.log(vendorId);
   
    var token = req.body.Authorization;
    console.log(token);
    var KYCNumber = req.body.kyc;
   // token= "JWT "+token;
    var isauth= route.memberinfo(token,vendorId);
    if(isauth){
        Vendor.findOne({ '_id': vendorId }, function(err, vendor) {
         
            if(!vendor){
                res.json({success: false, msg: 'No vendor with this ID exist'});
            }
            
            else{
      
           
                    Vendor.update({'_id': vendorId}, { $set: {'kyc':KYCNumber}}, function (err, vendor) {
                        //handle it
                                 if(err){
                                 res.json({success: false,msg:'Failed to Update KYC'});
                                 //throw err;
                                 console.log(err);
                             }
                                 else{
                                     res.json({success: true,msg:'KYC Successfully Updated'});
                                 }
                         });
                
                
           
           
                    
            }
        });
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});



apiRoutes.post('/setDeviceId/', function(req, res){
	  
        
           var userId=req.body.userId;
           var token = req.body.token;
        var isauth= route.memberinfo(token,userId);
        var deviceId =req.body.tokenDevice;
    console.log(deviceId);
        
        if(isauth){
    
         
             
            
                User.update({'_id': userId}, { $set:{
                    "deviceId":deviceId
                    
                }}, function(err, numberAffected, rawResponse) {
                   //handle it
                            if(err){
                                console.log(err);
                            res.json({success: false,msg:'Status Failed To Update'});
                            throw err;
                        }
                            else{
                                 
                                res.json({success: true,msg:'Status Updated Successfully'});
                            }
                });
           
                
        }
        else{
            res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
        }
    
    });



apiRoutes.post('/updateProduct',  uploadMiddleware.single('attachment'),  function( req, res, next ){


    var token = getToken(req.headers);
    var userId=req.body.userId;
    
    var productId = req.body.productId;
    var productName = req.body.productName;
    var productCost = parseInt(req.body.productCost);
    var productSell = parseInt(req.body.productsell);
    var productQuantity = req.body.productQuantity;
    var productCategoryId = req.body.productCategoryId;
    var productCategoryName = req.body.productCategoryName;

    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
        if(req.body.subCatImg=="false")
        {
            Product.update({_id: productId}, { $set:{
                "productName":productName,
                "productCost":productCost,
                "productsell":productSell,
                "productQuantity":productQuantity,
                "categoryName":productCategoryName,
                "categoryId":productCategoryId,
            }}, function(err, numberAffected, rawResponse) {
               //handle it
                        if(err){
                            console.log(err);
                        res.json({success: false,msg:'Product Failed To Update'});
                        throw err;
                    }
                        else{
                             
                            res.json({success: true,msg:'Product Updated Successfully'});
                        }
            });

        }
        else {
        
            Product.update({_id: productId}, { $set:{
                "productName":productName,
                "productCost":productCost,
                "productsell":productSell,
                "productQuantity":productQuantity,
                "categoryName":productCategoryName,
                "categoryId":productCategoryId,
                "productUrl":baseUrl+req.file.filename

            }}, function(err, numberAffected, rawResponse) {
               //handle it
                        if(err){
                            console.log(err);
                        res.json({success: false,msg:'Product Failed To Update'});
                        throw err;
                    }
                        else{
                             
                            res.json({success: true,msg:'Product Updated Successfully'});
                        }
            });


        }
        

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Add Latest News And Updates

apiRoutes.post( '/addNews', uploadMiddleware.single('attachment'), function( req, res, next ) {

 
    var token = getToken(req.headers);
    var userId=req.body.userId;
    var news = req.body;
    
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
    var newsData = {

        newsTitle:news.newsTitle, 
        newsDescription:news.newsDescription,  
        newsType:news.newsType,       
        newsVendorId:news.newsVendorId,
        newsVendorName:news.newsVendorName,
        newsUrl:baseUrl+req.file.filename,
      

    }



 

 
  

 var isauth= route.memberinfo(token,userId);

 if(isauth){

    Feed.addFeed(newsData,function(err, newsData){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'News and Updates Added Successfully',data:newsData});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Token Authentication failed'});
 }

});

//update Feed
apiRoutes.post('/updateNews',  uploadMiddleware.single('attachment'),  function( req, res, next ){
	  

    var token = getToken(req.headers);
    var userId=req.body.userId;
    var feed = req.body;
    var feedId = req.body.feedId;
   console.log(feed);
    
    
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
        if(req.body.subCatImg=="false")
        {
          var feedData = {
              
            newsTitle:feed.newsTitle,        
            newsDescription:feed.newsDescription
              
          }
          
          
              Feed.update({_id: feedId}, { $set:{
                "newsTitle":feedData.newsTitle,
                  "newsDescription":feedData.newsDescription,
                  
                 
                  
              }}, function(err, numberAffected, rawResponse) {
                 //handle it
                          if(err){
                              console.log(err);
                          res.json({success: false,msg:'Store Failed To Update'});
                          throw err;
                      }
                          else{
                               
                              res.json({success: true,msg:'Store Updated Successfully'});
                          }
              });
      
      }
          else
      {
          var feedData = {
               
            newsTitle:feed.newsTitle,        
            newsDescription:feed.newsDescription,    
              feedUrl:baseUrl+req.file.filename
          
          }
                    
              Feed.update({_id: trendId}, { $set:{
                  
                  "newsTitle":feedData.newsTitle,
                  "newsDescription":feedData.newsDescription,
                  "newsUrl":feedData.feedUrl
                  
              }}, function(err, numberAffected, rawResponse) {
                 //handle it
                          if(err){
                              console.log(err);
                          res.json({success: false,msg:'Store Failed To Update'});
                          throw err;
                      }
                          else{
                               
                              res.json({success: true,msg:'Store Updated Successfully'});
                          }
              });
         
      }
       
     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Update Store

apiRoutes.post('/updateStore',  uploadMiddleware.single('attachment'),  function( req, res, next ){
	  

    var token = getToken(req.headers);
    var userId=req.body.userId;
    var store = req.body;
    var storeId=store.storeId;
    console.log(store)
    console.log(userId);
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
        if(req.body.subCatImg=="false")
  {
    var storeData = {
        
        storeId:store.storeId,  
        storeName:store.storeName,        
        storeContactNumber:store.storeContactNumber,
        storeAddress:store.storeAddress 
      
    }
 
    
        Store.update({_id: storeId}, { $set:{
            "storeName":storeData.storeName,
            "storeContactNumber":storeData.storeContactNumber,
            "storeAddress":storeData.storeAddress,
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Store Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Store Updated Successfully'});
                    }
        });

}
    else
{
    var storeData = {
        storeId:store.storeId,  
        storeName:store.storeName,        
        storeContactNumber:store.storeContactNumber,    
        storeUrl:baseUrl+req.file.filename,
        storeAddress:store.storeAddress
        
    
    }
              
        Store.update({_id: storeId}, { $set:{
            "storeName":storeData.storeName,
            "storeContactNumber":storeData.storeContactNumber,
            "storeAddress":storeData.storeAddress,
            "storeUrl":storeData.storeUrl
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Store Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Store Updated Successfully'});
                    }
        });
    
   
    
    
 

}
 
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Update Vendor Cat

apiRoutes.post('/updateVendorCat',  uploadMiddleware.single('attachment'),  function( req, res, next ){
	  

    var token = getToken(req.headers);
    var userId=req.body.userId;
    var vendorCat = req.body;
    var vendorCategoryId=req.body.vendorCategoryId;
   
    console.log(userId);
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
        if(req.body.subCatImg=="false")
  {
    var vendCatData = {
        
       
        vendorCategoryName:vendorCat.vendorCategoryName,        
     
      
    }
 
    
        vendorCategory.update({_id: vendorCategoryId}, { $set:{
            "vendorCategoryName":vendCatData.vendorCategoryName,
           
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Store Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Store Updated Successfully'});
                    }
        });

}
    else
{
    var vendCatData = {
        
        vendorCategoryName:vendorCat.vendorCategoryName,        
       
        vendorCategoryUrl:baseUrl+req.file.filename,
        
        
    
    }
              
        vendorCategory.update({_id: vendorCategoryId}, { $set:{
            "vendorCategoryName":vendCatData.vendorCategoryName,
            
            "vendorCategoryUrl":vendCatData.vendorCategoryUrl
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Store Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Store Updated Successfully'});
                    }
        });
    
   
    
    
 

}
 
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Delete Feed

//delete expensehead using expensehead  id
apiRoutes.post('/deleteFeed', function(req, res){
	var feedId = req.body.feedId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(isauth){
        Feed.findByIdAndRemove({_id: feedId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Feed'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Feed Successfully Deactivated'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});

//delete Product

apiRoutes.post('/deleteProduct', function(req, res){
	var productId = req.body.productId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(isauth){
        Product.findByIdAndRemove({_id: productId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Product'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Product Removed Successfully'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});

//Delete Store


apiRoutes.post('/deleteStore', function(req, res){
	var storeId = req.body.storeId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(isauth){
        Store.findByIdAndRemove({_id: storeId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No Such Store'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Store Removed Successfully'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});



//Delete CAtegory

apiRoutes.post('/deleteCategory', function(req, res){
	var categoryId = req.body.categoryId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(isauth){
        Category.findByIdAndRemove({_id: categoryId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Feed'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Category Removed Successfully'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});

//delete Subcategory

apiRoutes.post('/deleteSubCategory', function(req, res){
	var subCategoryId = req.body.subCategoryId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(isauth){
        SubCategory.findByIdAndRemove({_id: subCategoryId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Feed'});
			throw err;
		}
            else{
                res.json({success: true,msg:'SubCategory Removed Successfully'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});

//delete vendorCategory


apiRoutes.post('/deleteVendorCategory', function(req, res){
	var vendorCategoryId = req.body.vendorCategoryId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(isauth){
        vendorCategory.findByIdAndRemove({_id: vendorCategoryId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Feed'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Vendor Category Removed Successfully'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});

apiRoutes.post('/deleteVendor', function(req, res){
	var vendorId = req.body.vendorId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(isauth){
        Vendor.findByIdAndRemove({_id: vendorId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Feed'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Vendor  Removed Successfully'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});
//Add Type and trends

apiRoutes.post( '/addTrend', uploadMiddleware.single('attachment'), function( req, res, next ) {

 
    var token = getToken(req.headers);
    var userId=req.body.userId;
    var trends = req.body;
    
    var isauth= route.memberinfo(token,userId);
    var trendData = {
        trendTitle:trends.trendTitle,
        trendType:trends.trendType,
        trendDescription:trends.trendDescription, 
        trendVendorId:trends.trendVendorId,
        trendVendorName:trends.trendVendorName,       
        
        trendUrl:baseUrl+req.file.filename,
      

    }



 
 var isauth= route.memberinfo(token,userId);

 if(userId){

    Trend.addTrend(trendData,function(err, trendData){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Banner Added Successfully',data:trendData});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Token Authentication failed'});
 }

});

//update Trend
apiRoutes.post('/updateTrend',  uploadMiddleware.single('attachment'),  function( req, res, next ){
	  
    var token = getToken(req.headers);
    var userId=req.body.userId;
    var trend = req.body;
    var trendId = req.body.trendId;
    var isauth= route.memberinfo(token,userId);
    
    if(userId){
        if(req.body.subCatImg=="false")
        {
          var trendData = {
              
              trendTitle:trend.trendTitle,        
              trendDescription:trend.trendDescription
              
          }
       
          
              Trend.update({_id: trendId}, { $set:{
                  "trendDescription":trendData.trendDescription,
                  "trendTitle":trendData.trendTitle
                 
                  
              }}, function(err, numberAffected, rawResponse) {
                 //handle it
                          if(err){
                              console.log(err);
                          res.json({success: false,msg:'Banner Failed To Update'});
                          throw err;
                      }
                          else{
                               
                              res.json({success: true,msg:'Banner Updated Successfully'});
                          }
              });
      
      }
          else
      {
          var trendData = {
               
            trendTitle:trend.trendTitle,        
              trendDescription:trend.trendDescription,    
              trendUrl:baseUrl+req.file.filename
          
          }
                    
              Trend.update({_id: trendId}, { $set:{
                  
                  "trendTitle":trendData.trendTitle,
                  "trendDescription":trendData.trendDescription,
                  "trendUrl":trendData.trendUrl
                  
              }}, function(err, numberAffected, rawResponse) {
                 //handle it
                          if(err){
                              console.log(err);
                          res.json({success: false,msg:'Banner Failed To Update'});
                          throw err;
                      }
                          else{
                               
                              res.json({success: true,msg:'Banner Updated Successfully'});
                          }
              });
         
      }
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Delete Feed

//delete expensehead using expensehead  id
apiRoutes.post('/deleteTrend', function(req, res){
	var trendId = req.body.trendId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(userId){
        Trend.findByIdAndRemove({_id: trendId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Banner'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Banner Successfully Deleted'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});


//Add Operator

apiRoutes.post( '/addOperator', uploadMiddleware.single('attachment'), function( req, res, next ) {

 
    var token = getToken(req.headers);
    var userId=req.body.userId;
    var operator = req.body;
    
    var isauth= route.memberinfo(token,userId);
    var operatorData = {
        operatorName:operator.operatorName,
    
        benefits:operator.benefits, 
      validity:operator.validity,     
      amount:operator.amount,
     
      operatorUrl:baseUrl+req.file.filename,
      

    }



 
 var isauth= route.memberinfo(token,userId);

 if(userId){

    Operators.addOperator(operatorData,function(err, operator){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Operator Added Successfully',data:operator});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Token Authentication failed'});
 }

});

//delete expensehead using expensehead  id
apiRoutes.post('/deleteOperator', function(req, res){
	var operatorId = req.body.operatorId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(userId){
        Operators.findByIdAndRemove({_id: operatorId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Trend'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Operator Successfully Deactivated'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});


//Add Offers

apiRoutes.post( '/addOffer', uploadMiddleware.single('attachment'), function( req, res, next ) {

 
    var token = getToken(req.headers);
    var userId=req.body.userId;
    var offers = req.body;
    
    var isauth= route.memberinfo(token,userId);
    var offerData = {
        title:offers.title,
    
        description:offers.description, 
      validity:offers.validity,     
      couponCode:offers.couponCode,
      amount:offers.amount,
      offerUrl:baseUrl+req.file.filename,
      

    }



 
 var isauth= route.memberinfo(token,userId);

 if(userId){

    Offers.addOffer(offerData,function(err, offer){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'Offer Added Successfully',data:offer});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Token Authentication failed'});
 }

});



//update Trend
apiRoutes.post('/updateOffer',  uploadMiddleware.single('attachment'),  function( req, res, next ){
	  
    var token = getToken(req.headers);
    var userId=req.body.userId;
    var trend = req.body;
    var trendId = req.body.trendId;
    var isauth= route.memberinfo(token,userId);
    
    if(userId){
        if(req.body.subCatImg=="false")
        {
          var trendData = {
              
              trendTitle:trend.trendTitle,        
              trendDescription:trend.trendDescription
              
          }
       
          
              Trend.update({_id: trendId}, { $set:{
                  "trendDescription":trendData.trendDescription,
                  "trendTitle":trendData.trendTitle
                 
                  
              }}, function(err, numberAffected, rawResponse) {
                 //handle it
                          if(err){
                              console.log(err);
                          res.json({success: false,msg:'Store Failed To Update'});
                          throw err;
                      }
                          else{
                               
                              res.json({success: true,msg:'Store Updated Successfully'});
                          }
              });
      
      }
          else
      {
          var trendData = {
               
            trendTitle:trend.trendTitle,        
              trendDescription:trend.trendDescription,    
              trendUrl:baseUrl+req.file.filename
          
          }
                    
              Trend.update({_id: trendId}, { $set:{
                  
                  "trendTitle":trendData.trendTitle,
                  "trendDescription":trendData.trendDescription,
                  "trendUrl":trendData.trendUrl
                  
              }}, function(err, numberAffected, rawResponse) {
                 //handle it
                          if(err){
                              console.log(err);
                          res.json({success: false,msg:'Store Failed To Update'});
                          throw err;
                      }
                          else{
                               
                              res.json({success: true,msg:'Store Updated Successfully'});
                          }
              });
         
      }
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Delete Feed


apiRoutes.post('/deleteOffer', function(req, res){
	var offerId = req.body.offerId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(userId){
        Offers.findByIdAndRemove({_id: offerId}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Trend'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Offer Successfully Deactivated'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});

//Subscription

apiRoutes.post( '/addSubscription', uploadMiddleware.single('attachment'), function( req, res, next ) {

 
    var token = getToken(req.headers);
    var userId=req.body.userId;
    var subscriptions = req.body;
    
    var isauth= route.memberinfo(token,userId);
   // token= "JWT "+token;
    var subscriptionData = {
        title:req.body.title,        
        description:req.body.description, 
        validity:subscriptions.validity,
        amount:subscriptions.amount,  
        freeTransaction:subscriptions.freeTransaction, 
           
        
        subscriptionUrl:baseUrl+req.file.filename,
      

    }



 
 var isauth= route.memberinfo(token,userId);

 if(isauth){

    Subscription.addSubscription(subscriptionData,function(err, subscriptionData){
        if(err){
            console.log(err);
            res.json({success: false, msg: 'Failed to add Request'});
        //	throw err;
        }
        else{
            res.json({success:true,msg:'News and Updates Added Successfully',data:subscriptionData});
        }
    });
    
 }
 else{
     res.json({success: false,msg:'Failed to update Token Authentication failed'});
 }

});

//update Subscription
apiRoutes.post('/updateSubscription', uploadMiddleware.single('attachment'),  function( req, res, next ){
	  

    var token = getToken(req.headers);
    var userId=req.body.userId;
    
    var subscriptionId = req.body.subscriptionId;
    var subscription = req.body;
   
   
    
    
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){

        if(req.body.subCatImg=="false")
        {
          var subscriptionData = {
              
            subscriptionName:subscription.subscriptionName,        
            subscriptionDescription:subscription.subscriptionDescription,
            subscriptionAmount:subscription.subscriptionAmount,
            subscriptionOfferPrice:subscription.subscriptionOfferPrice,
          }
       
          
              Subscription.update({_id: subscriptionId}, { $set:{
                "subscriptionName":subscriptionData.subscriptionName,
                "subscriptionDescription":subscriptionData.subscriptionDescription,
                "subscriptionAmount":subscriptionData.subscriptionAmount,
                "subscriptionOfferPrice":subscriptionData.subscriptionOfferPrice,
                  
              }}, function(err, numberAffected, rawResponse) {
                 //handle it
                          if(err){
                              console.log(err);
                          res.json({success: false,msg:'Store Failed To Update'});
                          throw err;
                      }
                          else{
                               
                              res.json({success: true,msg:'Store Updated Successfully'});
                          }
              });
      
      }
          else
      {
          var subscriptionData = {
               
            subscriptionName:subscription.subscriptionName,        
            subscriptionDescription:subscription.subscriptionDescription,
            subscriptionAmount:subscription.subscriptionAmount,
            subscriptionOfferPrice:subscription.subscriptionOfferPrice,
            subscriptionUrl:subscription.subscriptionUrl,
          }
              Subscription.update({_id: subscriptionId}, { $set:{
                  
                "subscriptionName":subscriptionData.subscriptionName,
                "subscriptionDescription":subscriptionData.subscriptionDescription,
                "subscriptionAmount":subscriptionData.subscriptionAmount,
                "subscriptionOfferPrice":subscriptionData.subscriptionOfferPrice,
                "subscriptionUrl":subscriptionData.subscriptionUrl,
                  
              }}, function(err, numberAffected, rawResponse) {
                 //handle it
                          if(err){
                              console.log(err);
                          res.json({success: false,msg:'Store Failed To Update'});
                          throw err;
                      }
                          else{
                               
                              res.json({success: true,msg:'Store Updated Successfully'});
                          }
              });
         
      }
        
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Delete Feed

//delete expensehead using expensehead  id
apiRoutes.post('/deleteSubscription', function(req, res){
	var subscriptionId = req.body.subscriptionId;
      var token = getToken(req.headers);
    var userId=req.body.userId;
    var isauth= route.memberinfo(token,userId);
    if(isauth){
        Subscription.findByIdAndRemove({_id: subscriptionId}, {
    isActive: false
}, function(err, numberAffected, rawResponse) {
   //handle it
            if(err){
            res.json({success: false,msg:'No such Subscription'});
			throw err;
		}
            else{
                res.json({success: true,msg:'Subscription Successfully Deactivated'});
            }
});

    }
    
    else{
        res.json({success: false,msg:'Not Authorised'});
    }
});

//SubscriptionOrder

apiRoutes.post('/subscriptionOrder', function(req, res){


    var txamt=req.body.txAmt;
    
    var token = req.body.token;
    var userId=req.body.userId;
    var userName=req.body.userName;
    var count = req.body.count;
    var selectedItems= req.body.selectedItems;
    var isauth= route.memberinfo(token,userId);
    var subscriptionVendorId = selectedItems[0].subscriptionVendorId;
    var sdate = Date.now();
    var startDate = moment(sdate);
   
    var endDate =  moment(startDate).add(1, 'months').calendar();
    var ea = moment(endDate);
    var showea = moment(ea).format("MMM Do YY");
    var randomstring2 = require("randomstring");
    var subId=randomstring2.generate(8);
    selectedItems[0].subId=subId;
       var cdata = {
        subscriptionId :subId,

        orderDesc : selectedItems,
        
        
        totalAmount : txamt,
        startDate:startDate,
        endDate:ea,
        pauseDate:sdate,
        userId : userId,
        userName : userName
        

       };
    if(isauth){
        User.findOne({ '_id':userId }, function(err, user) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }
        
            else
            {
                cdata.add=user.add;
                ActiveSubscription.addsubscriptionOrder(cdata, function(err, suborder){
                    if(err){
                        
                        res.json({success: false, msg: 'Failed to add Request'});
                    //	throw err;
                    }
                   
                    res.json({success: true,msg:'Request Sent Successfully',subscriptionorder:suborder});
                   
                });
            }
        });

        Vendor.findOne({ '_id':subscriptionVendorId }, function(err, vendor) {
             
             
            if(err){
                
               res.json("failed to fetch data");
                
            }else{
                var subscriptionId=cdata.subscriptionId;
                var tAmt = cdata.totalAmount;
                var vendorDevice = vendor.deviceId;
                axios
                .post('https://fcm.googleapis.com/fcm/send', {
                  "notification":{
                    "title":"New Active Subscription",
                    "body":"Subscription Id: "+subscriptionId+" Amount: " +tAmt+".This subscription will expire on "+showea+".",
                    "sound":"default",
                    "click_action":"FCM_PLUGIN_ACTIVITY",
                    "icon":"fcm_push_icon"
                  },
                  "data":{
                    "landing_page":"tabs/tab2",
                    "price":"$3,000.00"
                  },
                    "to":vendorDevice,
                    "priority":"high",
                    "restricted_package_name":""
                },
                {
                  headers: {
                   'Content-Type':'application/json',
                    'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
                  }
              })
                .then(res => {
                  console.log(`statusCode: ${res.statusCode}`)
                  console.log(res)
                })
                .catch(error => {
                  console.error(error)
                })
            }
        });
         
      
    }
    else{
        res.json({success: false,msg:'Failed to add Request Token Authentication failed'});
    }
});

//Pause subscription

apiRoutes.post('/pauseProduct', function(req, res){
	  
  
   
    
    var token = req.body.token;
    var vendorId=req.body.vendorId;
    var isauth= route.memberinfo(token,vendorId);
    var productId=req.body.productId;
    
    var isActive=req.body.isActive;
  
    
    if(isauth){

      
        Product.update({'_id': productId}, { $set: {'isActive':isActive}}, function (err, product) {
            //handle it
                     if(err){
                     res.json({success: false,msg:'Product Failed to Pause'});
                     //throw err;
                     console.log(err);
                 }
                     else{
                         res.json({success: true,msg:'Product Paused Successfully'});
                     }
             });

             
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//Resume Subscription

apiRoutes.post('/resumeProduct/', function(req, res){
	  
  
   
   
    var token = req.body.token;
    var vendorId=req.body.vendorId;
    var isauth= route.memberinfo(token,vendorId);
    var productId=req.body.productId;
    
    var isActive=req.body.isActive;
    
    if(isauth){
        console.log(productId);
       

            
        ActiveSubscription.update({'_id': productId}, { $set: {'isActive':isActive}}, 
        function (err, product) {
            //handle it
                     if(err){
                     res.json({success: false,msg:'Product Failed to Update '});
                     //throw err;
                     console.log(err);
                 }
                     else{
                         res.json({success: true,msg:'Product Resumed Successfully Updated'});
                     }
             });
           

           
   
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

//raw

apiRoutes.post('/updateBrand', function(req, res){
	  

    var token = getToken(req.headers);
    var userId=req.body.userId;
    
    var brandId = req.body.brandId;
    var brandName = req.body.brandName;
    
    
    var isauth= route.memberinfo(token,userId);
    
    if(isauth){
        Brand.update({_id: brandId}, { $set:{
            "brandName":brandName
            
        }}, function(err, numberAffected, rawResponse) {
           //handle it
                    if(err){
                        console.log(err);
                    res.json({success: false,msg:'Brand Failed To Update'});
                    throw err;
                }
                    else{
                         
                        res.json({success: true,msg:'Brand Updated Successfully'});
                    }
        });

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

    apiRoutes.post('/updatevendor',  uploadMiddleware.single('attachment'),  function( req, res, next ){	  

    var token = getToken(req.headers);
    console.log(JSON.stringify(req.body));
    var userId=req.body.userId;
    var vendorId = req.body.vendorId;
    var vendorCategoryId = req.body.vendorCategoryId;
    var vendorCategoryName = req.body.vendorCategoryName;

    var isauth= route.memberinfo(token,userId);

    if(isauth){
        if(req.body.subCatImg=="false")
        {
            Vendor.update({_id: vendorId}, { $set:{
                "vendorCategoryId":vendorCategoryId,
                "vendorCategoryName":vendorCategoryName
                
            }}, function(err, numberAffected, rawResponse) {
               //handle it
                        if(err){
                            console.log(err);
                        res.json({success: false,msg:'Vendor Failed To Update'});
                        throw err;
                    }
                        else{
                             
                            res.json({success: true,msg:'Vendor Updated Successfully'});
                        }
            });
        }

        else {
            Vendor.update({_id: vendorId}, { $set:{
                "vendorCategoryId":vendorCategoryId,
                "vendorCategoryName":vendorCategoryName,
                "vendorUrl":baseUrl+req.file.filename
                
            }}, function(err, numberAffected, rawResponse) {
               //handle it
                        if(err){
                            console.log(err);
                        res.json({success: false,msg:'Vendor Failed To Update'});
                        throw err;
                    }
                        else{
                             
                            res.json({success: true,msg:'Vendor Updated Successfully'});
                        }
            });


        }
      

     
       
    }
    else{
        res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
    }

});

// user forget password

apiRoutes.post('/getotp', function(req, res){
    
   
    if(req.body.contactNum)
        {
            User.findOne({ 'contactNum': req.body.contactNum }, function(err, user) {
         
         if(!user){
             res.json({success: false, msg: 'No user with this mobile number exist'});
         }
         
         else{
             
            options = {
                 min:0000,
                 max:  9999
              ,
              integer: true
              }
              var pass =  randomNumber(options)
 
         
             
             user.password=pass;
         
             
                User.update({'contactNum': req.body.contactNum}, { $set: {password:user.password}}, function (err, userupdate) {
                    //handle it
                             if(err){
                             res.json({success: false,msg:'Failed to Update Address'});
                             //throw err;
                             console.log(err);
                         }
                            
        
         //push nitification
         var deviceId=user.deviceId;
         var otp = user.password;
         var userName = user.userName;
         axios
         .post('https://fcm.googleapis.com/fcm/send', {
           "notification":{
             "title":"OTP",
             "body":"This is your otp "+otp+".",
             "sound":"default",
             "click_action":"FCM_PLUGIN_ACTIVITY",
             "icon":"fcm_push_icon"
           },
           "data":{
             "landing_page":"login",
             
           },
             "to":deviceId,
             "priority":"high",
             "restricted_package_name":""
         },
         {
           headers: {
            'Content-Type':'application/json',
             'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
           }
       })
         .then(res => {
           console.log(`statusCode: ${res.statusCode}`)
           console.log(res)
         })
         .catch(error => {
           console.error(error)
         })
         
         res.json({success: true,msg:'otp sent Successfully',user:userupdate});
 });

  
        
    }
}); 
        }else{
            res.json({success: false,msg:'Failed To send otp'});
        }
   
    });
        

     apiRoutes.post('/verifyOtp', function(req, res){
        if(req.body.pass)
        {
            User.findOne({ contactNum: req.body.contactNum, password: req.body.pass}, function(err, user) {
         
         if(!user){
             res.json({success: false, msg: 'No user with this mobile number exist'});
         }else{
            res.json({success: true, msg: 'Password set'});
         }
        });
    }
    });

     apiRoutes.post('/setUserPass', function(req, res){
         
        if(req.body.contactNum)
        {
            User.findOne({ 'contactNum': req.body.contactNum}, function(err, user) {
                if(!user){
                    res.json({success: false, msg: 'No user with this mobile number exist'});
                }else{
                    var pass = req.body.pass;
             
                    user.password=pass;
             
                    user.save(function(err,user){
                    User.update({'contactNum': req.body.contactNum}, { $set: {'password':user.password}}, function (err, userupdate) {
                        //handle it
                                 if(err){
                                 res.json({success: false,msg:'Failed to Update password'});
                                 //throw err;
                                 console.log(err);
                             }
                
                            });
                        });
            res.json({success: true, msg: 'Password set'});
         
        
    }
    });

    }else{
        res.json({success: false, msg: 'No contact exist'});
    }
    });


    //vendor forget password

    apiRoutes.post('/getVendorOtp', function(req, res){
    
   
        if(req.body.contactNum)
            {
                Vendor.findOne({ 'vendorContactNumber': req.body.contactNum }, function(err, vendor) {
             
             if(!vendor){
                 res.json({success: false, msg: 'No user with this mobile number exist'});
             }
             
             else{
                 
                options = {
                     min:0000,
                     max:  9999
                  ,
                  integer: true
                  }
                  var pass =  randomNumber(options)
     
             
                 
                 vendor.password=pass;
             
                 
                    Vendor.update({'vendorContactNumber': req.body.contactNum}, { $set: {'password':vendor.password}}, function (err, vendorupdate) {
                        //handle it
                                 if(err){
                                 res.json({success: false,msg:'Failed to Update Address'});
                                 //throw err;
                                 console.log(err);
                             }
                                
            
             //push nitification
             var deviceId=vendor.deviceId;
             var otp = vendor.password;
            
             axios
             .post('https://fcm.googleapis.com/fcm/send', {
               "notification":{
                 "title":"OTP",
                 "body":"This is your otp "+otp+".",
                 "sound":"default",
                 "click_action":"FCM_PLUGIN_ACTIVITY",
                 "icon":"fcm_push_icon"
               },
               "data":{
                 "landing_page":"tabs/tab1",
                 
               },
                 "to":deviceId,
                 "priority":"high",
                 "restricted_package_name":""
             },
             {
               headers: {
                'Content-Type':'application/json',
                 'Authorization': 'key=AAAAdqT0nfo:APA91bH3p5tYgbQPJo8ZVVk_agg1TgU_g9iNaQo7rn4rjvoXAc7mBsHEB6-uz3HUJnboBMIEJU_hsUqkYFZbKFmpvYkwHBhzVCY9NLY8n9gWNDkS9xylEJzkeFgkYVJ8LJYyirqEElfr' 
               }
           })
             .then(res => {
               console.log(`statusCode: ${res.statusCode}`)
               console.log(res)
             })
             .catch(error => {
               console.error(error)
             })
             
             res.json({success: true,msg:'otp sent Successfully',vendor:vendorupdate});
     });
    
      
            
        }
    }); 
            }else{
                res.json({success: false,msg:'Failed To send otp'});
            }
       
        });
            
    
         apiRoutes.post('/verifyVendorOtp', function(req, res){
            if(req.body.pass)
            {
                Vendor.findOne({ 'vendorContactNumber': req.body.contactNum, 'password': req.body.pass}, function(err, vendor) {
             
             if(!vendor){
                 res.json({success: false, msg: 'No user with this mobile number exist'});
             }else{
                res.json({success: true, msg: 'Password set'});
             }
            });
        }
        });
    
         apiRoutes.post('/setVendorPass', function(req, res){
             
            if(req.body.contactNum)
            {
                Vendor.findOne({ 'vendorContactNumber': req.body.contactNum}, function(err, vendor) {
                    if(!vendor){
                        res.json({success: false, msg: 'No user with this mobile number exist'});
                    }else{
                        var pass = req.body.pass;
                 
                        vendor.password=pass;
                 
                        vendor.save(function(err,user){
                        Vendor.update({'vendorContactNumber': req.body.contactNum}, { $set: {'password':vendor.password}}, function (err, userupdate) {
                            //handle it
                                     if(err){
                                     res.json({success: false,msg:'Failed to Update password'});
                                     //throw err;
                                     console.log(err);
                                 }
                    
                                });
                            });
                res.json({success: true, msg: 'Password set'});
             
            
        }
        });
    
        }else{
            res.json({success: false, msg: 'No contact exist'});
        }
        });
   


        //getUserReviews

        apiRoutes.post('/getUserReviews', function(req, res){
	  
  
   
            var vendorId=req.body.vendorId;
            var token = req.body.token;
            var isauth= route.memberinfo(token,vendorId); 
            
            // var productName = req.body.productName;
            
            if(isauth){
        
              
                Order.find({}, function(error, orders) {
                   
                       
                        if(error){
                         
                            res.json("failed to fetch data");
                             
                         }
                         else{
                            var review=[];
                               var filterOrder = [];
                               for(var x=0;x<orders.length;x++){
                                   for(var y=0;y<orders[x].orderDesc.length;y++){
                                      if(orders[x].orderDesc[y].productVendorId===vendorId && orders[x].isCancelled===false && orders[x].isRate===true){
                                          filterOrder.push(orders[x]);
                                       }else{
                                           console.log("Order id does not match");
                                       }
                                      
                                   
                                   
                                    }
                                   
                               }
                               var odata=[];
                             var comp=[];
                             var newOrder=[];
                                    for(var k=0; k<filterOrder.length;k++){
                                        if(filterOrder[k].status==="Accepted"){
                                            odata.push(filterOrder[k]);
                                        }else if(filterOrder[k].status==="Completed" && filterOrder[k].rate != 0){
                                            comp.push(filterOrder[k]);
                                        
                                        }else{
                                            newOrder.push(filterOrder[k]);
                                        }
                                    }
                              
                              
                              
                               
                             
                      
                         res.json({success:true,msg:'Subscription List fetched successfully',review:comp});
                        
                        }
                    
        
        });
        
                 
            }
            else{
                res.json({success: false,msg:'Unable to fetch Token Authentication failed'});
            }
            
        
        });

    

apiRoutes.post('/login', route.login);
 apiRoutes.post('/signup', route.signup);
 apiRoutes.post('/signupUser', route.signupUser);
//  apiRoutes.post('/loginUser', route.loginVendor);
apiRoutes.post('/forgot', route.forgot);
// Start the server
app.listen(port);

console.log('Server is running at  http://localhost:' + port);        
        
        
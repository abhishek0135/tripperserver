var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OperatorSchema = new Schema({
   
 
    operatorName :
    {
       type: String,
        required:true,
       
    },
      planName:
      {
      type:String,
  
      },
      benefits:
      {
      type:String,
      
      },
      validity:
      {
      type:String,
      
      },
      amount:
      {
      type:Number,
      
      },
      operatorUrl:
      {
      type:String
      
      },
      createdDate:
      {
          type:Date,
          default:Date.now
      }
});
 

var Operator= module.exports = mongoose.model('Operator', OperatorSchema);

module.exports.addOperator = function(operator, callback){
     
      Operator.create(operator, callback);
};



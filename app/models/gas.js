var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GasSchema = new Schema({
   
    
    billerId: 
    {
        type: String,
        required: true
    },
    billerName: 
    {
        type: String,
       
    },
   
    billerCategory :
    {
       type: String  
         
    },

    inputParam:{
        type:[]
      },


    billerAdhoc :
    {
       type: String  
         
    },
    billerCoverage :
    {
       type: String  
         
    },
    
    billerFetchRequiremet:{
        type:String
    },
    billerPaymentExactness:{
        type:String
    },
    billerSupportBillValidation:{
        type:String
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
    isActive:
    {
        type:Boolean,
        default:true
    },

    billerUrl:{
        type:String
    }
   
    
   
    
});
 

var Gas= module.exports = mongoose.model('Gas', GasSchema);
module.exports.addGas = function(gas, callback){
     
	Gas.create(gas, callback);
};

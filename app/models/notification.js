var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NotificationSchema = new Schema({
    title :
    {
       type: String,
        required:true,
        unique:true
    },
    description:{
        type: String,
        required:true,
    },
    deviceKey:{
        type: String,
     
    },

    selectOption:{
        type: String,
        required:true,
    },

    sentAll:{
        type: Boolean
    },
    
    createdDate:
    {
        type:Date,
        default:Date.now
    },
    
   
    
    
});
 
var Notification= module.exports = mongoose.model('Notifications', NotificationSchema);

module.exports.addNotification = function(notification, callback){
      console.log("Notification Saved"+notification);
    
	Notification.create(notification, callback);
};

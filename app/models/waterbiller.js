var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WaterSchema = new Schema({

    billerId: 
    {
        type: String,
        required: true
    },
    billerName: 
    {
        type: String,
       
    },
   
    billerCategory :
    {
       type: String  
         
    },

    inputParam:{
        type:[]
      },


    billerAdhoc :
    {
       type: String  
         
    },
    billerCoverage :
    {
       type: String  
         
    },
    
    billerFetchRequiremet:{
        type:String
    },
    billerPaymentExactness:{
        type:String
    },
    billerSupportBillValidation:{
        type:String
    },
    billerUrl:{
        type:String
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
    isActive:
    {
        type:Boolean,
        default:true
    },
    
});
 

var Water= module.exports = mongoose.model('Water', WaterSchema);

module.exports.addWater = function(water, callback){
      
	Water.create(water, callback);
};
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TransactionSchema = new Schema({
      createdTime : 
    { 
        type : Date,
         default: Date.now 
    },
    userId:{
        type:String,
        
    },
    userName: 
    {
        type: String,
      
    },
    contactNum: 
    {
        type: Number,
      
    },
     paymentId:
    {
        type:String,
        
    },
   transactionRef:
    {
        type:String,
        
    },
    amount : 
    {
        type: Number
    },
    transactionType: 
    {
        type: String,
      
    },
    timeStamp:{
        type: String,
    },



    date:{
        type: String,
    },

    time:{
        type: String,
    },


    ProductName:{
        type: String,
    },
    ProductNo:{
        type: String,
    },
    Validity:{
        type: String,
    },
    Description:{
        type: String,
    },
    HowToUse:{
        type: String,
    },
    
    
    
});
 

var Transaction= module.exports = mongoose.model('Transaction',TransactionSchema);
module.exports.addTransaction = function(transaction, callback){
    // console.log("logging in nowwwwww"+client);
   
    Transaction.create(transaction, callback);
};



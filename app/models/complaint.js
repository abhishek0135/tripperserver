var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ComplaintSchema = new Schema({
   
 
    billerId :
    {
       type: String  ,
        required:true,
      
    },
    complaintDescription :
    {
       type: String  
         
    },
  
    complaintType :
    {
       type: String  ,
        required: true 
    },
    servReason :
    {
       type: String  ,
        required: true 
    },
    requestId :
    {
       type: String  ,
        required: true 
    },

    complaintId :
    {
       type: String  ,
        required: true 
    },
    complaintAssigned :
    {
       type: String  ,
        required: true 
    },

    userId:
    {
       type: String  ,
        required: true 
    },

    createdDate:
    {
        type:Date,
        default:Date.now
    }





    
});
 

var Complaint= module.exports = mongoose.model('Complaint', ComplaintSchema);

module.exports.addComplaint = function(complaint, callback){
      
    
	complaint.create(complaint, callback);
};



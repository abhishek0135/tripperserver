var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BankSchema = new Schema({
    accountVerificationAllowed :
    {
        type:String
       
    },
    bankCode: 
    {
        type:String
    },
    
    bankName:{
        type:String
    },
    impsAllowed:{
        type:String
    },
    neftAllowed:{
        type:String
    },
   
    createdDate:
    {
        type:Date,
        default:Date.now
    }
    
    
    
    
    
    
});
 
var Bank= module.exports = mongoose.model('Bank', BankSchema);

module.exports.addBank = function(bank, callback){
      
	Bank.create(bank, callback);
};

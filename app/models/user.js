var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcryptjs');

 
 
// set up a mongoose model
var UserSchema = new Schema({
   createdTime : 
    { 
        type : Date, default: Date.now 
    },
    dob : 
    { 
        type : Date
    },
   fullName: {
        type: String,
        
    },
    aadhar:{
        type:String,
    },
    gstCertificate:{
        type:String,
    },
    pancard:{
        type:String,
    },
    familyName: {
        type: String,
        
    },
    bloodGroup: {
        type: String,
        
    },
    business: {
        type: String,
        
    },
    education: {
        type: String,
        
    },
    incomeRange: {
        type: String,
        
    },
    emailId:{
        type: String,
        unique: true,
        
    },
    password: {
        type: String,
        
    },
    contactNum: {
        type: Number,
        unique: true,
      
    },
    wallet: {
        type: Number,
        default:0
    },
   
    
    deviceId: {
        type: String
    },
    user_img: {
        type: String
    },
    
    accessLevelId: {
         type: String
        
    },
     accessLevelName : {
        type: String
    },
    
    
 add:{
           type:String                 
                },
    
    access_token:{
        type:String
    },

    user_type:{
        type:Number
    },
    
    isSubscribed:{
        type:Boolean,
        default:false
    },

    freeTransactions:{
        type:Number,
        default:0
    },
    
                            
    
    isSuperAdmin:
    {
        type:Boolean
    },

    emailIdRegistered:{
        type:Boolean,
        default:false
      
    },
    contactNumRegistered:{
        type:Boolean,
        default:true
    },
    isVerified:{
        type:Boolean,
        default:false
    },

    aadharRegistered:{
        type:Boolean,
        default:false
    },

    panRegistered:{
        type:Boolean,
        default:false
    },
   
  
    
    
     
     
                            
    
    
   
    
});
 
UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});
 
UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};
UserSchema.statics.findOrCreate = function findOrCreate(profile, cb){
    var userObj = new this();
    this.findOne({_id : profile.id},function(err,result){ 
        if(!result){
            userObj.username = profile.displayName;
            //....
            userObj.save(cb);
        }else{
            cb(err,result);
        }
    });
};

 
module.exports = mongoose.model('User', UserSchema);
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subscriptionSchema = new Schema({
    title: 
    {
        type: String,
       
    },
    description: 
    {
        type: String,
       
    },

   
    validity: 
    {
        type: Number,
        required: true
    },
    
    freeTransaction: 
    {
        type: Number,
       
    },
    amount: 
    {
        type: Number,
       
    },
    
    createdDate:
    {
        type:Date,
        default:Date.now
    },
   
    subscriptionUrl :
    {
       type: String  
         
    },
    isActive:
    {
        type:Boolean,
        default:true
    },
   
    
});
 

var Subscription= module.exports = mongoose.model('Subscription', subscriptionSchema);
module.exports.addSubscription = function(subscription, callback){
     // console.log("logging in nowwwwww"+client);
    
	Subscription.create(subscription, callback);
};


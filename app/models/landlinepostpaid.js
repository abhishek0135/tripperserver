var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var landlinePostpaidSchema = new Schema({
   
    billerId: 
    {
        type: String,
        required: true
    },
    billerName: 
    {
        type: String,
       
    },
   
    billerCategory :
    {
       type: String  
         
    },
    billerAdhoc :
    {
       type: String  
         
    },
    billerCoverage :
    {
       type: String  
         
    },

    inputParam:{
        type:[]
      },
    
    billerFetchRequiremet:{
        type:String
    },
    billerPaymentExactness:{
        type:String
    },
    billerSupportBillValidation:{
        type:String
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
    isActive:
    {
        type:Boolean,
        default:true
    },
    billerUrl:{
        type:String
    }
    
   
    
});
 

var LandlinePostpaid= module.exports = mongoose.model('landlinePostpaid', landlinePostpaidSchema);

module.exports.addlandlinePostpaid = function(landlinePostpaid, callback){
     
    
    LandlinePostpaid.create(landlinePostpaid, callback);
};
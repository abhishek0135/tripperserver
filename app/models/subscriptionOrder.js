var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubscriptionOrderSchema = new Schema({
   
 
    refId :
    {
       type: String,
        required:true,
   
    },

    subscriptionRef:
{
    type: String,
        required:true,
     
} ,
   userId :
    {
       type: String,
        required:true,
     
    },
  

   
    validity: 
    {
        type: Number,
        required: true
    },
    
    freeTransaction: 
    {
        type: Number,
       
    },
    amount: 
    {
        type: Number,
       
    },
   

   
    startDate:
    {
        type:Date,
        
        
    },
    endDate:
    {
        type:Date,
        
        
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
   
   
    isActive:
    {
        type:Boolean,
        default:true
    },
    
    
});
 

var subscriptionOrder= module.exports = mongoose.model('subscriptionOrder', SubscriptionOrderSchema);

module.exports.addsubscriptionOrder = function(suborder, callback){
      console.log("logging in nowwwwww"+suborder);
    
      subscriptionOrder.create(suborder, callback);
};



var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var offerSchema = new Schema({
   
    title: 
    {
        type: String,
        required: true
    },
    description: 
    {
        type: String,
        required: true
    },
    validity: 
    {
        type: String,
       
    },

    amount: 
    {
        type: Number,
       
    },

    couponCode: 
    {
        type: String,
       
    },

    userIds: [

    ],
   
  
    offerUrl :
    {
       type: String  
         
    },
    isActive:
    {
        type:Boolean,
        default:true
    },
   
    
});
 

var Offer= module.exports = mongoose.model('Offer', offerSchema);
module.exports.addOffer = function(offer, callback){
    
	Offer.create(offer, callback);
};


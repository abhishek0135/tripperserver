var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ElectricitySchema = new Schema({

    billerId :
    {
       type: String,
        required:true 
    },
   
    billerName:{
        type:String
    },
    billerCategory:{
        type:String
    },
    billerAdhoc:{
        type:String
    },
    billerCoverage:{
        type:String
    },
    billerFetchRequiremet:{
        type:String
    },

    inputParam:{
        type:[]
      },

    billerPaymentExactness:{
        type:String
    },
    billerSupportBillValidation:{
        type:String
    },
    
    createdDate:
    {
        type:Date,
        default:Date.now
    },
      isActive:{
          type:Boolean,
          default:true
      }, 
       billerUrl:{
        type:String
    }
    
});
 

var Electricity= module.exports = mongoose.model('Electricity', ElectricitySchema);

module.exports.addElectricity = function(electricity, callback){
    
	Electricity.create(electricity, callback);
};
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var housingSocietySchema = new Schema({
   
    billerId: 
    {
        type: String,
        required: true
    },
    billerName: 
    {
        type: String,
       
    },
   
    billerCategory :
    {
       type: String  
         
    },
    billerAdhoc :
    {
       type: String  
         
    },

    inputParam:{
        type:[]
      },


    billerCoverage :
    {
       type: String  
         
    },
    
    billerFetchRequiremet:{
        type:String
    },
    billerPaymentExactness:{
        type:String
    },
    billerSupportBillValidation:{
        type:String
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
    isActive:
    {
        type:Boolean,
        default:true
    },
    billerUrl:{
        type:String
    }
    
   
    
});
 

var HousingSociety= module.exports = mongoose.model('housingSociety', housingSocietySchema);

module.exports.addHousingSociety = function(housingSociety, callback){
     
    
    HousingSociety.create(housingSociety, callback);
};
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AppliedSchema = new Schema({
   
    offerId: 
    {
        type: String,
        required: true
    },
    
  

    amount: 
    {
        type: Number,
       
    },

    couponCode: 
    {
        type: String,
       
    },

    userId: {
        type: String,
        required: true
    },

    
   
  
   
    isActive:
    {
        type:Boolean,
        default:true
    },
   
    
});
 

var appliedVoucher= module.exports = mongoose.model('AppliedCoupons', AppliedSchema);
module.exports.addApplyVoucher = function(applyVoucher, callback){
    
	appliedVoucher.create(applyVoucher, callback);
};


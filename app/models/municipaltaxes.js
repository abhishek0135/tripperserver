var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var municipalTaxesSchema = new Schema({
   
    billerId: 
    {
        type: String,
        required: true
    },
    billerName: 
    {
        type: String,
       
    },
   
    billerCategory :
    {
       type: String  
         
    },
    billerAdhoc :
    {
       type: String  
         
    },
    billerCoverage :
    {
       type: String  
         
    },

    inputParam:{
        type:[]
      },
    
    billerFetchRequiremet:{
        type:String
    },
    billerPaymentExactness:{
        type:String
    },
    billerSupportBillValidation:{
        type:String
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
    isActive:
    {
        type:Boolean,
        default:true
    },

    billerUrl:{
        type:String
    }
    
   
    
});
 

var MunicipalTaxes= module.exports = mongoose.model('municipalTaxes', municipalTaxesSchema);

module.exports.addMunicipalTaxes = function(municipalTaxes, callback){
     
    
    MunicipalTaxes.create(municipalTaxes, callback);
};
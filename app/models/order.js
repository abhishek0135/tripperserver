var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrderSchema = new Schema({
   
 
    orderId :
    {
       type: String,
        required:true,
        unique:true
    },
   
    billAmount:{
        type:String
    },
    userId:{
        type:String
    },
    consumerName:{
        type:String
    },

    billerName:{
        type:String
    },
    status:{
        type:String
    },
    rate:{
        type:Number,
        
    },

    billerId: 
    {
        type:String
    },


    billPeriod:{
type:String
    },

    isReviewed:{
        type:Boolean,
        default:false
    },


    billNumber:{
        type:String,
        
    },


    isRate:{
        type:Boolean,
        default:false
    },
    
    
    billDate:
    {
        type:String
       
        
    },

    createdDate:
    {
        type:Date,
        default:Date.now
        
    }
  
    
});
 

var Order= module.exports = mongoose.model('Order', OrderSchema);

module.exports.addOrder = function(order, callback){
      console.log("logging in nowwwwww"+order);
    
	Order.create(order, callback);
};



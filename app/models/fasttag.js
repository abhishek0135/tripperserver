var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FastTagSchema = new Schema({
   
    billerId: 
    {
        type: String,
        required: true
    },
    billerName: 
    {
        type: String,
       
    },
   
    billerCategory :
    {
       type: String  
         
    },

    inputParam:{
        type:[]
      },


    billerAdhoc :
    {
       type: String  
         
    },
    billerCoverage :
    {
       type: String  
         
    },
    
    billerFetchRequiremet:{
        type:String
    },
    billerPaymentExactness:{
        type:String
    },
    billerSupportBillValidation:{
        type:String
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
    isActive:
    {
        type:Boolean,
        default:true
    },

    billerUrl:{
        type:String
    }
    
   
    
});
 

var FastTag= module.exports = mongoose.model('FastTag', FastTagSchema);

module.exports.addFastTag = function(fastTag, callback){
     
    
    FastTag.create(fastTag, callback);
};
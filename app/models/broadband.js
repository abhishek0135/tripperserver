var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var broadbandSchema = new Schema({
   
    billerId: 
    {
        type: String,
        required: true
    },
    billerName: 
    {
        type: String,
       
    },
   
    billerCategory :
    {
       type: String  
         
    },
    billerAdhoc :
    {
       type: String  
         
    },
    billerCoverage :
    {
       type: String  
         
    },
    
    billerFetchRequiremet:{
        type:String
    },

    inputParam:{
      type:[]
    },

    billerPaymentExactness:{
        type:String
    },
    billerSupportBillValidation:{
        type:String
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
    isActive:
    {
        type:Boolean,
        default:true
    },

    billerUrl:{
        type:String
    }
   
    
});
 

var Broadband= module.exports = mongoose.model('Broadband', broadbandSchema);
module.exports.addbroadband = function(broadband, callback){
     
	Broadband.create(broadband, callback);
};


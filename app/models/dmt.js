var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DmtSchema = new Schema({

    userId :
    {
       type: String  ,
        
    },
    senderMobile :
    {
       type: String  ,
         
    },
    recipientId:
    {
       type:String,
    },
    txnRefId :
    {
       type: String  ,
        
    },
    transactionType :
    {
       type: String  ,
        
    },
    txnAmount :
    {
       type: String  ,
        
    },

    convFee:{
      type: String  
       
   },

    recipientName :
    {
       type: String  ,
        
    },

    
    
  
    isActive :
    {
       type:Boolean,
       default:true,
    },
    createdDate:
    {
        type:Date,
        default:Date.now
    },
   




    
});
 

var Dmt= module.exports = mongoose.model('DMT', DmtSchema);

module.exports.addDmt = function(dmt, callback){
     // console.log("logging in nowwwwww"+client);
    
	Dmt.create(dmt, callback);
};